/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_SERVICE_IMPLEMENTATION
#define IW_VANET_SERVICE_IMPLEMENTATION

#include <VA.h>
#include <va.grpc.pb.h>

class CVANetServerImpl;

class VANetServiceImplementation final : public VA::VA::Service
{
public:
	VANetServiceImplementation( ) = default;

	explicit VANetServiceImplementation( CVANetServerImpl* pParent );

	explicit VANetServiceImplementation( IVAInterface* pCore, CVANetServerImpl* pParent );

	~VANetServiceImplementation( ) override;

	void SetRealVACore( IVAInterface* pCore );

	void ShutdownEventHandling( );

private:
	IVAInterface* m_pCore;

	CVANetServerImpl* m_pParent;

	std::atomic_bool m_bShutdownRequest { false };

public:
	grpc::Status AttachEventHandler( grpc::ServerContext* context, const google::protobuf::Empty* request, grpc::ServerWriter<VA::Event>* writer ) override;

	grpc::Status GetVersionInfo( ::grpc::ServerContext* context, const ::google::protobuf::Empty* request, VA::VersionInfo* response ) override;

	grpc::Status GetState( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::CoreState* response ) override;

	grpc::Status Reset( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Empty* response ) override;

	grpc::Status GetModules( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::VAModuleInfos* response ) override;

	grpc::Status CallModule( grpc::ServerContext* context, const VA::CallModuleRequest* request, VA::Struct* response ) override;

	grpc::Status GetSearchPaths( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Struct* response ) override;

	grpc::Status GetFileList( grpc::ServerContext* context, const VA::GetFileListRequest* request, VA::Struct* response ) override;

	grpc::Status GetCoreConfiguration( grpc::ServerContext* context, const VA::GetCoreConfigurationRequest* request, VA::Struct* response ) override;

	grpc::Status GetHardwareConfiguration( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Struct* response ) override;

	grpc::Status CreateDirectivityFromParameters( grpc::ServerContext* context, const VA::CreateDirectivityFromParametersRequest* request,
	                                              google::protobuf::Int32Value* response ) override;

	grpc::Status DeleteDirectivity( grpc::ServerContext* context, const VA::DeleteDirectivityRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status GetDirectivityInfo( grpc::ServerContext* context, const VA::GetDirectivityInfoRequest* request, VA::DirectivityInfo* response ) override;

	grpc::Status GetDirectivityInfos( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::DirectivityInfosReply* response ) override;

	grpc::Status SetDirectivityName( grpc::ServerContext* context, const VA::SetDirectivityNameRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetDirectivityName( grpc::ServerContext* context, const VA::GetDirectivityNameRequest* request, google::protobuf::StringValue* response ) override;

	grpc::Status SetDirectivityParameters( grpc::ServerContext* context, const VA::SetDirectivityParametersRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetDirectivityParameters( grpc::ServerContext* context, const VA::GetDirectivityParametersRequest* request, VA::Struct* response ) override;

	grpc::Status CreateSignalSourceBufferFromParameters( grpc::ServerContext* context, const VA::CreateSignalSourceBufferFromParametersRequest* request,
	                                                     google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourcePrototypeFromParameters( grpc::ServerContext* context, const VA::CreateSignalSourcePrototypeFromParametersRequest* request,
	                                                        google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourceTextToSpeech( grpc::ServerContext* context, const VA::CreateSignalSourceTextToSpeechRequest* request,
	                                             google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourceSequencer( grpc::ServerContext* context, const VA::CreateSignalSourceSequencerRequest* request,
	                                          google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourceNetworkStream( grpc::ServerContext* context, const VA::CreateSignalSourceNetworkStreamRequest* request,
	                                              google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourceEngine( grpc::ServerContext* context, const VA::CreateSignalSourceEngineRequest* request,
	                                       google::protobuf::StringValue* response ) override;

	grpc::Status CreateSignalSourceMachine( grpc::ServerContext* context, const VA::CreateSignalSourceMachineRequest* request,
	                                        google::protobuf::StringValue* response ) override;

	grpc::Status DeleteSignalSource( grpc::ServerContext* context, const VA::DeleteSignalSourceRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status GetSignalSourceInfo( grpc::ServerContext* context, const VA::GetSignalSourceInfoRequest* request, VA::SignalSourceInfo* response ) override;

	grpc::Status GetSignalSourceInfos( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::SignalSourceInfos* response ) override;

	grpc::Status GetSignalSourceBufferPlaybackState( grpc::ServerContext* context, const VA::GetSignalSourceBufferPlaybackStateRequest* request,
	                                                 VA::PlaybackState* response ) override;

	grpc::Status SetSignalSourceBufferPlaybackAction( grpc::ServerContext* context, const VA::SetSignalSourceBufferPlaybackActionRequest* request,
	                                                  google::protobuf::Empty* response ) override;

	grpc::Status SetSignalSourceBufferPlaybackPosition( grpc::ServerContext* context, const VA::SetSignalSourceBufferPlaybackPositionRequest* request,
	                                                    google::protobuf::Empty* response ) override;

	grpc::Status SetSignalSourceBufferLooping( grpc::ServerContext* context, const VA::SetSignalSourceBufferLoopingRequest* request,
	                                           google::protobuf::Empty* response ) override;

	grpc::Status GetSignalSourceBufferLooping( grpc::ServerContext* context, const VA::GetSignalSourceBufferLoopingRequest* request,
	                                           google::protobuf::BoolValue* response ) override;

	grpc::Status SetSignalSourceParameters( grpc::ServerContext* context, const VA::SetSignalSourceParametersRequest* request,
	                                        google::protobuf::Empty* response ) override;

	grpc::Status GetSignalSourceParameters( grpc::ServerContext* context, const VA::GetSignalSourceParametersRequest* request, VA::Struct* response ) override;

	grpc::Status AddSignalSourceSequencerSample( grpc::ServerContext* context, const VA::AddSignalSourceSequencerSampleRequest* request,
	                                             google::protobuf::Int32Value* response ) override;

	grpc::Status AddSignalSourceSequencerPlayback( grpc::ServerContext* context, const VA::AddSignalSourceSequencerPlaybackRequest* request,
	                                               google::protobuf::Int32Value* response ) override;

	grpc::Status RemoveSignalSourceSequencerSample( grpc::ServerContext* context, const VA::RemoveSignalSourceSequencerSampleRequest* request,
	                                                google::protobuf::Empty* response ) override;

	grpc::Status CreateSoundSource( grpc::ServerContext* context, const VA::CreateSoundSourceRequest* request, google::protobuf::Int32Value* response ) override;

	grpc::Status GetSoundSourceIDs( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::IntIdVector* response ) override;

	grpc::Status GetSoundSourceInfo( grpc::ServerContext* context, const VA::GetSoundSourceInfoRequest* request, VA::SoundSourceInfo* response ) override;

	grpc::Status CreateSoundSourceExplicitRenderer( grpc::ServerContext* context, const VA::CreateSoundSourceExplicitRendererRequest* request,
	                                                google::protobuf::Int32Value* response ) override;

	grpc::Status DeleteSoundSource( grpc::ServerContext* context, const VA::DeleteSoundSourceRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status SetSoundSourceEnabled( grpc::ServerContext* context, const VA::SetSoundSourceEnabledRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceEnabled( grpc::ServerContext* context, const VA::GetSoundSourceEnabledRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status GetSoundSourceName( grpc::ServerContext* context, const VA::GetSoundSourceNameRequest* request, google::protobuf::StringValue* response ) override;

	grpc::Status SetSoundSourceName( grpc::ServerContext* context, const VA::SetSoundSourceNameRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceSignalSource( grpc::ServerContext* context, const VA::GetSoundSourceSignalSourceRequest* request,
	                                         google::protobuf::StringValue* response ) override;

	grpc::Status SetSoundSourceSignalSource( grpc::ServerContext* context, const VA::SetSoundSourceSignalSourceRequest* request,
	                                         google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceAuralizationMode( grpc::ServerContext* context, const VA::GetSoundSourceAuralizationModeRequest* request,
	                                             google::protobuf::Int32Value* response ) override;

	grpc::Status SetSoundSourceAuralizationMode( grpc::ServerContext* context, const VA::SetSoundSourceAuralizationModeRequest* request,
	                                             google::protobuf::Empty* response ) override;

	grpc::Status SetSoundSourceParameters( grpc::ServerContext* context, const VA::SetSoundSourceParametersRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceParameters( grpc::ServerContext* context, const VA::GetSoundSourceParametersRequest* request, VA::Struct* response ) override;

	grpc::Status GetSoundSourceDirectivity( grpc::ServerContext* context, const VA::GetSoundSourceDirectivityRequest* request,
	                                        google::protobuf::Int32Value* response ) override;

	grpc::Status SetSoundSourceDirectivity( grpc::ServerContext* context, const VA::SetSoundSourceDirectivityRequest* request,
	                                        google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceSoundPower( grpc::ServerContext* context, const VA::GetSoundSourceSoundPowerRequest* request,
	                                       google::protobuf::DoubleValue* response ) override;

	grpc::Status SetSoundSourceSoundPower( grpc::ServerContext* context, const VA::SetSoundSourceSoundPowerRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceMuted( grpc::ServerContext* context, const VA::GetSoundSourceMutedRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status SetSoundSourceMuted( grpc::ServerContext* context, const VA::SetSoundSourceMutedRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourcePose( grpc::ServerContext* context, const VA::GetSoundSourcePoseRequest* request, VA::GetSoundSourcePoseReply* response ) override;

	grpc::Status SetSoundSourcePose( grpc::ServerContext* context, const VA::SetSoundSourcePoseRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourcePosition( grpc::ServerContext* context, const VA::GetSoundSourcePositionRequest* request, VA::Vector3* response ) override;

	grpc::Status SetSoundSourcePosition( grpc::ServerContext* context, const VA::SetSoundSourcePositionRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceOrientation( grpc::ServerContext* context, const VA::GetSoundSourceOrientationRequest* request, VA::Quaternion* response ) override;

	grpc::Status SetSoundSourceOrientation( grpc::ServerContext* context, const VA::SetSoundSourceOrientationRequest* request,
	                                        google::protobuf::Empty* response ) override;

	grpc::Status GetSoundSourceOrientationVU( grpc::ServerContext* context, const VA::GetSoundSourceOrientationVURequest* request,
	                                          VA::GetSoundSourceOrientationVUReply* response ) override;

	grpc::Status SetSoundSourceOrientationVU( grpc::ServerContext* context, const VA::SetSoundSourceOrientationVURequest* request,
	                                          google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverIDs( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::IntIdVector* response ) override;

	grpc::Status CreateSoundReceiver( grpc::ServerContext* context, const VA::CreateSoundReceiverRequest* request, google::protobuf::Int32Value* response ) override;

	grpc::Status CreateSoundReceiverExplicitRenderer( grpc::ServerContext* context, const VA::CreateSoundReceiverExplicitRendererRequest* request,
	                                                  google::protobuf::Int32Value* response ) override;

	grpc::Status DeleteSoundReceiver( grpc::ServerContext* context, const VA::DeleteSoundReceiverRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status GetSoundReceiverInfo( grpc::ServerContext* context, const VA::GetSoundReceiverInfoRequest* request, VA::SoundReceiverInfo* response ) override;

	grpc::Status SetSoundReceiverEnabled( grpc::ServerContext* context, const VA::SetSoundReceiverEnabledRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverEnabled( grpc::ServerContext* context, const VA::GetSoundReceiverEnabledRequest* request,
	                                      google::protobuf::BoolValue* response ) override;

	grpc::Status GetSoundReceiverName( grpc::ServerContext* context, const VA::GetSoundReceiverNameRequest* request, google::protobuf::StringValue* response ) override;

	grpc::Status SetSoundReceiverName( grpc::ServerContext* context, const VA::SetSoundReceiverNameRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverAuralizationMode( grpc::ServerContext* context, const VA::GetSoundReceiverAuralizationModeRequest* request,
	                                               google::protobuf::Int32Value* response ) override;

	grpc::Status SetSoundReceiverAuralizationMode( grpc::ServerContext* context, const VA::SetSoundReceiverAuralizationModeRequest* request,
	                                               google::protobuf::Empty* response ) override;

	grpc::Status SetSoundReceiverParameters( grpc::ServerContext* context, const VA::SetSoundReceiverParametersRequest* request,
	                                         google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverParameters( grpc::ServerContext* context, const VA::GetSoundReceiverParametersRequest* request, VA::Struct* response ) override;

	grpc::Status GetSoundReceiverDirectivity( grpc::ServerContext* context, const VA::GetSoundReceiverDirectivityRequest* request,
	                                          google::protobuf::Int32Value* response ) override;

	grpc::Status SetSoundReceiverDirectivity( grpc::ServerContext* context, const VA::SetSoundReceiverDirectivityRequest* request,
	                                          google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverMuted( grpc::ServerContext* context, const VA::GetSoundReceiverMutedRequest* request, google::protobuf::BoolValue* response ) override;

	grpc::Status SetSoundReceiverMuted( grpc::ServerContext* context, const VA::SetSoundReceiverMutedRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverPose( grpc::ServerContext* context, const VA::GetSoundReceiverPoseRequest* request, VA::GetSoundReceiverPoseReply* response ) override;

	grpc::Status SetSoundReceiverPose( grpc::ServerContext* context, const VA::SetSoundReceiverPoseRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverPosition( grpc::ServerContext* context, const VA::GetSoundReceiverPositionRequest* request, VA::Vector3* response ) override;

	grpc::Status SetSoundReceiverPosition( grpc::ServerContext* context, const VA::SetSoundReceiverPositionRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverOrientationVU( grpc::ServerContext* context, const VA::GetSoundReceiverOrientationVURequest* request,
	                                            VA::GetSoundReceiverOrientationVUReply* response ) override;

	grpc::Status SetSoundReceiverOrientationVU( grpc::ServerContext* context, const VA::SetSoundReceiverOrientationVURequest* request,
	                                            google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverOrientation( grpc::ServerContext* context, const VA::GetSoundReceiverOrientationRequest* request, VA::Quaternion* response ) override;

	grpc::Status SetSoundReceiverOrientation( grpc::ServerContext* context, const VA::SetSoundReceiverOrientationRequest* request,
	                                          google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverHeadAboveTorsoOrientation( grpc::ServerContext* context, const VA::GetSoundReceiverHeadAboveTorsoOrientationRequest* request,
	                                                        VA::Quaternion* response ) override;

	grpc::Status SetSoundReceiverHeadAboveTorsoOrientation( grpc::ServerContext* context, const VA::SetSoundReceiverHeadAboveTorsoOrientationRequest* request,
	                                                        google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverRealWorldPositionOrientationVU( grpc::ServerContext* context, const VA::GetSoundReceiverRealWorldPositionOrientationVURequest* request,
	                                                             VA::GetSoundReceiverRealWorldPositionOrientationVUReply* response ) override;

	grpc::Status SetSoundReceiverRealWorldPositionOrientationVU( grpc::ServerContext* context, const VA::SetSoundReceiverRealWorldPositionOrientationVURequest* request,
	                                                             google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverRealWorldPose( grpc::ServerContext* context, const VA::GetSoundReceiverRealWorldPoseRequest* request,
	                                            VA::GetSoundReceiverRealWorldPoseReply* response ) override;

	grpc::Status SetSoundReceiverRealWorldPose( grpc::ServerContext* context, const VA::SetSoundReceiverRealWorldPoseRequest* request,
	                                            google::protobuf::Empty* response ) override;

	grpc::Status GetSoundReceiverRealWorldHeadAboveTorsoOrientation( grpc::ServerContext* context,
	                                                                 const VA::GetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest* request,
	                                                                 VA::Quaternion* response ) override;

	grpc::Status SetSoundReceiverRealWorldHeadAboveTorsoOrientation( grpc::ServerContext* context,
	                                                                 const VA::SetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest* request,
	                                                                 google::protobuf::Empty* response ) override;

	grpc::Status GetRenderingModules( grpc::ServerContext* context, const VA::GetRenderingModulesRequest* request, VA::AudioRendererInfos* response ) override;

	grpc::Status SetRenderingModuleMuted( grpc::ServerContext* context, const VA::SetRenderingModuleMutedRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetRenderingModuleMuted( grpc::ServerContext* context, const VA::GetRenderingModuleMutedRequest* request,
	                                      google::protobuf::BoolValue* response ) override;

	grpc::Status SetRenderingModuleGain( grpc::ServerContext* context, const VA::SetRenderingModuleGainRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetRenderingModuleGain( grpc::ServerContext* context, const VA::GetRenderingModuleGainRequest* request,
	                                     google::protobuf::DoubleValue* response ) override;

	grpc::Status SetRenderingModuleAuralizationMode( grpc::ServerContext* context, const VA::SetRenderingModuleAuralizationModeRequest* request,
	                                                 google::protobuf::Empty* response ) override;

	grpc::Status GetRenderingModuleAuralizationMode( grpc::ServerContext* context, const VA::GetRenderingModuleAuralizationModeRequest* request,
	                                                 google::protobuf::Int32Value* response ) override;

	grpc::Status SetRenderingModuleParameters( grpc::ServerContext* context, const VA::SetRenderingModuleParametersRequest* request,
	                                           google::protobuf::Empty* response ) override;

	grpc::Status GetRenderingModuleParameters( grpc::ServerContext* context, const VA::GetRenderingModuleParametersRequest* request, VA::Struct* response ) override;

	grpc::Status GetReproductionModules( grpc::ServerContext* context, const VA::GetReproductionModulesRequest* request, VA::AudioReproductionInfos* response ) override;

	grpc::Status SetReproductionModuleMuted( grpc::ServerContext* context, const VA::SetReproductionModuleMutedRequest* request,
	                                         google::protobuf::Empty* response ) override;

	grpc::Status GetReproductionModuleMuted( grpc::ServerContext* context, const VA::GetReproductionModuleMutedRequest* request,
	                                         google::protobuf::BoolValue* response ) override;

	grpc::Status SetReproductionModuleGain( grpc::ServerContext* context, const VA::SetReproductionModuleGainRequest* request,
	                                        google::protobuf::Empty* response ) override;

	grpc::Status GetReproductionModuleGain( grpc::ServerContext* context, const VA::GetReproductionModuleGainRequest* request,
	                                        google::protobuf::DoubleValue* response ) override;

	grpc::Status SetReproductionModuleParameters( grpc::ServerContext* context, const VA::SetReproductionModuleParametersRequest* request,
	                                              google::protobuf::Empty* response ) override;

	grpc::Status GetReproductionModuleParameters( grpc::ServerContext* context, const VA::GetReproductionModuleParametersRequest* request,
	                                              VA::Struct* response ) override;

	grpc::Status GetInputGain( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response ) override;

	grpc::Status SetInputGain( grpc::ServerContext* context, const VA::SetInputGainRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetInputMuted( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response ) override;

	grpc::Status SetInputMuted( grpc::ServerContext* context, const VA::SetInputMutedRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetOutputGain( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response ) override;

	grpc::Status SetOutputGain( grpc::ServerContext* context, const VA::SetOutputGainRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetOutputMuted( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response ) override;

	grpc::Status SetOutputMuted( grpc::ServerContext* context, const VA::SetOutputMutedRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status GetGlobalAuralizationMode( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Int32Value* response ) override;

	grpc::Status SetGlobalAuralizationMode( grpc::ServerContext* context, const VA::SetGlobalAuralizationModeRequest* request,
	                                        google::protobuf::Empty* response ) override;

	grpc::Status GetCoreClock( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response ) override;

	grpc::Status SetCoreClock( grpc::ServerContext* context, const VA::SetCoreClockRequest* request, google::protobuf::Empty* response ) override;

	grpc::Status SetHomogeneousMediumSoundSpeed( grpc::ServerContext* context, const VA::SetHomogeneousMediumSoundSpeedRequest* request,
	                                             google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumSoundSpeed( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response ) override;

	grpc::Status SetHomogeneousMediumTemperature( grpc::ServerContext* context, const VA::SetHomogeneousMediumTemperatureRequest* request,
	                                              google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumTemperature( grpc::ServerContext* context, const google::protobuf::Empty* request,
	                                              google::protobuf::DoubleValue* response ) override;

	grpc::Status SetHomogeneousMediumStaticPressure( grpc::ServerContext* context, const VA::SetHomogeneousMediumStaticPressureRequest* request,
	                                                 google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumStaticPressure( grpc::ServerContext* context, const google::protobuf::Empty* request,
	                                                 google::protobuf::DoubleValue* response ) override;

	grpc::Status SetHomogeneousMediumRelativeHumidity( grpc::ServerContext* context, const VA::SetHomogeneousMediumRelativeHumidityRequest* request,
	                                                   google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumRelativeHumidity( grpc::ServerContext* context, const google::protobuf::Empty* request,
	                                                   google::protobuf::DoubleValue* response ) override;

	grpc::Status SetHomogeneousMediumShiftSpeed( grpc::ServerContext* context, const VA::Vector3* request, google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumShiftSpeed( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Vector3* response ) override;

	grpc::Status SetHomogeneousMediumParameters( grpc::ServerContext* context, const VA::HomogeneousMediumParametersRequest* request,
	                                             google::protobuf::Empty* response ) override;

	grpc::Status GetHomogeneousMediumParameters( grpc::ServerContext* context, const VA::HomogeneousMediumParametersRequest* request, VA::Struct* response ) override;

	grpc::Status GetUpdateLocked( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response ) override;

	grpc::Status LockUpdate( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Empty* response ) override;

	grpc::Status UnlockUpdate( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Int32Value* response ) override;
};

#endif // IW_VANET_AUDIO_STREAM_SERVER_IMPL
