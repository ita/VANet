/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_CLIENT_NETWORKED_CORE
#define IW_VANET_CLIENT_NETWORKED_CORE

#include "VANetSerializationUtils.h"

#include <VA.h>
#include <grpcpp/grpcpp.h>
#include <set>
#include <va.grpc.pb.h>


////////////////////////////////////////////////
/////// NetworkedVACore                 ////////
////////////////////////////////////////////////

class CVANetClientImpl::CNetworkedVACore : public IVAInterface
{
public:
	CNetworkedVACore( CVANetClientImpl* pParent, std::shared_ptr<::grpc::Channel> channel ) : IVAInterface( ), m_pParent( pParent ), m_pStub( VA::VA::NewStub( channel ) )
	{
		m_tEventHandlerThread = std::thread(
		    [this]( )
		    {
			    const google::protobuf::Empty request;
			    const std::unique_ptr<grpc::ClientReader<VA::Event>> reader( m_pStub->AttachEventHandler( &m_oEventHandlingContext, request ) );

			    VA::Event incomingEvent;
			    while( !m_bShutdownRequest && reader->Read( &incomingEvent ) )
			    {
				    const auto vaEvent = Decode( incomingEvent );
				    for( auto&& handler: m_vEventHandlers )
				    {
					    handler->HandleVAEvent( &vaEvent );
				    }
			    }
			    const auto returnStatus = reader->Finish( );

			    if( returnStatus.error_code( ) != grpc::StatusCode::UNAVAILABLE && returnStatus.error_code( ) != grpc::StatusCode::CANCELLED )
			    {
				    handleRPCStatus( returnStatus );
			    }
		    } );
	}

	~CNetworkedVACore( ) override
	{
		m_bShutdownRequest = true;
		m_oEventHandlingContext.TryCancel( );
		m_tEventHandlerThread.join( );
	};

	void GetVersionInfo( CVAVersionInfo* pVersionInfo ) const override
	{
		google::protobuf::Empty request;
		VA::VersionInfo reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetVersionInfo( &context, request, &reply ) );

		pVersionInfo->sVersion  = reply.version( );
		pVersionInfo->sDate     = reply.release_date( );
		pVersionInfo->sFlags    = reply.property_flags( );
		pVersionInfo->sComments = reply.comments( );
	};

	void SetOutputStream( std::ostream* ) { VA_EXCEPT_NOT_IMPLEMENTED; };

	[[nodiscard]] int GetState( ) const override
	{
		google::protobuf::Empty request;
		VA::CoreState reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetState( &context, request, &reply ) );

		int returnState = VA_CORESTATE_FAIL;

		switch( reply.state( ) )
		{
			case VA::CoreState_State_CREATED:
				returnState = VA_CORESTATE_CREATED;
				break;
			case VA::CoreState_State_READY:
				returnState = VA_CORESTATE_READY;
				break;
			case VA::CoreState_State_FAIL:
				returnState = VA_CORESTATE_FAIL;
				break;
			default:
				VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new data type was added" );
		}

		return returnState;
	};

	void Initialize( ) override
	{
		throw CVAException( CVAException::NOT_IMPLEMENTED, "Initialize is not supported for networked cores" );
	};

	void Finalize( ) override
	{
		throw CVAException( CVAException::NOT_IMPLEMENTED, "Finalize is not supported for networked cores" );
	};

	void Reset( ) override
	{
		const google::protobuf::Empty request;
		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->Reset( &context, request, &reply ) );
	};

	void AttachEventHandler( IVAEventHandler* pEventHandler ) override { m_vEventHandlers.insert( pEventHandler ); }

	void DetachEventHandler( IVAEventHandler* pEventHandler ) override { m_vEventHandlers.erase( pEventHandler ); }

	CVAStruct GetSearchPaths( ) const override
	{
		google::protobuf::Empty request;

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSearchPaths( &context, request, &reply ) );

		return Decode( reply );
	};

	std::string FindFilePath( const std::string& sFilePath ) const override
	{
		// VA::Identifier request;
		// request.set_string_identifier( sFilePath );

		// VA::Identifier reply;
		// grpc::ClientContext context;
		// handleRPCStatus( m_pStub->FindFilePath( &context, request, &reply ) ); TODO

		return { };
	};

	CVAStruct GetCoreConfiguration( const bool bFilterEnabled = true ) const override
	{
		VA::GetCoreConfigurationRequest request;
		request.set_only_enabled( bFilterEnabled );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetCoreConfiguration( &context, request, &reply ) );

		return Decode( reply );
	};

	CVAStruct GetHardwareConfiguration( ) const override
	{
		google::protobuf::Empty request;

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHardwareConfiguration( &context, request, &reply ) );

		return Decode( reply );
	};

	CVAStruct GetFileList( const bool bRecursive = true, const std::string& sFileSuffixMask = "*" ) const override
	{
		VA::GetFileListRequest request;
		request.set_recursive( bRecursive );
		request.set_file_suffix_filter( sFileSuffixMask );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetFileList( &context, request, &reply ) );

		return Decode( reply );
	};

	void GetModules( std::vector<CVAModuleInfo>& viModuleInfos ) const
	{
		google::protobuf::Empty request;
		VA::VAModuleInfos reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetModules( &context, request, &reply ) );

		viModuleInfos.clear( );

		for( auto&& moduleInfo: reply.module_infos( ) )
		{
			CVAModuleInfo module;

			module.sName = moduleInfo.name( );
			module.sDesc = moduleInfo.description( );
			module.iID   = moduleInfo.id( );

			viModuleInfos.push_back( module );
		}
	};

	CVAStruct CallModule( const std::string& sModuleName, const CVAStruct& oArgs ) override
	{
		VA::CallModuleRequest request;
		request.set_module_name( sModuleName );
		request.mutable_module_parameters( )->CopyFrom( Encode( oArgs ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CallModule( &context, request, &reply ) );

		return Decode( reply );
	};

	// Directivity
	[[nodiscard]] int CreateDirectivityFromParameters( const CVAStruct& oParams, const std::string& sName ) override
	{
		VA::CreateDirectivityFromParametersRequest request;
		request.set_name( sName );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateDirectivityFromParameters( &context, request, &reply ) );

		return reply.value( );
	};

	[[nodiscard]] bool DeleteDirectivity( const int iID ) override
	{
		VA::DeleteDirectivityRequest request;
		request.set_directivity_id( iID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->DeleteDirectivity( &context, request, &reply ) );

		return reply.value( );
	};

	CVADirectivityInfo GetDirectivityInfo( const int iDirID ) const override
	{
		VA::GetDirectivityInfoRequest request;
		request.set_directivity_id( iDirID );

		VA::DirectivityInfo reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetDirectivityInfo( &context, request, &reply ) );

		CVADirectivityInfo directivityInfo = Decode( reply );

		return directivityInfo;
	};

	void GetDirectivityInfos( std::vector<CVADirectivityInfo>& vdiDest ) const override
	{
		google::protobuf::Empty request;

		VA::DirectivityInfosReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetDirectivityInfos( &context, request, &reply ) );

		vdiDest.clear( );

		for( int i = 0; i < reply.directivity_infos_size( ); i++ )
			vdiDest.push_back( Decode( reply.directivity_infos( ).at( i ) ) );
	};

	void SetDirectivityName( const int iID, const std::string& sName ) override
	{
		VA::SetDirectivityNameRequest request;
		request.set_directivity_id( iID );
		request.set_name( sName );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetDirectivityName( &context, request, &reply ) );
	};

	std::string GetDirectivityName( const int iID ) const override
	{
		VA::GetDirectivityNameRequest request;
		request.set_directivity_id( iID );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetDirectivityName( &context, request, &reply ) );

		return reply.value( );
	};

	void SetDirectivityParameters( const int iID, const CVAStruct& oParams )
	{
		VA::SetDirectivityParametersRequest request;
		request.set_directivity_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetDirectivityParameters( &context, request, &reply ) );
	};

	CVAStruct GetDirectivityParameters( const int iID, const CVAStruct& oArgs ) const override
	{
		VA::GetDirectivityParametersRequest request;
		request.set_directivity_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oArgs ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetDirectivityParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	// SignalSource
	[[nodiscard]] std::string CreateSignalSourceBufferFromParameters( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceBufferFromParametersRequest request;
		request.set_name( sName );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceBufferFromParameters( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourcePrototypeFromParameters( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		VA::CreateSignalSourcePrototypeFromParametersRequest request;
		request.set_name( sName );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourcePrototypeFromParameters( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourceTextToSpeech( const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceTextToSpeechRequest request;
		request.set_name( sName );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceTextToSpeech( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourceSequencer( const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceSequencerRequest request;
		request.set_name( sName );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceSequencer( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourceNetworkStream( const std::string& sInterface, const int iPort, const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceNetworkStreamRequest request;
		request.set_interface( sInterface );
		request.set_port( iPort );
		request.set_name( sName );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceNetworkStream( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourceEngine( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceEngineRequest request;
		request.set_name( sName );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceEngine( &context, request, &reply ) );

		return reply.value( );
	};

	std::string CreateSignalSourceMachine( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		VA::CreateSignalSourceMachineRequest request;
		request.set_name( sName );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSignalSourceMachine( &context, request, &reply ) );

		return reply.value( );
	};

	bool DeleteSignalSource( const std::string& sID ) override
	{
		VA::DeleteSignalSourceRequest request;
		request.set_signal_source_id( sID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->DeleteSignalSource( &context, request, &reply ) );

		return reply.value( );
	};

	std::string RegisterSignalSource( IVAAudioSignalSource*, const std::string& ) { VA_EXCEPT_NOT_IMPLEMENTED; };

	bool UnregisterSignalSource( IVAAudioSignalSource* ) { VA_EXCEPT_NOT_IMPLEMENTED; };

	CVASignalSourceInfo GetSignalSourceInfo( const std::string& sSignalSourceID ) const override
	{
		VA::GetSignalSourceInfoRequest request;
		request.set_signal_source_id( sSignalSourceID );

		VA::SignalSourceInfo reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSignalSourceInfo( &context, request, &reply ) );

		return Decode( reply );
	};

	void GetSignalSourceInfos( std::vector<CVASignalSourceInfo>& vssiDest ) const override
	{
		google::protobuf::Empty request;
		VA::SignalSourceInfos reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSignalSourceInfos( &context, request, &reply ) );

		vssiDest.clear( );

		for( auto&& signalSourceInfo: reply.signal_source_infos( ) )
		{
			CVASignalSourceInfo info;
			info = Decode( signalSourceInfo );
			vssiDest.push_back( info );
		}
	};

	int GetSignalSourceBufferPlaybackState( const std::string& sSignalSourceID ) const override
	{
		VA::GetSignalSourceBufferPlaybackStateRequest request;
		request.set_signal_source_id( sSignalSourceID );

		VA::PlaybackState reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSignalSourceBufferPlaybackState( &context, request, &reply ) );

		switch( reply.state( ) )
		{
			case VA::PlaybackState_State_INVALID:
				return IVAInterface::VA_PLAYBACK_STATE_INVALID;
				break;
			case VA::PlaybackState_State_STOPPED:
				return IVAInterface::VA_PLAYBACK_STATE_STOPPED;
				break;
			case VA::PlaybackState_State_PAUSED:
				return IVAInterface::VA_PLAYBACK_STATE_PAUSED;
				break;
			case VA::PlaybackState_State_PLAYING:
				return IVAInterface::VA_PLAYBACK_STATE_PLAYING;
				break;
			default:
				VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new playback state was added" );
		}
	};

	void SetSignalSourceBufferPlaybackAction( const std::string& sSignalSourceID, const int iPlayState ) override
	{
		VA::SetSignalSourceBufferPlaybackActionRequest request;
		request.set_signal_source_id( sSignalSourceID );
		switch( iPlayState )
		{
			case IVAInterface::VA_PLAYBACK_ACTION_NONE:
				request.mutable_playback_action( )->set_action( VA::PlaybackAction_Action_NONE );
				break;
			case IVAInterface::VA_PLAYBACK_ACTION_STOP:
				request.mutable_playback_action( )->set_action( VA::PlaybackAction_Action_STOP );
				break;
			case IVAInterface::VA_PLAYBACK_ACTION_PAUSE:
				request.mutable_playback_action( )->set_action( VA::PlaybackAction_Action_PAUSE );
				break;
			case IVAInterface::VA_PLAYBACK_ACTION_PLAY:
				request.mutable_playback_action( )->set_action( VA::PlaybackAction_Action_PLAY );
				break;
			default:
				VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new playback action was added" );
		}

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSignalSourceBufferPlaybackAction( &context, request, &reply ) );
	};

	void SetSignalSourceBufferPlaybackPosition( const std::string& sSignalSourceID, const double dPlaybackPosition ) override
	{
		VA::SetSignalSourceBufferPlaybackPositionRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.set_position( dPlaybackPosition );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSignalSourceBufferPlaybackPosition( &context, request, &reply ) );
	};

	bool GetSignalSourceBufferLooping( const std::string& sSignalSourceID ) const override
	{
		VA::GetSignalSourceBufferLoopingRequest request;
		request.set_signal_source_id( sSignalSourceID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSignalSourceBufferLooping( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSignalSourceBufferLooping( const std::string& sSignalSourceID, const bool bLooping ) override
	{
		VA::SetSignalSourceBufferLoopingRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.set_looping( bLooping );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSignalSourceBufferLooping( &context, request, &reply ) );
	};

	void SetSignalSourceParameters( const std::string& sSignalSourceID, const CVAStruct& oParams ) override
	{
		VA::SetSignalSourceParametersRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSignalSourceParameters( &context, request, &reply ) );
	};

	CVAStruct GetSignalSourceParameters( const std::string& sSignalSourceID, const CVAStruct& oParams ) const override
	{
		VA::GetSignalSourceParametersRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSignalSourceParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	int AddSignalSourceSequencerSample( const std::string& sSignalSourceID, const CVAStruct& oArgs ) { VA_EXCEPT_NOT_IMPLEMENTED; };

	int AddSignalSourceSequencerPlayback( const std::string& sSignalSourceID, const int iSoundID, const int iFlags, const double dTimeCode ) override
	{
		VA::AddSignalSourceSequencerPlaybackRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.set_sound_id( iSoundID );
		request.set_flags( iFlags );
		request.set_time_code( dTimeCode );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->AddSignalSourceSequencerPlayback( &context, request, &reply ) );

		return reply.value( );
	};

	void RemoveSignalSourceSequencerSample( const std::string& sSignalSourceID, const int iSoundID )
	{
		VA::RemoveSignalSourceSequencerSampleRequest request;
		request.set_signal_source_id( sSignalSourceID );
		request.set_sample_id( iSoundID );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->RemoveSignalSourceSequencerSample( &context, request, &reply ) );
	};

	// Update
	bool GetUpdateLocked( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetUpdateLocked( &context, request, &reply ) );

		return reply.value( );
	};

	void LockUpdate( ) override
	{
		google::protobuf::Empty request;

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->LockUpdate( &context, request, &reply ) );
	};

	int UnlockUpdate( ) override
	{
		google::protobuf::Empty request;

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->UnlockUpdate( &context, request, &reply ) );

		return reply.value( );
	};

	// SoundSource

	void GetSoundSourceIDs( std::vector<int>& vSoundSourceIDs ) override
	{
		google::protobuf::Empty request;

		VA::IntIdVector reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceIDs( &context, request, &reply ) );

		vSoundSourceIDs = Decode( reply );
	};

	[[nodiscard]] int CreateSoundSource( const std::string& sName = "" ) override
	{
		VA::CreateSoundSourceRequest request;
		request.set_name( sName );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSoundSource( &context, request, &reply ) );

		return reply.value( );
	};

	int CreateSoundSourceExplicitRenderer( const std::string& sRendererID, const std::string& sName = "" ) override
	{
		VA::CreateSoundSourceExplicitRendererRequest request;
		request.set_renderer_id( sRendererID );
		request.set_name( sName );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSoundSourceExplicitRenderer( &context, request, &reply ) );

		return reply.value( );
	};

	int DeleteSoundSource( const int iID ) override
	{
		VA::DeleteSoundSourceRequest request;
		request.set_sound_source_id( iID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->DeleteSoundSource( &context, request, &reply ) );

		if( reply.value( ) )
			return 0;
		else
			return -1;
	};

	void SetSoundSourceEnabled( const int iSoundSourceID, const bool bEnabled = true ) override
	{
		VA::SetSoundSourceEnabledRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_enabled( bEnabled );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceEnabled( &context, request, &reply ) );
	};

	bool GetSoundSourceEnabled( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceEnabledRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceEnabled( &context, request, &reply ) );

		return reply.value( );
	};

	CVASoundSourceInfo GetSoundSourceInfo( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceInfoRequest request;
		request.set_sound_source_id( iSoundSourceID );

		VA::SoundSourceInfo reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceInfo( &context, request, &reply ) );

		CVASoundSourceInfo oSoundSourceInfo = Decode( reply );

		return oSoundSourceInfo;
	};

	std::string GetSoundSourceName( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceNameRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceName( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceName( const int iSoundSourceID, const std::string& sName ) override
	{
		VA::SetSoundSourceNameRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_name( sName );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceName( &context, request, &reply ) );
	};

	std::string GetSoundSourceSignalSource( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceSignalSourceRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceSignalSource( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceSignalSource( const int iSoundSourceID, const std::string& sSignalSourceID ) override
	{
		VA::SetSoundSourceSignalSourceRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_signal_source_id( sSignalSourceID );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceSignalSource( &context, request, &reply ) );
	};

	int GetSoundSourceAuralizationMode( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceAuralizationModeRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceAuralizationMode( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceAuralizationMode( const int iSoundSourceID, const int iAuralizationMode ) override
	{
		VA::SetSoundSourceAuralizationModeRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_mode( iAuralizationMode );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceAuralizationMode( &context, request, &reply ) );
	};

	CVAStruct GetSoundSourceParameters( const int iID, const CVAStruct& oArgs ) const
	{
		VA::GetSoundSourceParametersRequest request;
		request.set_sound_source_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oArgs ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundSourceParameters( const int iID, const CVAStruct& oParams ) override
	{
		VA::SetSoundSourceParametersRequest request;
		request.set_sound_source_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceParameters( &context, request, &reply ) );
	};

	int GetSoundSourceDirectivity( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceDirectivityRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceDirectivity( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceDirectivity( const int iSoundSourceID, const int iDirectivityID ) override
	{
		VA::SetSoundSourceDirectivityRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_directivity_id( iDirectivityID );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceDirectivity( &context, request, &reply ) );
	};

	double GetSoundSourceSoundPower( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceSoundPowerRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceSoundPower( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceSoundPower( const int iSoundSourceID, const double dPower ) override
	{
		VA::SetSoundSourceSoundPowerRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_sound_power( dPower );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceSoundPower( &context, request, &reply ) );
	};

	bool GetSoundSourceMuted( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceMutedRequest request;
		request.set_sound_source_id( iSoundSourceID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundSourceMuted( const int iSoundSourceID, const bool bMuted = true ) override
	{
		VA::SetSoundSourceMutedRequest request;
		request.set_sound_source_id( iSoundSourceID );
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceMuted( &context, request, &reply ) );
	};

	void GetSoundSourcePose( const int iID, VAVec3& v3Pos, VAQuat& qOrient ) const override
	{
		VA::GetSoundSourcePoseRequest request;
		request.set_sound_source_id( iID );

		VA::GetSoundSourcePoseReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourcePose( &context, request, &reply ) );
		v3Pos   = Decode( reply.position( ) );
		qOrient = Decode( reply.orientation( ) );
	};

	void SetSoundSourcePose( const int iID, const VAVec3& v3Pos, const VAQuat& qOrient ) override
	{
		VA::SetSoundSourcePoseRequest request;
		request.set_sound_source_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourcePose( &context, request, &reply ) );
	};

	void SetSoundSourcePosition( const int iID, const VAVec3& v3Pos ) override
	{
		VA::SetSoundSourcePositionRequest request;
		request.set_sound_source_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourcePosition( &context, request, &reply ) );
	};

	void SetSoundSourceOrientation( const int iID, const VAQuat& qOrient ) override
	{
		VA::SetSoundSourceOrientationRequest request;
		request.set_sound_source_id( iID );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceOrientation( &context, request, &reply ) );
	};

	void SetSoundSourceOrientationVU( const int iID, const VAVec3& v3View, const VAVec3& v3Up ) override
	{
		VA::SetSoundSourceOrientationVURequest request;
		request.set_sound_source_id( iID );
		request.mutable_view( )->CopyFrom( Encode( v3View ) );
		request.mutable_up( )->CopyFrom( Encode( v3Up ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundSourceOrientationVU( &context, request, &reply ) );
	};

	VAVec3 GetSoundSourcePosition( const int iID ) const override
	{
		VA::GetSoundSourcePositionRequest request;
		request.set_sound_source_id( iID );

		VA::Vector3 reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourcePosition( &context, request, &reply ) );

		return Decode( reply );
	};

	VAQuat GetSoundSourceOrientation( const int iSoundSourceID ) const override
	{
		VA::GetSoundSourceOrientationRequest request;
		request.set_sound_source_id( iSoundSourceID );

		VA::Quaternion reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceOrientation( &context, request, &reply ) );

		return Decode( reply );
	};

	void GetSoundSourceOrientationVU( const int iSoundSourceID, VAVec3& v3View, VAVec3& v3Up ) const override
	{
		VA::GetSoundSourceOrientationVURequest request;
		request.set_sound_source_id( iSoundSourceID );

		VA::GetSoundSourceOrientationVUReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundSourceOrientationVU( &context, request, &reply ) );
		v3View = Decode( reply.view( ) );
		v3Up   = Decode( reply.up( ) );
	};

	int GetSoundSourceGeometryMesh( const int iID ) const override
	{
		return {};
	};

	void SetSoundSourceGeometryMesh( const int iSoundSourceID, const int iGeometryMeshID ) override
	{
	};

	// Sound Receiver

	void GetSoundReceiverIDs( std::vector<int>& vSoundReceiverIDs ) const override
	{
		google::protobuf::Empty request;

		VA::IntIdVector reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverIDs( &context, request, &reply ) );

		vSoundReceiverIDs = Decode( reply );
	};

	[[nodiscard]] int CreateSoundReceiver( const std::string& sName = "" ) override
	{
		VA::CreateSoundReceiverRequest request;
		request.set_name( sName );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSoundReceiver( &context, request, &reply ) );

		return reply.value( );
	};

	int CreateSoundReceiverExplicitRenderer( const std::string& sRendererID, const std::string& sName ) override
	{
		VA::CreateSoundReceiverExplicitRendererRequest request;
		request.set_renderer_id( sRendererID );
		request.set_name( sName );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->CreateSoundReceiverExplicitRenderer( &context, request, &reply ) );

		return reply.value( );
	};

	int DeleteSoundReceiver( const int iSoundReceiverID ) override
	{
		VA::DeleteSoundReceiverRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->DeleteSoundReceiver( &context, request, &reply ) );

		if( reply.value( ) )
			return 0;
		else
			return -1;
	};

	CVASoundReceiverInfo GetSoundReceiverInfo( const int iID ) const override
	{
		VA::GetSoundReceiverInfoRequest request;
		request.set_sound_receiver_id( iID );

		VA::SoundReceiverInfo reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverInfo( &context, request, &reply ) );

		CVASoundReceiverInfo SoundReceiverInfo = Decode( reply );

		return SoundReceiverInfo;
	};

	void SetSoundReceiverEnabled( const int iSoundReceiverID, const bool bEnabled = true ) override
	{
		VA::SetSoundReceiverEnabledRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );
		request.set_enabled( bEnabled );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverEnabled( &context, request, &reply ) );
	};

	bool GetSoundReceiverEnabled( const int iSoundReceiverID ) const override
	{
		VA::GetSoundReceiverEnabledRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverEnabled( &context, request, &reply ) );

		return reply.value( );
	};

	std::string GetSoundReceiverName( const int iSoundReceiverID ) const override
	{
		VA::GetSoundReceiverNameRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		google::protobuf::StringValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverName( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundReceiverName( const int iSoundReceiverID, const std::string& sName ) override
	{
		VA::SetSoundReceiverNameRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );
		request.set_name( sName );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverName( &context, request, &reply ) );
	};

	int GetSoundReceiverAuralizationMode( const int iSoundReceiverID ) const override
	{
		VA::GetSoundReceiverAuralizationModeRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverAuralizationMode( &context, request, &reply ) );

		return reply.value( );
	}

	void SetSoundReceiverAuralizationMode( const int iSoundReceiverID, const int iAuralizationMode ) override
	{
		VA::SetSoundReceiverAuralizationModeRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );
		request.set_mode( iAuralizationMode );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverAuralizationMode( &context, request, &reply ) );
	};

	CVAStruct GetSoundReceiverParameters( const int iID, const CVAStruct& oArgs ) const override
	{
		VA::GetSoundReceiverParametersRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oArgs ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundReceiverParameters( const int iID, const CVAStruct& oParams ) override
	{
		VA::SetSoundReceiverParametersRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverParameters( &context, request, &reply ) );
	};

	int GetSoundReceiverDirectivity( const int iSoundReceiverID ) const override
	{
		VA::GetSoundReceiverDirectivityRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverDirectivity( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundReceiverDirectivity( const int iSoundReceiverID, const int iDirectivityID ) override
	{
		VA::SetSoundReceiverDirectivityRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );
		request.set_directivity_id( iDirectivityID );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverDirectivity( &context, request, &reply ) );
	};

	bool GetSoundReceiverMuted( const int iID ) const override
	{
		VA::GetSoundReceiverMutedRequest request;
		request.set_sound_receiver_id( iID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetSoundReceiverMuted( const int iID, const bool bMuted = true ) override
	{
		VA::SetSoundReceiverMutedRequest request;
		request.set_sound_receiver_id( iID );
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverMuted( &context, request, &reply ) );
	};

	void GetSoundReceiverPose( const int iID, VAVec3& v3Pos, VAQuat& qOrient ) const override
	{
		VA::GetSoundReceiverPoseRequest request;
		request.set_sound_receiver_id( iID );

		VA::GetSoundReceiverPoseReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverPose( &context, request, &reply ) );
		v3Pos   = Decode( reply.position( ) );
		qOrient = Decode( reply.orientation( ) );
	};

	void SetSoundReceiverPose( const int iID, const VAVec3& v3Pos, const VAQuat& qOrient ) override
	{
		VA::SetSoundReceiverPoseRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverPose( &context, request, &reply ) );
	};

	VAVec3 GetSoundReceiverPosition( const int iID ) const override
	{
		VA::GetSoundReceiverPositionRequest request;
		request.set_sound_receiver_id( iID );

		VA::Vector3 reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverPosition( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundReceiverPosition( const int iID, const VAVec3& v3Pos ) override
	{
		VA::SetSoundReceiverPositionRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverPosition( &context, request, &reply ) );
	};

	VAQuat GetSoundReceiverOrientation( const int iSoundReceiverID ) const override
	{
		VA::GetSoundReceiverOrientationRequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		VA::Quaternion reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverOrientation( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundReceiverOrientation( const int iID, const VAQuat& qOrient ) override
	{
		VA::SetSoundReceiverOrientationRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverOrientation( &context, request, &reply ) );
	};

	void GetSoundReceiverOrientationVU( const int iSoundReceiverID, VAVec3& v3View, VAVec3& v3Up ) const override
	{
		VA::GetSoundReceiverOrientationVURequest request;
		request.set_sound_receiver_id( iSoundReceiverID );

		VA::GetSoundReceiverOrientationVUReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverOrientationVU( &context, request, &reply ) );
		v3View = Decode( reply.view( ) );
		v3Up   = Decode( reply.up( ) );
	};

	void SetSoundReceiverOrientationVU( const int iID, const VAVec3& v3View, const VAVec3& v3Up ) override
	{
		VA::SetSoundReceiverOrientationVURequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_view( )->CopyFrom( Encode( v3View ) );
		request.mutable_up( )->CopyFrom( Encode( v3Up ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverOrientationVU( &context, request, &reply ) );
	};

	VAQuat GetSoundReceiverHeadAboveTorsoOrientation( const int iID ) const override
	{
		VA::GetSoundReceiverHeadAboveTorsoOrientationRequest request;
		request.set_sound_receiver_id( iID );

		VA::Quaternion reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverHeadAboveTorsoOrientation( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundReceiverHeadAboveTorsoOrientation( const int iID, const VAQuat& qOrient ) override
	{
		VA::SetSoundReceiverHeadAboveTorsoOrientationRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverHeadAboveTorsoOrientation( &context, request, &reply ) );
	};

	int GetSoundReceiverGeometryMesh( const int iID ) const override
	{
		return {};
	};

	void SetSoundReceiverGeometryMesh( const int iSoundReceiverID, const int iGeometryMeshID ) override
	{
	};

	void GetSoundReceiverRealWorldPose( const int iID, VAVec3& v3Pos, VAQuat& qOrient ) const override
	{
		VA::GetSoundReceiverRealWorldPoseRequest request;
		request.set_sound_receiver_id( iID );

		VA::GetSoundReceiverRealWorldPoseReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverRealWorldPose( &context, request, &reply ) );
		v3Pos   = Decode( reply.position( ) );
		qOrient = Decode( reply.orientation( ) );
	};

	void SetSoundReceiverRealWorldPose( const int iID, const VAVec3& v3Pos, const VAQuat& qOrient ) override
	{
		VA::SetSoundReceiverRealWorldPoseRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverRealWorldPose( &context, request, &reply ) );
	};

	VAQuat GetSoundReceiverRealWorldHeadAboveTorsoOrientation( const int iID ) const override
	{
		VA::GetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest request;
		request.set_sound_receiver_id( iID );

		VA::Quaternion reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverRealWorldHeadAboveTorsoOrientation( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetSoundReceiverRealWorldHeadAboveTorsoOrientation( const int iID, const VAQuat& qOrient ) override
	{
		VA::SetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_orientation( )->CopyFrom( Encode( qOrient ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverRealWorldHeadAboveTorsoOrientation( &context, request, &reply ) );
	};

	void GetSoundReceiverRealWorldPositionOrientationVU( const int iID, VAVec3& v3Pos, VAVec3& v3View, VAVec3& v3Up ) const
	{
		VA::GetSoundReceiverRealWorldPositionOrientationVURequest request;
		request.set_sound_receiver_id( iID );

		VA::GetSoundReceiverRealWorldPositionOrientationVUReply reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetSoundReceiverRealWorldPositionOrientationVU( &context, request, &reply ) );
		v3Pos  = Decode( reply.position( ) );
		v3View = Decode( reply.view( ) );
		v3Up   = Decode( reply.up( ) );
	};

	void SetSoundReceiverRealWorldPositionOrientationVU( const int iID, const VAVec3& v3Pos, const VAVec3& v3View, const VAVec3& v3Up ) override
	{
		VA::SetSoundReceiverRealWorldPositionOrientationVURequest request;
		request.set_sound_receiver_id( iID );
		request.mutable_position( )->CopyFrom( Encode( v3Pos ) );
		request.mutable_view( )->CopyFrom( Encode( v3View ) );
		request.mutable_up( )->CopyFrom( Encode( v3Up ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetSoundReceiverRealWorldPositionOrientationVU( &context, request, &reply ) );
	};

	// HomogeneusMedium

	void SetHomogeneousMediumSoundSpeed( const double dSoundSpeed ) override
	{
		VA::SetHomogeneousMediumSoundSpeedRequest request;
		request.set_sound_speed( dSoundSpeed );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumSoundSpeed( &context, request, &reply ) );
	};

	double GetHomogeneousMediumSoundSpeed( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumSoundSpeed( &context, request, &reply ) );

		return reply.value( );
	};

	void SetHomogeneousMediumTemperature( const double dDegreesCentigrade ) override
	{
		VA::SetHomogeneousMediumTemperatureRequest request;
		request.set_temperature( dDegreesCentigrade );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumTemperature( &context, request, &reply ) );
	};

	double GetHomogeneousMediumTemperature( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumTemperature( &context, request, &reply ) );

		return reply.value( );
	};

	void SetHomogeneousMediumStaticPressure( const double dPressurePascal ) override
	{
		VA::SetHomogeneousMediumStaticPressureRequest request;
		request.set_static_pressure( dPressurePascal );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumStaticPressure( &context, request, &reply ) );
	};

	double GetHomogeneousMediumStaticPressure( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumStaticPressure( &context, request, &reply ) );

		return reply.value( );
	};

	void SetHomogeneousMediumRelativeHumidity( const double dRelativeHumidityPercent ) override
	{
		VA::SetHomogeneousMediumRelativeHumidityRequest request;
		request.set_humidity( dRelativeHumidityPercent );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumRelativeHumidity( &context, request, &reply ) );
	};

	double GetHomogeneousMediumRelativeHumidity( ) override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumRelativeHumidity( &context, request, &reply ) );

		return reply.value( );
	};

	void SetHomogeneousMediumShiftSpeed( const VAVec3& v3TranslationSpeed ) override
	{
		VA::Vector3 request;
		request.CopyFrom( Encode( v3TranslationSpeed ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumShiftSpeed( &context, request, &reply ) );
	};

	VAVec3 GetHomogeneousMediumShiftSpeed( ) const override
	{
		google::protobuf::Empty request;

		VA::Vector3 reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumShiftSpeed( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetHomogeneousMediumParameters( const CVAStruct& oParams ) override
	{
		VA::HomogeneousMediumParametersRequest request;
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );


		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetHomogeneousMediumParameters( &context, request, &reply ) );
	};

	CVAStruct GetHomogeneousMediumParameters( const CVAStruct& oArgs ) override
	{
		VA::HomogeneousMediumParametersRequest request;
		request.mutable_parameters( )->CopyFrom( Encode( oArgs ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetHomogeneousMediumParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	// Scene
	std::string CreateScene( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		return {};
	};

	void GetSceneIDs( std::vector<std::string>& vsIDs ) const override
	{
	};

	void SetSceneName( const std::string& sID, const std::string& sName ) override
	{
	};

	std::string GetSceneName( const std::string& sID ) const override
	{
		return {};
	};

	CVASceneInfo GetSceneInfo( const std::string& sID ) const override
	{
		return {};
	};

	void SetSceneEnabled( const std::string& sID, const bool bEnabled = true ) override
	{
	};

	bool GetSceneEnabled( const std::string& sID ) const override
	{
		return {};
	};

	// SoundPortal
	int CreateSoundPortal( const std::string& sName = "" ) override
	{
		return {};
	};

	void GetSoundPortalIDs( std::vector<int>& viIDs ) override
	{
	};

	std::string GetSoundPortalName( const int iID ) const override
	{
		return {};
	};

	void SetSoundPortalName( const int iPortalID, const std::string& sName ) override
	{
	};

	CVAStruct GetSoundPortalParameters( const int iPortalID, const CVAStruct& oArgs ) const override
	{
		return {};
	};

	void SetSoundPortalParameters( const int iPortalID, const CVAStruct& oParams ) override
	{
	};

	CVASoundPortalInfo GetSoundPortalInfo( const int iID ) const override
	{
		return {};
	};

	void SetSoundPortalMaterial( const int iSoundPortalID, const int iMaterialID ) override
	{
	};

	int GetSoundPortalMaterial( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalNextPortal( const int iSoundPortalID, const int iNextSoundPortalID ) override
	{
	};

	int GetSoundPortalNextPortal( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalSoundReceiver( const int iSoundPortalID, const int iSoundReceiverID )
	{
	};

	int GetSoundPortalSoundReceiver( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalSoundSource( const int iSoundPortalID, const int iSoundSourceID ) override
	{
	};

	int GetSoundPortalSoundSource( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalPosition( const int iSoundPortalID, const VAVec3& v3Pos ) override
	{
	};

	VAVec3 GetSoundPortalPosition( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalOrientation( const int iSoundPortalID, const VAQuat& qOrient ) override
	{
	};

	VAQuat GetSoundPortalOrientation( const int iSoundPortalID ) const override
	{
		return {};
	};

	void SetSoundPortalEnabled( const int iSoundPortalID, const bool bEnabled = true ) override
	{
	};

	bool GetSoundPortalEnabled( const int iSoundPortalID ) const override
	{
		return {};
	};

	// InputOutput
	bool GetInputMuted( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetInputMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetInputMuted( const bool bMuted = true ) override
	{
		VA::SetInputMutedRequest request;
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetInputMuted( &context, request, &reply ) );
	}

	double GetInputGain( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetInputGain( &context, request, &reply ) );

		return reply.value( );
	}

	void SetInputGain( const double dGain ) override
	{
		VA::SetInputGainRequest request;
		request.set_gain( dGain );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetInputGain( &context, request, &reply ) );
	}

	double GetOutputGain( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetOutputGain( &context, request, &reply ) );

		return reply.value( );
	}

	void SetOutputGain( const double dGain ) override
	{
		VA::SetOutputGainRequest request;
		request.set_gain( dGain );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetOutputGain( &context, request, &reply ) );
	};

	bool GetOutputMuted( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetOutputMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetOutputMuted( const bool bMuted = true ) override
	{
		VA::SetOutputMutedRequest request;
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetOutputMuted( &context, request, &reply ) );
	};

	int GetGlobalAuralizationMode( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetGlobalAuralizationMode( &context, request, &reply ) );

		return reply.value( );
	}

	void SetGlobalAuralizationMode( const int iAuralizationMode ) override
	{
		VA::SetGlobalAuralizationModeRequest request;
		request.set_mode( iAuralizationMode );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetGlobalAuralizationMode( &context, request, &reply ) );
	}

	double GetCoreClock( ) const override
	{
		google::protobuf::Empty request;

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetCoreClock( &context, request, &reply ) );

		return reply.value( );
	};

	void SetCoreClock( const double dSeconds ) override
	{
		VA::SetCoreClockRequest request;
		request.set_time( dSeconds );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetCoreClock( &context, request, &reply ) );
	};

	// Error VAStub has no member Substitute Macros
	std::string SubstituteMacros( const std::string& sStr ) const { VA_EXCEPT_NOT_IMPLEMENTED; };

	// Rendering
	void GetRenderingModules( std::vector<CVAAudioRendererInfo>& voRenderer, const bool bFilterEnabled = true ) const override
	{
		VA::GetRenderingModulesRequest request;
		request.set_only_enabled( bFilterEnabled );

		VA::AudioRendererInfos reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetRenderingModules( &context, request, &reply ) );

		voRenderer.clear( );

		for( auto&& renderingModule: reply.audio_renderer_infos( ) )
		{
			CVAAudioRendererInfo info;

			info.sID                      = renderingModule.name( );
			info.sClass                   = renderingModule.class_( );
			info.sDescription             = renderingModule.description( );
			info.bEnabled                 = renderingModule.enabled( );
			info.bOutputDetectorEnabled   = renderingModule.output_detector_enabled( );
			info.bOutputRecordingEnabled  = renderingModule.output_recording_enabled( );
			info.sOutputRecordingFilePath = renderingModule.output_recording_file_path( );
			info.oParams                  = Decode( renderingModule.parameters( ) );

			voRenderer.push_back( info );
		}
	};

	void SetRenderingModuleMuted( const std::string& sModuleID, const bool bMuted = true ) override
	{
		VA::SetRenderingModuleMutedRequest request;
		request.set_rendering_module_id( sModuleID );
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetRenderingModuleMuted( &context, request, &reply ) );
	};

	bool GetRenderingModuleMuted( const std::string& sModuleID ) const override
	{
		VA::GetRenderingModuleMutedRequest request;
		request.set_rendering_module_id( sModuleID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetRenderingModuleMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetRenderingModuleAuralizationMode( const std::string& sModuleID, const int iAuraMode ) override
	{
		VA::SetRenderingModuleAuralizationModeRequest request;
		request.set_rendering_module_id( sModuleID );
		request.set_mode( iAuraMode );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetRenderingModuleAuralizationMode( &context, request, &reply ) );
	};

	int GetRenderingModuleAuralizationMode( const std::string& sModuleID ) const override
	{
		VA::GetRenderingModuleAuralizationModeRequest request;
		request.set_rendering_module_id( sModuleID );

		google::protobuf::Int32Value reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetRenderingModuleAuralizationMode( &context, request, &reply ) );

		return reply.value( );
	};

	void SetRenderingModuleParameters( const std::string& sID, const CVAStruct& oParams ) override
	{
		VA::SetRenderingModuleParametersRequest request;
		request.set_rendering_module_id( sID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetRenderingModuleParameters( &context, request, &reply ) );
	};

	CVAStruct GetRenderingModuleParameters( const std::string& sID, const CVAStruct& oParams ) const override
	{
		VA::GetRenderingModuleParametersRequest request;
		request.set_rendering_module_id( sID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetRenderingModuleParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	void SetRenderingModuleGain( const std::string& sModuleID, const double dGain ) override
	{
		VA::SetRenderingModuleGainRequest request;
		request.set_rendering_module_id( sModuleID );
		request.set_gain( dGain );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetRenderingModuleGain( &context, request, &reply ) );
	};

	double GetRenderingModuleGain( const std::string& sModuleID ) const override
	{
		VA::GetRenderingModuleGainRequest request;
		request.set_rendering_module_id( sModuleID );

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetRenderingModuleGain( &context, request, &reply ) );

		return reply.value( );
	};

	// Reproduction
	void GetReproductionModules( std::vector<CVAAudioReproductionInfo>& voRepros, const bool bFilterEnabled = true ) const override
	{
		VA::GetReproductionModulesRequest request;
		request.set_only_enabled( bFilterEnabled );

		VA::AudioReproductionInfos reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetReproductionModules( &context, request, &reply ) );

		voRepros.clear( );

		for( auto&& reproductionModule: reply.audio_reproduction_infos( ) )
		{
			CVAAudioReproductionInfo info;

			info.sID                      = reproductionModule.id( );
			info.sClass                   = reproductionModule.class_( );
			info.sDescription             = reproductionModule.description( );
			info.bEnabled                 = reproductionModule.enabled( );
			info.bInputDetectorEnabled    = reproductionModule.input_detector_enabled( );
			info.bInputRecordingEnabled   = reproductionModule.input_recording_enabled( );
			info.sInputRecordingFilePath  = reproductionModule.input_recording_file_path( );
			info.bOutputDetectorEnabled   = reproductionModule.bool_detector_enabled( );
			info.bOutputRecordingEnabled  = reproductionModule.bool_recording_enabled( );
			info.sOutputRecordingFilePath = reproductionModule.bool_recording_file_path( );
			info.oParams                  = Decode( reproductionModule.parameters( ) );

			voRepros.push_back( info );
		}
	}

	void SetReproductionModuleMuted( const std::string& sModuleID, const bool bMuted = true ) override
	{
		VA::SetReproductionModuleMutedRequest request;
		request.set_reproduction_module_id( sModuleID );
		request.set_muted( bMuted );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetReproductionModuleMuted( &context, request, &reply ) );
	};

	bool GetReproductionModuleMuted( const std::string& sModuleID ) const override
	{
		VA::GetReproductionModuleMutedRequest request;
		request.set_reproduction_module_id( sModuleID );

		google::protobuf::BoolValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetReproductionModuleMuted( &context, request, &reply ) );

		return reply.value( );
	};

	void SetReproductionModuleGain( const std::string& sModuleID, const double dGain ) override
	{
		VA::SetReproductionModuleGainRequest request;
		request.set_reproduction_module_id( sModuleID );
		request.set_gain( dGain );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetReproductionModuleGain( &context, request, &reply ) );
	};

	double GetReproductionModuleGain( const std::string& sModuleID ) const override
	{
		VA::GetReproductionModuleGainRequest request;
		request.set_reproduction_module_id( sModuleID );

		google::protobuf::DoubleValue reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetReproductionModuleGain( &context, request, &reply ) );

		return reply.value( );
	};

	void SetReproductionModuleParameters( const std::string& sID, const CVAStruct& oParams ) override
	{
		VA::SetReproductionModuleParametersRequest request;
		request.set_reproduction_module_id( sID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		google::protobuf::Empty reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->SetReproductionModuleParameters( &context, request, &reply ) );
	};

	CVAStruct GetReproductionModuleParameters( const std::string& sID, const CVAStruct& oParams ) const override
	{
		VA::GetReproductionModuleParametersRequest request;
		request.set_reproduction_module_id( sID );
		request.mutable_parameters( )->CopyFrom( Encode( oParams ) );

		VA::Struct reply;
		grpc::ClientContext context;
		handleRPCStatus( m_pStub->GetReproductionModuleParameters( &context, request, &reply ) );

		return Decode( reply );
	};

	// Acoustic Material
	int CreateAcousticMaterial( const CVAAcousticMaterial& oMaterial, const std::string& sName = "" ) override
	{
		return {};
	};

	int CreateAcousticMaterialFromParameters( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		return {};
	};

	bool DeleteAcousticMaterial( const int iID ) override
	{
		return {};
	};

	CVAAcousticMaterial GetAcousticMaterialInfo( const int iID ) const override
	{
		return {};
	};

	void GetAcousticMaterialInfos( std::vector<CVAAcousticMaterial>& voDest ) const override
	{
	};

	void SetAcousticMaterialName( const int iID, const std::string& sName ) override
	{
	};

	std::string GetAcousticMaterialName( const int iID ) const override
	{
		return {};
	};

	CVAStruct GetAcousticMaterialParameters( const int iID, const CVAStruct& oParams ) const override
	{
		return {};
	};

	void SetAcousticMaterialParameters( const int iID, const CVAStruct& oParams ) override
	{
	};

	// Geometry Mesh
	int CreateGeometryMesh( const CVAGeometryMesh& oMesh, const std::string& sName = "" ) override
	{
		return {};
	};

	int CreateGeometryMeshFromParameters( const CVAStruct& oParams, const std::string& sName = "" ) override
	{
		return {};
	};

	bool DeleteGeometryMesh( const int iID ) override
	{
		return {};
	};

	CVAGeometryMesh GetGeometryMesh( const int iID ) const override
	{
		return {};
	};

	void GetGeometryMeshIDs( std::vector<int>& viIDs ) const override
	{
	};

	void SetGeometryMeshEnabled( const int iID, const bool bEnabled = true ) override
	{
	};

	bool GetGeometryMeshEnabled( const int iID ) const override
	{
		return {};
	};

	void SetGeometryMeshName( const int iID, const std::string& sName ) override
	{
	};

	std::string GetGeometryMeshName( const int iID ) const override
	{
		return {};
	};

	CVAStruct GetGeometryMeshParameters( const int iID, const CVAStruct& oParams ) const override
	{
		return {};
	};

	void SetGeometryMeshParameters( const int iID, const CVAStruct& oParams ) override
	{
	};


protected:
	void handleRPCStatus( const grpc::Status& rpcStatus ) const
	{
		// TODO maybe only rethrow certain exceptions aka gRPC error states
		if( rpcStatus.ok( ) )
			return;

		if( m_pParent->GetExceptionHandlingMode( ) == ExceptionHandlingMode::EXC_CLIENT_THROW )
			throw Decode( rpcStatus );
	}

	CVANetClientImpl* m_pParent;

	std::unique_ptr<VA::VA::Stub> m_pStub;

	std::thread m_tEventHandlerThread;

	std::set<IVAEventHandler*> m_vEventHandlers;

	std::atomic_bool m_bShutdownRequest { false };

	grpc::ClientContext m_oEventHandlingContext;
};

#endif // IW_VANET_CLIENT_NETWORKED_CORE