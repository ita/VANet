/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_CLIENT_IMPL
#define IW_VANET_CLIENT_IMPL

#include <VANetClient.h>
#include <grpcpp/grpcpp.h>
#include <list>
#include <memory>

class IVAEventHandler;

/**
 * Thread-Safe implementation
 */

class CVANetClientImpl : public IVANetClient
{
public:
	CVANetClientImpl( );

	~CVANetClientImpl( ) override;

	[[nodiscard]] IVAInterface* GetCoreInstance( ) const override;

	[[nodiscard]] bool IsConnected( ) const override;

	[[nodiscard]] std::string GetServerAddress( ) const override;

	[[nodiscard]] bool Initialize( const std::string& sServerAddress, const int iServerPort,
	                               const ExceptionHandlingMode eExceptionHandlingMode = ExceptionHandlingMode::EXC_CLIENT_THROW ) override;

	[[nodiscard]] bool Disconnect( ) override;

	void SetExceptionHandlingMode( const ExceptionHandlingMode eExceptionHandlingMode ) override;

	[[nodiscard]] ExceptionHandlingMode GetExceptionHandlingMode( ) const override;

protected:
	class CNetworkedVACore;
	std::unique_ptr<CNetworkedVACore> m_pVACore;

	std::string m_sServerIP;

	ExceptionHandlingMode m_eExceptionHandlingMode { ExceptionHandlingMode::EXC_CLIENT_THROW };

	std::shared_ptr<grpc::Channel> m_pChannel;
};

#endif // IW_VANET_CLIENT_IMPL
