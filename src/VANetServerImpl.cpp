/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VANetServerImpl.h"

CVANetServerImpl::~CVANetServerImpl( )
{
	// Todo: Finalize returns
	Finalize( );
}

bool CVANetServerImpl::Initialize( const std::string& sInterface, const int iServerPort, const int iFreePortMin, const int iFreePortMax, const int iMaxNumClients )
{
	assert( iServerPort > 0 );
	assert( ( iFreePortMin > 0 ) && ( iFreePortMax > 0 ) && ( iFreePortMin <= iFreePortMax ) );

	// Create the port list
	tPortList liFreePorts;
	liFreePorts.push_back( tPortRange( iFreePortMin, iFreePortMax ) );

	return Initialize( sInterface, iServerPort, liFreePorts, iMaxNumClients );
}

bool CVANetServerImpl::Initialize( const std::string& sInterface, const int iServerPort, const tPortList& liFreePorts, const int iMaxNumClients )
{
	m_sServerAddress = sInterface;
	m_iServerPort    = iServerPort;

	m_pService = std::make_unique<VANetServiceImplementation>( this );
	m_pService->SetRealVACore( m_pRealCore );

	grpc::EnableDefaultHealthCheckService( true );
	grpc::reflection::InitProtoReflectionServerBuilderPlugin( );
	grpc::ServerBuilder builder;

	const std::string serverAddress( m_sServerAddress + ":" + std::to_string( m_iServerPort ) );

	// Listen on the given address without any authentication mechanism.
	builder.AddListeningPort( serverAddress, grpc::InsecureServerCredentials( ) );

	// Register "service" as the instance through which we'll communicate with clients.
	builder.RegisterService( m_pService.get( ) );

	// Assemble the server.
	m_pServer = builder.BuildAndStart( );

	m_tServingThread = std::thread( [&]( ) { m_pServer->Wait( ); } );

	return true;
}

bool CVANetServerImpl::Finalize( )
{
#ifdef VANET_SERVER_VERBOSE
	std::cout << "VANetServer: Shutting down" << std::endl;
#endif

	m_pService->ShutdownEventHandling( );

	m_pServer->Shutdown( std::chrono::system_clock::now() + std::chrono::milliseconds( 500 ) );
	m_tServingThread.join( );

	m_pService.reset( );
	m_pServer.reset( );

	return true;
}

void CVANetServerImpl::Reset( )
{
#ifdef VANET_SERVER_VERBOSE
	std::cout << "VANetServer: Resetting" << std::endl;
#endif

	// while( m_vecClients.empty( ) == false )
	// {
	// 	RemoveClient( 0, CVANetNetworkProtocol::VA_NET_SERVER_RESET );
	// }

	/* @todo: reset core? */
}

std::string CVANetServerImpl::GetServerAddress( ) const
{
	return m_sServerAddress;
}

int CVANetServerImpl::GetServerPort( ) const
{
	return m_iServerPort;
}

IVAInterface* CVANetServerImpl::GetCoreInstance( ) const
{
	return m_pRealCore;
}

void CVANetServerImpl::SetCoreInstance( IVAInterface* pCore )
{
	m_pRealCore = pCore;
	if( m_pService )
		m_pService->SetRealVACore( pCore );
}

bool CVANetServerImpl::IsClientConnected( ) const
{
	return !m_vConnections.empty( );
}

int CVANetServerImpl::GetNumConnectedClients( ) const
{
	return m_vConnections.size( );
}

std::string CVANetServerImpl::GetClientHostname( const int iClientIndex ) const
{
	std::lock_guard<std::mutex> guard( m_mConnectionAccessMutex );
	if( iClientIndex < 0 || iClientIndex >= m_vConnections.size( ) )
		return "";

	return m_vConnections.at( iClientIndex )->peerUri;
}

CVANetServerImpl::Connection* CVANetServerImpl::AddConnection( std::unique_ptr<Connection>&& connection )
{
	std::lock_guard<std::mutex> guard( m_mConnectionAccessMutex );
	m_vConnections.emplace_back( std::move( connection ) );
	return m_vConnections.back( ).get( );
}

void CVANetServerImpl::RemoveConnection( Connection* connection )
{
	std::lock_guard<std::mutex> guard( m_mConnectionAccessMutex );
	m_vConnections.erase( std::remove_if( m_vConnections.begin( ), m_vConnections.end( ),
	                                      [connectionToDelete = connection]( const std::unique_ptr<Connection>& connection )
	                                      { return connection.get( ) == connectionToDelete; } ),
	                      m_vConnections.end( ) );
}
