/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_UTILS
#define IW_VANET_UTILS

#include <VA.h>
#include <va.grpc.pb.h>
#include <va_exception.pb.h>

inline CVAStruct::iterator begin( CVAStruct& s )
{
	return s.Begin( );
}

inline CVAStruct::iterator end( CVAStruct& s )
{
	return s.End( );
}

inline CVAStruct::const_iterator begin( const CVAStruct& s )
{
	return s.Begin( );
}

inline CVAStruct::const_iterator end( const CVAStruct& s )
{
	return s.End( );
}

VA::Value EncodeValue( const CVAStructValue& );

inline VA::Struct Encode( const CVAStruct& data )
{
	VA::Struct out;

	for( const auto& [key, value]: data )
	{
		out.mutable_fields( )->emplace( key, EncodeValue( value ) );
	}
	return out;
}

inline VA::SampleBuffer EncodeBuffer( const CVASampleBuffer& buffer )
{
	VA::SampleBuffer out;

	for( auto&& sample: buffer.vfSamples )
	{
		out.add_samples( sample );
	}

	return out;
}

inline VA::Value EncodeValue( const CVAStructValue& value )
{
	VA::Value out;

	switch( value.GetDatatype( ) )
	{
		case CVAStructValue::BOOL:
			out.set_bool_value( value );
			break;
		case CVAStructValue::INT:
			out.set_integer_value( value );
			break;
		case CVAStructValue::DOUBLE:
			out.set_double_value( value );
			break;
		case CVAStructValue::STRING:
			out.set_string_value( value );
			break;
		case CVAStructValue::STRUCT:
			out.mutable_struct_value( )->CopyFrom( Encode( value.GetStruct( ) ) );
			break;
		case CVAStructValue::DATA:
			out.set_data_value( std::string( static_cast<const char*>( value.GetData( ) ), value.GetDataSize( ) ) );
			break;
		case CVAStructValue::SAMPLEBUFFER:
		{
			const CVASampleBuffer& samplesBuffer( value );
			out.mutable_buffer_value( )->CopyFrom( EncodeBuffer( samplesBuffer ) );
		}
		break;
		default:
			VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new data type was added" );
	}

	return out;
}

CVAStructValue DecodeValue( const VA::Value& );

inline CVAStruct Decode( const VA::Struct& data )
{
	CVAStruct out;

	for( const auto& [key, value]: data.fields( ) )
	{
		out[key] = DecodeValue( value );
	}

	return out;
}

inline CVASampleBuffer DecodeBuffer( const VA::SampleBuffer& buffer )
{
	CVASampleBuffer out( buffer.samples_size( ), false );

	for( size_t i = 0; i < buffer.samples_size( ); ++i )
	{
		out.vfSamples.at( i ) = buffer.samples( ).Get( i );
	}

	return out;
}

inline CVAStructValue DecodeValue( const VA::Value& value )
{
	if( value.has_bool_value( ) )
		return CVAStructValue( value.bool_value( ) );
	if( value.has_integer_value( ) )
		return CVAStructValue( value.integer_value( ) );
	if( value.has_double_value( ) )
		return CVAStructValue( value.double_value( ) );
	if( value.has_string_value( ) )
		return CVAStructValue( value.string_value( ) );
	if( value.has_struct_value( ) )
		return CVAStructValue( Decode( value.struct_value( ) ) );
	if( value.has_data_value( ) )
		// Casting const'ness away, this is very bad! In this case it can be accepted since the data is passed to a memcopy.
		return CVAStructValue( const_cast<char*>( value.data_value( ).data( ) ), static_cast<int>( value.data_value( ).size( ) ) );
	if( value.has_buffer_value( ) )
		return CVAStructValue( DecodeBuffer( value.buffer_value( ) ) );

	VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new data type was added" );
}

inline VA::Vector3 Encode( const VAVec3& vec )
{
	VA::Vector3 out;

	out.set_x( vec.x );
	out.set_y( vec.y );
	out.set_z( vec.z );

	return out;
}

inline VAVec3 Decode( const VA::Vector3& vec )
{
	VAVec3 out;

	out.x = vec.x( );
	out.y = vec.y( );
	out.z = vec.z( );

	return out;
}

inline VA::Quaternion Encode( const VAQuat& quaternion )
{
	VA::Quaternion out;

	out.set_x( quaternion.x );
	out.set_y( quaternion.y );
	out.set_z( quaternion.z );
	out.set_w( quaternion.w );

	return out;
}

inline VAQuat Decode( const VA::Quaternion& quaternion )
{
	VAQuat out;

	out.x = quaternion.x( );
	out.y = quaternion.y( );
	out.z = quaternion.z( );
	out.w = quaternion.w( );

	return out;
}

inline grpc::Status Encode( const CVAException& exception )
{
	VA::Exception rpcException;

	rpcException.set_error_message( exception.GetErrorMessage( ) );

	VA::Exception::ErrorCode errorCode;
	switch( exception.GetErrorCode( ) )
	{
		case CVAException::UNSPECIFIED:
			errorCode = VA::Exception_ErrorCode_UNSPECIFIED;
			break;
		case CVAException::INVALID_PARAMETER:
			errorCode = VA::Exception_ErrorCode_INVALID_PARAMETER;
			break;
		case CVAException::INVALID_ID:
			errorCode = VA::Exception_ErrorCode_INVALID_ID;
			break;
		case CVAException::MODAL_ERROR:
			errorCode = VA::Exception_ErrorCode_MODAL_ERROR;
			break;
		case CVAException::RESOURCE_IN_USE:
			errorCode = VA::Exception_ErrorCode_RESOURCE_IN_USE;
			break;
		case CVAException::FILE_NOT_FOUND:
			errorCode = VA::Exception_ErrorCode_FILE_NOT_FOUND;
			break;
		case CVAException::NETWORK_ERROR:
			errorCode = VA::Exception_ErrorCode_NETWORK_ERROR;
			break;
		case CVAException::PROTOCOL_ERROR:
			errorCode = VA::Exception_ErrorCode_PROTOCOL_ERROR;
			break;
		case CVAException::NOT_IMPLEMENTED:
			errorCode = VA::Exception_ErrorCode_NOT_IMPLEMENTED;
			break;
		default:
			assert( 0 );
	}
	rpcException.set_error_code( errorCode );

	return grpc::Status( grpc::StatusCode::INTERNAL, exception.GetErrorMessage( ), rpcException.SerializeAsString( ) );
}

inline CVAException Decode( const grpc::Status& status )
{
	VA::Exception exception;

	if( !status.error_details( ).empty( ) && exception.ParseFromString( status.error_details( ) ) )
	{
		CVAException::ErrorCode errorCode { CVAException::UNSPECIFIED };
		switch( exception.error_code( ) )
		{
			case VA::Exception_ErrorCode_UNSPECIFIED:
				errorCode = CVAException::UNSPECIFIED;
				break;
			case VA::Exception_ErrorCode_INVALID_PARAMETER:
				errorCode = CVAException::INVALID_PARAMETER;
				break;
			case VA::Exception_ErrorCode_INVALID_ID:
				errorCode = CVAException::INVALID_ID;
				break;
			case VA::Exception_ErrorCode_MODAL_ERROR:
				errorCode = CVAException::MODAL_ERROR;
				break;
			case VA::Exception_ErrorCode_RESOURCE_IN_USE:
				errorCode = CVAException::RESOURCE_IN_USE;
				break;
			case VA::Exception_ErrorCode_FILE_NOT_FOUND:
				errorCode = CVAException::FILE_NOT_FOUND;
				break;
			case VA::Exception_ErrorCode_NETWORK_ERROR:
				errorCode = CVAException::NETWORK_ERROR;
				break;
			case VA::Exception_ErrorCode_PROTOCOL_ERROR:
				errorCode = CVAException::PROTOCOL_ERROR;
				break;
			case VA::Exception_ErrorCode_NOT_IMPLEMENTED:
				errorCode = CVAException::NOT_IMPLEMENTED;
				break;
			default:
				assert( 0 );
		}
		return CVAException( errorCode, exception.error_message( ) );
	}

	CVAException::ErrorCode errorCode { CVAException::UNSPECIFIED };
	switch( status.error_code( ) )
	{
		case grpc::StatusCode::CANCELLED:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::UNKNOWN:
			errorCode = CVAException::UNSPECIFIED;
			break;
		case grpc::StatusCode::INVALID_ARGUMENT:
			errorCode = CVAException::INVALID_PARAMETER;
			break;
		case grpc::StatusCode::DEADLINE_EXCEEDED:
			errorCode = CVAException::NETWORK_ERROR;
			break;
		case grpc::StatusCode::NOT_FOUND:
			errorCode = CVAException::NOT_IMPLEMENTED;
			break;
		case grpc::StatusCode::ALREADY_EXISTS:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::PERMISSION_DENIED:
			errorCode = CVAException::NETWORK_ERROR;
			break;
		case grpc::StatusCode::UNAUTHENTICATED:
			errorCode = CVAException::NETWORK_ERROR;
			break;
		case grpc::StatusCode::RESOURCE_EXHAUSTED:
			errorCode = CVAException::RESOURCE_IN_USE;
			break;
		case grpc::StatusCode::FAILED_PRECONDITION:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::ABORTED:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::OUT_OF_RANGE:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::UNIMPLEMENTED:
			errorCode = CVAException::NOT_IMPLEMENTED;
			break;
		case grpc::StatusCode::INTERNAL:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::UNAVAILABLE:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		case grpc::StatusCode::DATA_LOSS:
			errorCode = CVAException::PROTOCOL_ERROR;
			break;
		default:
			assert( 0 );
	}

	return CVAException( errorCode, status.error_message( ) );
}

template<typename T>
std::vector<T> Decode( const google::protobuf::RepeatedField<T>& data )
{
	return std::vector<T>( data.begin( ), data.end( ) );
}

template<typename T>
google::protobuf::RepeatedField<T> Encode( const std::vector<T>& data )
{
	return google::protobuf::RepeatedField<T>( data.begin( ), data.end( ) );
}

inline CVAProgress Decode( const VA::Progress& progress )
{
	CVAProgress out;

	out.iCurrentStep = progress.current_step( );
	out.iMaxStep     = progress.max_step( );
	out.sAction      = progress.action( );
	out.sSubaction   = progress.sub_action( );

	return out;
}

inline VA::Progress Encode( const CVAProgress& progress )
{
	VA::Progress out;

	out.set_current_step( progress.iCurrentStep );
	out.set_max_step( progress.iMaxStep );
	out.set_action( progress.sAction );
	out.set_sub_action( progress.sSubaction );

	return out;
}

inline CVAEvent Decode( const VA::Event& vaEvent )
{
	CVAEvent out;

	out.pSender           = reinterpret_cast<IVAInterface*>( vaEvent.sender( ) );
	out.iEventID          = vaEvent.event_id( );
	out.iEventType        = vaEvent.event_type( );
	out.iObjectID         = vaEvent.int_object_id( );
	out.sObjectID         = vaEvent.string_object_id( );
	out.iParamID          = vaEvent.parameter_id( );
	out.sParam            = vaEvent.parameter( );
	out.iIndex            = vaEvent.index( );
	out.iAuralizationMode = vaEvent.auralization_mode( );
	out.dVolume           = vaEvent.volume( );
	out.dState            = vaEvent.state( );
	out.bMuted            = vaEvent.muted( );
	out.sName             = vaEvent.name( );
	out.sFilePath         = vaEvent.file_path( );
	out.vPos              = Decode( vaEvent.position( ) );
	out.vView             = Decode( vaEvent.view( ) );
	out.vUp               = Decode( vaEvent.up( ) );
	out.qHATO             = Decode( vaEvent.head_above_torso_orientation( ) );
	out.oOrientation      = Decode( vaEvent.orientation( ) );
	out.vfInputPeaks      = Decode( vaEvent.input_peaks( ) );
	out.vfInputRMSs       = Decode( vaEvent.input_rmss( ) );
	out.vfOutputPeaks     = Decode( vaEvent.output_peaks( ) );
	out.vfOutputRMSs      = Decode( vaEvent.output_rmss( ) );
	out.fSysLoad          = vaEvent.system_load( );
	out.fDSPLoad          = vaEvent.dsp_load( );
	out.dCoreClock        = vaEvent.core_clock( );
	out.oProgress         = Decode( vaEvent.progress( ) );
	out.oPrototypeParams  = Decode( vaEvent.prototype_parameters( ) );

	return out;
}

inline VA::Event Encode( const CVAEvent& vaEvent )
{
	VA::Event out;

	out.set_sender( reinterpret_cast<uint64_t>( vaEvent.pSender ) );
	out.set_event_id( vaEvent.iEventID );
	out.set_event_type( vaEvent.iEventType );
	out.set_int_object_id( vaEvent.iObjectID );
	out.set_string_object_id( vaEvent.sObjectID );
	out.set_parameter_id( vaEvent.iParamID );
	out.set_parameter( vaEvent.sParam );
	out.set_index( vaEvent.iIndex );
	out.set_auralization_mode( vaEvent.iAuralizationMode );
	out.set_volume( vaEvent.dVolume );
	out.set_state( vaEvent.dState );
	out.set_muted( vaEvent.bMuted );
	out.set_name( vaEvent.sName );
	out.set_file_path( vaEvent.sFilePath );
	out.mutable_position( )->CopyFrom( Encode( vaEvent.vPos ) );
	out.mutable_view( )->CopyFrom( Encode( vaEvent.vView ) );
	out.mutable_up( )->CopyFrom( Encode( vaEvent.vUp ) );
	out.mutable_head_above_torso_orientation( )->CopyFrom( Encode( vaEvent.qHATO ) );
	out.mutable_orientation( )->CopyFrom( Encode( vaEvent.oOrientation ) );
	out.mutable_input_peaks( )->CopyFrom( Encode( vaEvent.vfInputPeaks ) );
	out.mutable_input_rmss( )->CopyFrom( Encode( vaEvent.vfInputRMSs ) );
	out.mutable_output_peaks( )->CopyFrom( Encode( vaEvent.vfOutputPeaks ) );
	out.mutable_output_rmss( )->CopyFrom( Encode( vaEvent.vfOutputRMSs ) );
	out.set_system_load( vaEvent.fSysLoad );
	out.set_dsp_load( vaEvent.fDSPLoad );
	out.set_core_clock( vaEvent.dCoreClock );
	out.mutable_progress( )->CopyFrom( Encode( vaEvent.oProgress ) );
	out.mutable_prototype_parameters( )->CopyFrom( Encode( vaEvent.oPrototypeParams ) );

	return out;
}


inline CVASoundSourceInfo Decode( const VA::SoundSourceInfo& SoundSourceInfo )
{
	CVASoundSourceInfo out;


	out.iID             = SoundSourceInfo.id( );
	out.sName           = SoundSourceInfo.name( );
	out.dSpoundPower    = SoundSourceInfo.sound_power( );
	out.bMuted          = SoundSourceInfo.muted( );
	out.sSignalSourceID = SoundSourceInfo.signal_source_id( );
	out.sExplicitRendererID = SoundSourceInfo.explicit_renderer_id( );

	return out;
}


inline VA::SoundSourceInfo Encode( const CVASoundSourceInfo& SoundSourceInfo )
{
	VA::SoundSourceInfo out;


	out.set_id( SoundSourceInfo.iID );
	out.set_name( SoundSourceInfo.sName );
	out.set_sound_power( SoundSourceInfo.dSpoundPower );
	out.set_muted( SoundSourceInfo.bMuted );
	out.set_signal_source_id( SoundSourceInfo.sSignalSourceID );
	out.set_explicit_renderer_id( SoundSourceInfo.sExplicitRendererID );

	return out;
}


inline CVASoundReceiverInfo Decode( const VA::SoundReceiverInfo& SoundReceiverInfo )
{
	CVASoundReceiverInfo out;

	out.iID            = SoundReceiverInfo.id( );
	out.sName          = SoundReceiverInfo.name( );
	out.sExplicitRendererID = SoundReceiverInfo.explicit_renderer_id( );

	return out;
}


inline VA::SoundReceiverInfo Encode( const CVASoundReceiverInfo& SoundReceiverInfo )
{
	VA::SoundReceiverInfo out;

	out.set_id( SoundReceiverInfo.iID );
	out.set_name( SoundReceiverInfo.sName );
	out.set_explicit_renderer_id( SoundReceiverInfo.sExplicitRendererID );

	return out;
}

inline CVADirectivityInfo Decode( const VA::DirectivityInfo& directivityInfo )
{
	CVADirectivityInfo out;

	out.iID         = directivityInfo.id( );
	out.iClass      = directivityInfo.class_( );
	out.sName       = directivityInfo.name( );
	out.sDesc       = directivityInfo.description( );
	out.iReferences = directivityInfo.num_references( );
	out.oParams     = Decode( directivityInfo.parameters( ) );

	return out;
}


inline VA::DirectivityInfo Encode( const CVADirectivityInfo& directivityInfo )
{
	VA::DirectivityInfo out;

	out.set_id( directivityInfo.iID );
	out.set_class_( VA::DirectivityInfo_Class( directivityInfo.iClass ) );
	out.set_name( directivityInfo.sName );
	out.set_description( directivityInfo.sDesc );
	out.set_num_references( directivityInfo.iReferences );
	out.mutable_parameters( )->CopyFrom( Encode( directivityInfo.oParams ) );

	return out;
}

inline std::vector<int> Decode( const VA::IntIdVector& Identifiers )
{
	std::vector<int> result( Identifiers.ids( ).begin( ), Identifiers.ids( ).end( ) );
	return result;
}

inline VA::IntIdVector Encode( const std::vector<int>& Identifiers )
{
	VA::IntIdVector result;
	*result.mutable_ids( ) = { Identifiers.begin( ), Identifiers.end( ) };
	return result;
}

inline CVASignalSourceInfo Decode( const VA::SignalSourceInfo& signalSourceInfo )
{
	CVASignalSourceInfo out;

	out.sID         = signalSourceInfo.id( );
	out.iType       = signalSourceInfo.type( );
	out.sName       = signalSourceInfo.name( );
	out.sDesc       = signalSourceInfo.description( );
	out.sState      = signalSourceInfo.state( );
	out.iReferences = signalSourceInfo.num_references( );


	return out;
}


inline VA::SignalSourceInfo Encode( const CVASignalSourceInfo& signalSourceInfo )
{
	VA::SignalSourceInfo out;

	out.set_id( signalSourceInfo.sID );
	out.set_type( VA::SignalSourceInfo_Type( signalSourceInfo.iType ) );
	out.set_name( signalSourceInfo.sName );
	out.set_description( signalSourceInfo.sDesc );
	out.set_state( signalSourceInfo.sState );
	out.set_num_references( signalSourceInfo.iReferences );

	return out;
}

#endif // IW_VANET_UTILS
