/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_SERVER_EVENT_HANDLER
#define IW_VANET_SERVER_EVENT_HANDLER

#include "VANetSerializationUtils.h"

#include <VA.h>

class CCoreEventHandler : public IVAEventHandler
{
public:
	CCoreEventHandler( grpc::ServerWriter<VA::Event>* writer ) : m_pWriter( writer ) { assert( m_pWriter ); }

	~CCoreEventHandler( ) override = default;

	void HandleVAEvent( const CVAEvent* pEvent ) override { m_pWriter->Write( Encode( *pEvent ) ); }

	grpc::ServerWriter<VA::Event>* m_pWriter;
};

#endif // IW_VANET_SERVER_EVENT_HANDLER
