/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_SERVER_IMPL
#define IW_VANET_SERVER_IMPL

#include "VANetServerEventHandler.h"
#include "VANetServiceImplementation.h"

#include <VANetServer.h>
#include <grpcpp/ext/proto_server_reflection_plugin.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>
#include <memory>
#include <string>
#include <vector>

class CVANetServerImpl : public IVANetServer
{
public:
	struct Connection
	{
		std::unique_ptr<CCoreEventHandler> pEventHandler { nullptr };
		std::string peerUri;
	};

	~CVANetServerImpl( ) override;
	[[nodiscard]] bool Initialize( const std::string& sInterface, const int iServerPort, const int iFreePortMin, const int iFreePortMax,
	                               const int iMaxNumClients ) override;
	[[nodiscard]] bool Initialize( const std::string& sInterface, const int iServerPort, const tPortList& liFreePorts, const int iMaxNumClients ) override;
	[[nodiscard]] bool Finalize( ) override;
	void Reset( ) override;
	[[nodiscard]] std::string GetServerAddress( ) const override;
	[[nodiscard]] int GetServerPort( ) const override;
	[[nodiscard]] IVAInterface* GetCoreInstance( ) const override;
	void SetCoreInstance( IVAInterface* pCore ) override;
	[[nodiscard]] bool IsClientConnected( ) const override;
	[[nodiscard]] int GetNumConnectedClients( ) const override;
	[[nodiscard]] std::string GetClientHostname( const int iClientIndex ) const override;

	Connection* AddConnection( std::unique_ptr<Connection>&& connection );
	void RemoveConnection( Connection* connection );

private:
	IVAInterface* m_pRealCore { nullptr };
	std::unique_ptr<VANetServiceImplementation> m_pService;
	std::unique_ptr<grpc::Server> m_pServer;
	std::thread m_tServingThread;

	std::string m_sServerAddress { "" };
	int m_iServerPort { -1 };


	std::vector<std::unique_ptr<Connection>> m_vConnections;
	mutable std::mutex m_mConnectionAccessMutex;
};

#endif // IW_VANET_SERVER_IMPL
