#include "VANetServiceImplementation.h"

#include "VANetSerializationUtils.h"
#include "VANetServerEventHandler.h"
#include "VANetServerImpl.h"

///
/// \brief Lippincott exception handling.
///
/// Implementation form [here](https://stackoverflow.com/questions/48036435/reusing-exception-handling-code-in-c) and
/// [here](https://stackoverflow.com/questions/3561659/how-can-i-abstract-out-a-repeating-try-catch-pattern-in-c).
///
/// \return a gRPC status that fits the thrown exception.
///
grpc::Status HandleException( ) noexcept
{
	try
	{
		throw;
	}
	catch( const CVAException& exception )
	{
		return Encode( exception );
	}
	catch( const std::exception& exception )
	{
		return grpc::Status( grpc::UNKNOWN, exception.what( ) );
	}
	catch( ... )
	{
		return grpc::Status( grpc::UNKNOWN, "Unknown exception was thrown." );
	}
}

///
/// \brief Exception handling macros.
/// \remark This is not the nicest implementation, but I haven't found a better one for this use-case
/// \note The return og a grpc::Status::OK has to be done after \p END_EXCEPTION_HANDLER.
///
#define BEGIN_EXCEPTION_HANDLER \
	try                         \
	{
#define END_EXCEPTION_HANDLER      \
	}                              \
	catch( ... )                   \
	{                              \
		return HandleException( ); \
	}


VANetServiceImplementation::VANetServiceImplementation( CVANetServerImpl* pParent ) : m_pParent( pParent )
{
	if( !m_pParent )
	{
		VA_EXCEPT2( CVAException::INVALID_PARAMETER, "Parent pointer was null" );
	}
}

VANetServiceImplementation::VANetServiceImplementation( IVAInterface* pCore, CVANetServerImpl* pParent ) : m_pCore( pCore ), m_pParent( pParent )
{
	if( !m_pCore )
	{
		VA_EXCEPT2( CVAException::INVALID_PARAMETER, "Core pointer was null" );
	}
	if( !m_pParent )
	{
		VA_EXCEPT2( CVAException::INVALID_PARAMETER, "Parent pointer was null" );
	}
}

VANetServiceImplementation::~VANetServiceImplementation( )
{
	m_bShutdownRequest = true;
}

void VANetServiceImplementation::SetRealVACore( IVAInterface* pCore )
{
	if( !pCore )
	{
		VA_EXCEPT2( CVAException::INVALID_PARAMETER, "Core pointer was null" );
	}

	m_pCore = pCore;
}

void VANetServiceImplementation::ShutdownEventHandling( )
{
	m_bShutdownRequest = true;

	// Wait for event handling to shut down.
	std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );
}

grpc::Status VANetServiceImplementation::AttachEventHandler( grpc::ServerContext* context, const google::protobuf::Empty* request, grpc::ServerWriter<VA::Event>* writer )
{
	BEGIN_EXCEPTION_HANDLER

	auto connection           = std::make_unique<CVANetServerImpl::Connection>( );
	connection->pEventHandler = std::make_unique<CCoreEventHandler>( writer );
	connection->peerUri       = context->peer( );

	m_pCore->AttachEventHandler( connection->pEventHandler.get( ) );

	const auto connectionPointer = m_pParent->AddConnection( std::move( connection ) );

	// TODO consider using a conditional variable here to potentially reduce CPU load
	while( !m_bShutdownRequest && !context->IsCancelled( ) )
	{
		std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
	}

	if( m_pCore )
	{
		m_pCore->DetachEventHandler( connectionPointer->pEventHandler.get( ) );
	}

	m_pParent->RemoveConnection( connectionPointer );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetVersionInfo( ::grpc::ServerContext* context, const ::google::protobuf::Empty* request, VA::VersionInfo* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAVersionInfo versionInfo;
	m_pCore->GetVersionInfo( &versionInfo );

	response->set_version( versionInfo.sVersion );
	response->set_release_date( versionInfo.sDate );
	response->set_property_flags( versionInfo.sFlags );
	response->set_comments( versionInfo.sComments );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetState( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::CoreState* response )
{
	BEGIN_EXCEPTION_HANDLER

	const auto state = m_pCore->GetState( );

	if( state == IVAInterface::VA_CORESTATE_CREATED )
		response->set_state( VA::CoreState_State_CREATED );
	else if( state == IVAInterface::VA_CORESTATE_READY )
		response->set_state( VA::CoreState_State_READY );
	else if( state == IVAInterface::VA_CORESTATE_FAIL )
		response->set_state( VA::CoreState_State_FAIL );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::Reset( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->Reset( );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetModules( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::VAModuleInfos* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<CVAModuleInfo> moduleInfos;
	m_pCore->GetModules( moduleInfos );

	for( auto&& info: moduleInfos )
	{
		const auto moduleInfo = response->add_module_infos( );

		moduleInfo->set_name( info.sName );
		moduleInfo->set_description( info.sDesc );
		moduleInfo->set_id( info.iID );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CallModule( grpc::ServerContext* context, const VA::CallModuleRequest* request, VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->CallModule( request->module_name( ), Decode( request->module_parameters( ) ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

// grpc::Status VANetServiceImplementation::AddSearchPath( grpc::ServerContext* context, const google::protobuf::StringValue* request,
//                                                        google::protobuf::BoolValue* response )
//{
//	response->set_value( m_pCore->AddSearchPath( request->value( ) ) );
//
//	return grpc::Status::OK;
//}

grpc::Status VANetServiceImplementation::GetSearchPaths( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSearchPaths( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetFileList( grpc::ServerContext* context, const VA::GetFileListRequest* request, VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetFileList( request->recursive( ), request->file_suffix_filter( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetCoreConfiguration( grpc::ServerContext* context, const VA::GetCoreConfigurationRequest* request, VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetCoreConfiguration( request->only_enabled( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHardwareConfiguration( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetHardwareConfiguration( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateDirectivityFromParameters( grpc::ServerContext* context, const VA::CreateDirectivityFromParametersRequest* request,

                                                                          google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateDirectivityFromParameters( Decode( request->parameters( ) ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::DeleteDirectivity( grpc::ServerContext* context, const VA::DeleteDirectivityRequest* request,
                                                            google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->DeleteDirectivity( request->directivity_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetDirectivityInfo( grpc::ServerContext* context, const VA::GetDirectivityInfoRequest* request, VA::DirectivityInfo* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetDirectivityInfo( request->directivity_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetDirectivityInfos( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::DirectivityInfosReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<CVADirectivityInfo> vdiDest;
	m_pCore->GetDirectivityInfos( vdiDest );

	for( int i = 0; i < vdiDest.size( ); i++ )
	{
		response->add_directivity_infos( )->CopyFrom( Encode( vdiDest.at( i ) ) );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetDirectivityName( grpc::ServerContext* context, const VA::SetDirectivityNameRequest* request,
                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetDirectivityName( request->directivity_id( ), request->name( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetDirectivityName( grpc::ServerContext* context, const VA::GetDirectivityNameRequest* request,
                                                             google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetDirectivityName( request->directivity_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetDirectivityParameters( grpc::ServerContext* context, const VA::SetDirectivityParametersRequest* request,
                                                                   google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetDirectivityParameters( request->directivity_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetDirectivityParameters( grpc::ServerContext* context, const VA::GetDirectivityParametersRequest* request,
                                                                   VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result;

	result = m_pCore->GetDirectivityParameters( request->directivity_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceBufferFromParameters( grpc::ServerContext* context,
                                                                                 const VA::CreateSignalSourceBufferFromParametersRequest* request,
                                                                                 google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceBufferFromParameters( Decode( request->parameters( ) ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourcePrototypeFromParameters( grpc::ServerContext* context,
                                                                                    const VA::CreateSignalSourcePrototypeFromParametersRequest* request,
                                                                                    google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourcePrototypeFromParameters( Decode( request->parameters( ) ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceTextToSpeech( grpc::ServerContext* context, const VA::CreateSignalSourceTextToSpeechRequest* request,
                                                                         google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceTextToSpeech( request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceSequencer( grpc::ServerContext* context, const VA::CreateSignalSourceSequencerRequest* request,
                                                                      google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceSequencer( request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceNetworkStream( grpc::ServerContext* context, const VA::CreateSignalSourceNetworkStreamRequest* request,
                                                                          google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceNetworkStream( request->interface( ), request->port( ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceEngine( grpc::ServerContext* context, const VA::CreateSignalSourceEngineRequest* request,
                                                                   google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceEngine( Decode( request->parameters( ) ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSignalSourceMachine( grpc::ServerContext* context, const VA::CreateSignalSourceMachineRequest* request,
                                                                    google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSignalSourceMachine( Decode( request->parameters( ) ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::DeleteSignalSource( grpc::ServerContext* context, const VA::DeleteSignalSourceRequest* request,
                                                             google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->DeleteSignalSource( request->signal_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSignalSourceInfo( grpc::ServerContext* context, const VA::GetSignalSourceInfoRequest* request,
                                                              VA::SignalSourceInfo* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSignalSourceInfo( request->signal_source_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSignalSourceInfos( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::SignalSourceInfos* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<CVASignalSourceInfo> signalSourceInfos;
	m_pCore->GetSignalSourceInfos( signalSourceInfos );

	for( auto&& info: signalSourceInfos )
	{
		const auto signalSourceInfo = response->add_signal_source_infos( );

		signalSourceInfo->CopyFrom( Encode( info ) );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSignalSourceBufferPlaybackState( grpc::ServerContext* context, const VA::GetSignalSourceBufferPlaybackStateRequest* request,
                                                                             VA::PlaybackState* response )
{
	BEGIN_EXCEPTION_HANDLER

	int iPlayState = m_pCore->GetSignalSourceBufferPlaybackState( request->signal_source_id( ) );

	switch( iPlayState )
	{
		case IVAInterface::VA_PLAYBACK_STATE_INVALID:
			response->set_state( VA::PlaybackState_State_INVALID );
			break;
		case IVAInterface::VA_PLAYBACK_STATE_STOPPED:
			response->set_state( VA::PlaybackState_State_STOPPED );
			break;
		case IVAInterface::VA_PLAYBACK_STATE_PAUSED:
			response->set_state( VA::PlaybackState_State_PAUSED );
			break;
		case IVAInterface::VA_PLAYBACK_STATE_PLAYING:
			response->set_state( VA::PlaybackState_State_PLAYING );
			break;
		default:
			VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new playback state was added" );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSignalSourceBufferPlaybackAction( grpc::ServerContext* context, const VA::SetSignalSourceBufferPlaybackActionRequest* request,
                                                                              google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	int playBackAction = IVAInterface::VA_PLAYBACK_STATE_INVALID;

	switch( request->playback_action( ).action( ) )
	{
		case VA::PlaybackAction_Action_NONE:
			playBackAction = IVAInterface::VA_PLAYBACK_ACTION_NONE;
			break;
		case VA::PlaybackAction_Action_STOP:
			playBackAction = IVAInterface::VA_PLAYBACK_ACTION_STOP;
			break;
		case VA::PlaybackAction_Action_PAUSE:
			playBackAction = IVAInterface::VA_PLAYBACK_ACTION_PAUSE;
			break;
		case VA::PlaybackAction_Action_PLAY:
			playBackAction = IVAInterface::VA_PLAYBACK_ACTION_PLAY;
			break;
		default:
			VA_EXCEPT2( CVAException::INVALID_PARAMETER, "It seems, a new data type was added" );
	}

	m_pCore->SetSignalSourceBufferPlaybackAction( request->signal_source_id( ), playBackAction );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSignalSourceBufferPlaybackPosition( grpc::ServerContext* context,
                                                                                const VA::SetSignalSourceBufferPlaybackPositionRequest* request,
                                                                                google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSignalSourceBufferPlaybackPosition( request->signal_source_id( ), request->position( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSignalSourceBufferLooping( grpc::ServerContext* context, const VA::SetSignalSourceBufferLoopingRequest* request,
                                                                       google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSignalSourceBufferLooping( request->signal_source_id( ), request->looping( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSignalSourceBufferLooping( grpc::ServerContext* context, const VA::GetSignalSourceBufferLoopingRequest* request,
                                                                       google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSignalSourceBufferLooping( request->signal_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSignalSourceParameters( grpc::ServerContext* context, const VA::SetSignalSourceParametersRequest* request,
                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSignalSourceParameters( request->signal_source_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSignalSourceParameters( grpc::ServerContext* context, const VA::GetSignalSourceParametersRequest* request,
                                                                    VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result = m_pCore->GetSignalSourceParameters( request->signal_source_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::AddSignalSourceSequencerSample( grpc::ServerContext* context, const VA::AddSignalSourceSequencerSampleRequest* request,
                                                                         google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->AddSignalSourceSequencerSample( request->signal_source_id( ), request->file_path( ) ));

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::AddSignalSourceSequencerPlayback( grpc::ServerContext* context, const VA::AddSignalSourceSequencerPlaybackRequest* request,
                                                                           google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->AddSignalSourceSequencerPlayback( request->signal_source_id( ), request->sound_id( ), request->flags( ), request->time_code( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::RemoveSignalSourceSequencerSample( grpc::ServerContext* context, const VA::RemoveSignalSourceSequencerSampleRequest* request,
                                                                            google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->RemoveSignalSourceSequencerSample( request->signal_source_id( ), request->sample_id( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSoundSource( grpc::ServerContext* context, const VA::CreateSoundSourceRequest* request,
                                                            google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSoundSource( request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceIDs( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::IntIdVector* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<int> viSoundSourceIDs;
	m_pCore->GetSoundSourceIDs( viSoundSourceIDs );

	response->CopyFrom( Encode( viSoundSourceIDs ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceInfo( grpc::ServerContext* context, const VA::GetSoundSourceInfoRequest* request, VA::SoundSourceInfo* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundSourceInfo( request->sound_source_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSoundSourceExplicitRenderer( grpc::ServerContext* context, const VA::CreateSoundSourceExplicitRendererRequest* request,
                                                                            google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSoundSourceExplicitRenderer( request->renderer_id( ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::DeleteSoundSource( grpc::ServerContext* context, const VA::DeleteSoundSourceRequest* request,
                                                            google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const int iDeleted = m_pCore->DeleteSoundSource( request->sound_source_id( ) );
	if( iDeleted == 0 )
		response->set_value( true );
	else
		response->set_value( false );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceEnabled( grpc::ServerContext* context, const VA::SetSoundSourceEnabledRequest* request,
                                                                google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceEnabled( request->sound_source_id( ), request->enabled( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceEnabled( grpc::ServerContext* context, const VA::GetSoundSourceEnabledRequest* request,
                                                                google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const bool bEnabled = m_pCore->GetSoundSourceEnabled( request->sound_source_id( ) );
	response->set_value( bEnabled );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceName( grpc::ServerContext* context, const VA::GetSoundSourceNameRequest* request,
                                                             google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundSourceName( request->sound_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceName( grpc::ServerContext* context, const VA::SetSoundSourceNameRequest* request,
                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceName( request->sound_source_id( ), request->name( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceSignalSource( grpc::ServerContext* context, const VA::GetSoundSourceSignalSourceRequest* request,
                                                                     google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundSourceSignalSource( request->sound_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceSignalSource( grpc::ServerContext* context, const VA::SetSoundSourceSignalSourceRequest* request,
                                                                     google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceSignalSource( request->sound_source_id( ), request->signal_source_id( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceAuralizationMode( grpc::ServerContext* context, const VA::GetSoundSourceAuralizationModeRequest* request,
                                                                         google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundSourceAuralizationMode( request->sound_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceAuralizationMode( grpc::ServerContext* context, const VA::SetSoundSourceAuralizationModeRequest* request,
                                                                         google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceAuralizationMode( request->sound_source_id( ), request->mode( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceParameters( grpc::ServerContext* context, const VA::SetSoundSourceParametersRequest* request,
                                                                   google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceParameters( request->sound_source_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceParameters( grpc::ServerContext* context, const VA::GetSoundSourceParametersRequest* request,
                                                                   VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result;

	result = m_pCore->GetSoundSourceParameters( request->sound_source_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceDirectivity( grpc::ServerContext* context, const VA::GetSoundSourceDirectivityRequest* request,
                                                                    google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundSourceDirectivity( request->sound_source_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceDirectivity( grpc::ServerContext* context, const VA::SetSoundSourceDirectivityRequest* request,
                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceDirectivity( request->sound_source_id( ), request->directivity_id( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceSoundPower( grpc::ServerContext* context, const VA::GetSoundSourceSoundPowerRequest* request,
                                                                   google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const double dSoundPower = m_pCore->GetSoundSourceSoundPower( request->sound_source_id( ) );
	response->set_value( dSoundPower );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceSoundPower( grpc::ServerContext* context, const VA::SetSoundSourceSoundPowerRequest* request,
                                                                   google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceSoundPower( request->sound_source_id( ), request->sound_power( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceMuted( grpc::ServerContext* context, const VA::GetSoundSourceMutedRequest* request,
                                                              google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const bool bMuted = m_pCore->GetSoundSourceMuted( request->sound_source_id( ) );
	response->set_value( bMuted );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceMuted( grpc::ServerContext* context, const VA::SetSoundSourceMutedRequest* request,
                                                              google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceMuted( request->sound_source_id( ), request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourcePose( grpc::ServerContext* context, const VA::GetSoundSourcePoseRequest* request,
                                                             VA::GetSoundSourcePoseReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3Pos;
	VAQuat qOrientation;

	m_pCore->GetSoundSourcePose( request->sound_source_id( ), v3Pos, qOrientation );

	response->mutable_position( )->CopyFrom( Encode( v3Pos ) );
	response->mutable_orientation( )->CopyFrom( Encode( qOrientation ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourcePose( grpc::ServerContext* context, const VA::SetSoundSourcePoseRequest* request,
                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourcePose( request->sound_source_id( ), Decode( request->position( ) ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourcePosition( grpc::ServerContext* context, const VA::GetSoundSourcePositionRequest* request, VA::Vector3* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundSourcePosition( request->sound_source_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourcePosition( grpc::ServerContext* context, const VA::SetSoundSourcePositionRequest* request,
                                                                 google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourcePosition( request->sound_source_id( ), Decode( request->position( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceOrientation( grpc::ServerContext* context, const VA::GetSoundSourceOrientationRequest* request,
                                                                    VA::Quaternion* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundSourceOrientation( request->sound_source_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceOrientation( grpc::ServerContext* context, const VA::SetSoundSourceOrientationRequest* request,
                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceOrientation( request->sound_source_id( ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundSourceOrientationVU( grpc::ServerContext* context, const VA::GetSoundSourceOrientationVURequest* request,
                                                                      VA::GetSoundSourceOrientationVUReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3View;
	VAVec3 v3Up;

	m_pCore->GetSoundSourceOrientationVU( request->sound_source_id( ), v3View, v3Up );

	response->mutable_view( )->CopyFrom( Encode( v3View ) );
	response->mutable_up( )->CopyFrom( Encode( v3Up ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundSourceOrientationVU( grpc::ServerContext* context, const VA::SetSoundSourceOrientationVURequest* request,
                                                                      google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundSourceOrientationVU( request->sound_source_id( ), Decode( request->view( ) ), Decode( request->up( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverIDs( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::IntIdVector* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<int> viSoundReceiverIDs;
	m_pCore->GetSoundReceiverIDs( viSoundReceiverIDs );

	response->CopyFrom( Encode( viSoundReceiverIDs ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSoundReceiver( grpc::ServerContext* context, const VA::CreateSoundReceiverRequest* request,
                                                              google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSoundReceiver( request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::CreateSoundReceiverExplicitRenderer( grpc::ServerContext* context, const VA::CreateSoundReceiverExplicitRendererRequest* request,
                                                                              google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->CreateSoundReceiverExplicitRenderer( request->renderer_id( ), request->name( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::DeleteSoundReceiver( grpc::ServerContext* context, const VA::DeleteSoundReceiverRequest* request,
                                                              google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const int iDeleted = m_pCore->DeleteSoundReceiver( request->sound_receiver_id( ) );
	if( iDeleted == 0 )
		response->set_value( true );
	else
		response->set_value( false );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverInfo( grpc::ServerContext* context, const VA::GetSoundReceiverInfoRequest* request,
                                                               VA::SoundReceiverInfo* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundReceiverInfo( request->sound_receiver_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverEnabled( grpc::ServerContext* context, const VA::SetSoundReceiverEnabledRequest* request,
                                                                  google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverEnabled( request->sound_receiver_id( ), request->enabled( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverEnabled( grpc::ServerContext* context, const VA::GetSoundReceiverEnabledRequest* request,
                                                                  google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	const bool bEnabled = m_pCore->GetSoundReceiverEnabled( request->sound_receiver_id( ) );
	response->set_value( bEnabled );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverName( grpc::ServerContext* context, const VA::GetSoundReceiverNameRequest* request,
                                                               google::protobuf::StringValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundReceiverName( request->sound_receiver_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}


grpc::Status VANetServiceImplementation::SetSoundReceiverName( grpc::ServerContext* context, const VA::SetSoundReceiverNameRequest* request,
                                                               google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverName( request->sound_receiver_id( ), request->name( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverAuralizationMode( grpc::ServerContext* context, const VA::GetSoundReceiverAuralizationModeRequest* request,
                                                                           google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundReceiverAuralizationMode( request->sound_receiver_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverAuralizationMode( grpc::ServerContext* context, const VA::SetSoundReceiverAuralizationModeRequest* request,
                                                                           google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverAuralizationMode( request->sound_receiver_id( ), request->mode( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverParameters( grpc::ServerContext* context, const VA::SetSoundReceiverParametersRequest* request,
                                                                     google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverParameters( request->sound_receiver_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverParameters( grpc::ServerContext* context, const VA::GetSoundReceiverParametersRequest* request,
                                                                     VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result;

	result = m_pCore->GetSoundReceiverParameters( request->sound_receiver_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverDirectivity( grpc::ServerContext* context, const VA::GetSoundReceiverDirectivityRequest* request,
                                                                      google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundReceiverDirectivity( request->sound_receiver_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverDirectivity( grpc::ServerContext* context, const VA::SetSoundReceiverDirectivityRequest* request,
                                                                      google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverDirectivity( request->sound_receiver_id( ), request->directivity_id( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverMuted( grpc::ServerContext* context, const VA::GetSoundReceiverMutedRequest* request,
                                                                google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetSoundReceiverMuted( request->sound_receiver_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverMuted( grpc::ServerContext* context, const VA::SetSoundReceiverMutedRequest* request,
                                                                google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverMuted( request->sound_receiver_id( ), request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverPose( grpc::ServerContext* context, const VA::GetSoundReceiverPoseRequest* request,
                                                               VA::GetSoundReceiverPoseReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3Pos;
	VAQuat qOrientation;

	m_pCore->GetSoundReceiverPose( request->sound_receiver_id( ), v3Pos, qOrientation );

	response->mutable_position( )->CopyFrom( Encode( v3Pos ) );
	response->mutable_orientation( )->CopyFrom( Encode( qOrientation ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverPose( grpc::ServerContext* context, const VA::SetSoundReceiverPoseRequest* request,
                                                               google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverPose( request->sound_receiver_id( ), Decode( request->position( ) ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverPosition( grpc::ServerContext* context, const VA::GetSoundReceiverPositionRequest* request,
                                                                   VA::Vector3* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundReceiverPosition( request->sound_receiver_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverPosition( grpc::ServerContext* context, const VA::SetSoundReceiverPositionRequest* request,
                                                                   google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverPosition( request->sound_receiver_id( ), Decode( request->position( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverOrientationVU( grpc::ServerContext* context, const VA::GetSoundReceiverOrientationVURequest* request,
                                                                        VA::GetSoundReceiverOrientationVUReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3View;
	VAVec3 v3Up;

	m_pCore->GetSoundReceiverOrientationVU( request->sound_receiver_id( ), v3View, v3Up );

	response->mutable_view( )->CopyFrom( Encode( v3View ) );
	response->mutable_up( )->CopyFrom( Encode( v3Up ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverOrientationVU( grpc::ServerContext* context, const VA::SetSoundReceiverOrientationVURequest* request,
                                                                        google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverOrientationVU( request->sound_receiver_id( ), Decode( request->view( ) ), Decode( request->up( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverOrientation( grpc::ServerContext* context, const VA::GetSoundReceiverOrientationRequest* request,
                                                                      VA::Quaternion* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundReceiverOrientation( request->sound_receiver_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverOrientation( grpc::ServerContext* context, const VA::SetSoundReceiverOrientationRequest* request,
                                                                      google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverOrientation( request->sound_receiver_id( ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverHeadAboveTorsoOrientation( grpc::ServerContext* context,
                                                                                    const VA::GetSoundReceiverHeadAboveTorsoOrientationRequest* request,
                                                                                    VA::Quaternion* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundReceiverHeadAboveTorsoOrientation( request->sound_receiver_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverHeadAboveTorsoOrientation( grpc::ServerContext* context,
                                                                                    const VA::SetSoundReceiverHeadAboveTorsoOrientationRequest* request,
                                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverHeadAboveTorsoOrientation( request->sound_receiver_id( ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverRealWorldPositionOrientationVU( grpc::ServerContext* context,
                                                                                         const VA::GetSoundReceiverRealWorldPositionOrientationVURequest* request,
                                                                                         VA::GetSoundReceiverRealWorldPositionOrientationVUReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3Pos;
	VAVec3 v3View;
	VAVec3 v3Up;

	m_pCore->GetSoundReceiverRealWorldPositionOrientationVU( request->sound_receiver_id( ), v3Pos, v3View, v3Up );

	response->mutable_position( )->CopyFrom( Encode( v3Pos ) );
	response->mutable_view( )->CopyFrom( Encode( v3View ) );
	response->mutable_up( )->CopyFrom( Encode( v3Up ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverRealWorldPositionOrientationVU( grpc::ServerContext* context,
                                                                                         const VA::SetSoundReceiverRealWorldPositionOrientationVURequest* request,
                                                                                         google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverRealWorldPositionOrientationVU( request->sound_receiver_id( ), Decode( request->position( ) ), Decode( request->view( ) ),
	                                                         Decode( request->up( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverRealWorldPose( grpc::ServerContext* context, const VA::GetSoundReceiverRealWorldPoseRequest* request,
                                                                        VA::GetSoundReceiverRealWorldPoseReply* response )
{
	BEGIN_EXCEPTION_HANDLER

	VAVec3 v3Pos;
	VAQuat qOrientation;

	m_pCore->GetSoundReceiverRealWorldPose( request->sound_receiver_id( ), v3Pos, qOrientation );

	response->mutable_position( )->CopyFrom( Encode( v3Pos ) );
	response->mutable_orientation( )->CopyFrom( Encode( qOrientation ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverRealWorldPose( grpc::ServerContext* context, const VA::SetSoundReceiverRealWorldPoseRequest* request,
                                                                        google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverRealWorldPose( request->sound_receiver_id( ), Decode( request->position( ) ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetSoundReceiverRealWorldHeadAboveTorsoOrientation( grpc::ServerContext* context,
                                                                                             const VA::GetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest* request,
                                                                                             VA::Quaternion* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetSoundReceiverRealWorldHeadAboveTorsoOrientation( request->sound_receiver_id( ) ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetSoundReceiverRealWorldHeadAboveTorsoOrientation( grpc::ServerContext* context,
                                                                                             const VA::SetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest* request,
                                                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetSoundReceiverRealWorldHeadAboveTorsoOrientation( request->sound_receiver_id( ), Decode( request->orientation( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetRenderingModules( grpc::ServerContext* context, const VA::GetRenderingModulesRequest* request,
                                                              VA::AudioRendererInfos* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<CVAAudioRendererInfo> renderingInfos;

	m_pCore->GetRenderingModules( renderingInfos, request->only_enabled( ) );

	for( auto&& renderingInfo: renderingInfos )
	{
		const auto info = response->add_audio_renderer_infos( );

		info->set_name( renderingInfo.sID );
		info->set_class_( renderingInfo.sClass );
		info->set_description( renderingInfo.sDescription );
		info->set_enabled( renderingInfo.bEnabled );
		info->set_output_detector_enabled( renderingInfo.bOutputDetectorEnabled );
		info->set_output_recording_enabled( renderingInfo.bOutputRecordingEnabled );
		info->set_output_recording_file_path( renderingInfo.sOutputRecordingFilePath );
		info->mutable_parameters( )->CopyFrom( Encode( renderingInfo.oParams ) );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetRenderingModuleMuted( grpc::ServerContext* context, const VA::SetRenderingModuleMutedRequest* request,
                                                                  google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetRenderingModuleMuted( request->rendering_module_id( ), request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetRenderingModuleMuted( grpc::ServerContext* context, const VA::GetRenderingModuleMutedRequest* request,
                                                                  google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetRenderingModuleMuted( request->rendering_module_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetRenderingModuleGain( grpc::ServerContext* context, const VA::SetRenderingModuleGainRequest* request,
                                                                 google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetRenderingModuleGain( request->rendering_module_id( ), request->gain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetRenderingModuleGain( grpc::ServerContext* context, const VA::GetRenderingModuleGainRequest* request,
                                                                 google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetRenderingModuleGain( request->rendering_module_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetRenderingModuleAuralizationMode( grpc::ServerContext* context, const VA::SetRenderingModuleAuralizationModeRequest* request,
                                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetRenderingModuleAuralizationMode( request->rendering_module_id( ), request->mode( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetRenderingModuleAuralizationMode( grpc::ServerContext* context, const VA::GetRenderingModuleAuralizationModeRequest* request,
                                                                             google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetRenderingModuleAuralizationMode( request->rendering_module_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetRenderingModuleParameters( grpc::ServerContext* context, const VA::SetRenderingModuleParametersRequest* request,
                                                                       google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetRenderingModuleParameters( request->rendering_module_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetRenderingModuleParameters( grpc::ServerContext* context, const VA::GetRenderingModuleParametersRequest* request,
                                                                       VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result = m_pCore->GetRenderingModuleParameters( request->rendering_module_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetReproductionModules( grpc::ServerContext* context, const VA::GetReproductionModulesRequest* request,
                                                                 VA::AudioReproductionInfos* response )
{
	BEGIN_EXCEPTION_HANDLER

	std::vector<CVAAudioReproductionInfo> reproductionInfos;

	m_pCore->GetReproductionModules( reproductionInfos, request->only_enabled( ) );

	for( auto&& renderingInfo: reproductionInfos )
	{
		const auto info = response->add_audio_reproduction_infos( );

		info->set_id( renderingInfo.sID );
		info->set_class_( renderingInfo.sClass );
		info->set_description( renderingInfo.sDescription );
		info->set_enabled( renderingInfo.bEnabled );
		info->set_input_detector_enabled( renderingInfo.bInputDetectorEnabled );
		info->set_input_recording_enabled( renderingInfo.bInputRecordingEnabled );
		info->set_input_recording_file_path( renderingInfo.sInputRecordingFilePath );
		info->set_bool_detector_enabled( renderingInfo.bOutputDetectorEnabled );
		info->set_bool_recording_enabled( renderingInfo.bOutputRecordingEnabled );
		info->set_bool_recording_file_path( renderingInfo.sOutputRecordingFilePath );
		info->mutable_parameters( )->CopyFrom( Encode( renderingInfo.oParams ) );
	}

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetReproductionModuleMuted( grpc::ServerContext* context, const VA::SetReproductionModuleMutedRequest* request,
                                                                     google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetReproductionModuleMuted( request->reproduction_module_id( ), request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetReproductionModuleMuted( grpc::ServerContext* context, const VA::GetReproductionModuleMutedRequest* request,
                                                                     google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetReproductionModuleMuted( request->reproduction_module_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetReproductionModuleGain( grpc::ServerContext* context, const VA::SetReproductionModuleGainRequest* request,
                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetReproductionModuleGain( request->reproduction_module_id( ), request->gain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetReproductionModuleGain( grpc::ServerContext* context, const VA::GetReproductionModuleGainRequest* request,
                                                                    google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetReproductionModuleGain( request->reproduction_module_id( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetReproductionModuleParameters( grpc::ServerContext* context, const VA::SetReproductionModuleParametersRequest* request,
                                                                          google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetReproductionModuleParameters( request->reproduction_module_id( ), Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetReproductionModuleParameters( grpc::ServerContext* context, const VA::GetReproductionModuleParametersRequest* request,
                                                                          VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result = m_pCore->GetReproductionModuleParameters( request->reproduction_module_id( ), Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetInputGain( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetInputGain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetInputGain( grpc::ServerContext* context, const VA::SetInputGainRequest* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetInputGain( request->gain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetInputMuted( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetInputMuted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetInputMuted( grpc::ServerContext* context, const VA::SetInputMutedRequest* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetInputMuted( request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetOutputGain( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetOutputGain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetOutputGain( grpc::ServerContext* context, const VA::SetOutputGainRequest* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetOutputGain( request->gain( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetOutputMuted( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetOutputMuted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetOutputMuted( grpc::ServerContext* context, const VA::SetOutputMutedRequest* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetOutputMuted( request->muted( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetGlobalAuralizationMode( grpc::ServerContext* context, const google::protobuf::Empty* request,
                                                                    google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetGlobalAuralizationMode( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}


grpc::Status VANetServiceImplementation::SetGlobalAuralizationMode( grpc::ServerContext* context, const VA::SetGlobalAuralizationModeRequest* request,
                                                                    google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetGlobalAuralizationMode( request->mode( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetCoreClock( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetCoreClock( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetCoreClock( grpc::ServerContext* context, const VA::SetCoreClockRequest* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetCoreClock( request->time( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumSoundSpeed( grpc::ServerContext* context, const VA::SetHomogeneousMediumSoundSpeedRequest* request,
                                                                         google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumSoundSpeed( request->sound_speed( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumSoundSpeed( grpc::ServerContext* context, const google::protobuf::Empty* request,
                                                                         google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetHomogeneousMediumSoundSpeed( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumTemperature( grpc::ServerContext* context, const VA::SetHomogeneousMediumTemperatureRequest* request,
                                                                          google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumTemperature( request->temperature( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumTemperature( grpc::ServerContext* context, const google::protobuf::Empty* request,
                                                                          google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetHomogeneousMediumTemperature( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumStaticPressure( grpc::ServerContext* context, const VA::SetHomogeneousMediumStaticPressureRequest* request,
                                                                             google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumStaticPressure( request->static_pressure( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumStaticPressure( grpc::ServerContext* context, const google::protobuf::Empty* request,
                                                                             google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetHomogeneousMediumStaticPressure( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumRelativeHumidity( grpc::ServerContext* context,
                                                                               const VA::SetHomogeneousMediumRelativeHumidityRequest* request,
                                                                               google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumRelativeHumidity( request->humidity( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumRelativeHumidity( grpc::ServerContext* context, const google::protobuf::Empty* request,
                                                                               google::protobuf::DoubleValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetHomogeneousMediumRelativeHumidity( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumShiftSpeed( grpc::ServerContext* context, const VA::Vector3* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumShiftSpeed( Decode( *request ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumShiftSpeed( grpc::ServerContext* context, const google::protobuf::Empty* request, VA::Vector3* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->CopyFrom( Encode( m_pCore->GetHomogeneousMediumShiftSpeed( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::SetHomogeneousMediumParameters( grpc::ServerContext* context, const VA::HomogeneousMediumParametersRequest* request,
                                                                         google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->SetHomogeneousMediumParameters( Decode( request->parameters( ) ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetHomogeneousMediumParameters( grpc::ServerContext* context, const VA::HomogeneousMediumParametersRequest* request,
                                                                         VA::Struct* response )
{
	BEGIN_EXCEPTION_HANDLER

	CVAStruct result;

	result = m_pCore->GetHomogeneousMediumParameters( Decode( request->parameters( ) ) );

	response->CopyFrom( Encode( result ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::GetUpdateLocked( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::BoolValue* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->GetUpdateLocked( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::LockUpdate( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Empty* response )
{
	BEGIN_EXCEPTION_HANDLER

	m_pCore->LockUpdate( );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}

grpc::Status VANetServiceImplementation::UnlockUpdate( grpc::ServerContext* context, const google::protobuf::Empty* request, google::protobuf::Int32Value* response )
{
	BEGIN_EXCEPTION_HANDLER

	response->set_value( m_pCore->UnlockUpdate( ) );

	END_EXCEPTION_HANDLER
	return grpc::Status::OK;
}
