/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#include "VANetClientImpl.h"

#include "VANetClientNetworkedCore.h"

CVANetClientImpl::CVANetClientImpl( ) {}

CVANetClientImpl::~CVANetClientImpl( ) {}

IVAInterface* CVANetClientImpl::GetCoreInstance( ) const
{
	return m_pVACore.get( );
}

bool CVANetClientImpl::IsConnected( ) const
{
	// TODO use WaitForConnected?
	return m_pChannel && m_pChannel->GetState( false ) != GRPC_CHANNEL_SHUTDOWN;
}

std::string CVANetClientImpl::GetServerAddress( ) const
{
	return m_sServerIP;
}

bool CVANetClientImpl::Initialize( const std::string& sServerAddress, const int iServerPort, const ExceptionHandlingMode eExceptionHandlingMode )
{
	m_eExceptionHandlingMode = eExceptionHandlingMode;
	m_sServerIP              = sServerAddress + ":" + std::to_string( iServerPort );
	m_pChannel               = CreateChannel( m_sServerIP, ::grpc::InsecureChannelCredentials( ) );
	m_pVACore                = std::make_unique<CNetworkedVACore>( this, m_pChannel );
	return IsConnected( );
}

bool CVANetClientImpl::Disconnect( )
{
	m_pChannel.reset( );
	m_pVACore.reset( );
	return !IsConnected( );
}

void CVANetClientImpl::SetExceptionHandlingMode( ExceptionHandlingMode eExceptionHandlingMode )
{
	m_eExceptionHandlingMode = eExceptionHandlingMode;
}

IVANetClient::ExceptionHandlingMode CVANetClientImpl::GetExceptionHandlingMode( ) const
{
	return m_eExceptionHandlingMode;
}
