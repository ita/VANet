# VANet

VANet is a C++, server and client library that enables to communicate with the [VA interface](https://git.rwth-aachen.de/ita/VABase) over a TCP/IP network connection.
Under the hood, VANet uses the [gRPC](https://github.com/grpc/grpc) library to implement the communication itself.
Thus, all client languages that are supported by gRPC are also supported here.

## License

Copyright 2015-2024 Institute of Technical Acoustics (ITA), RWTH Aachen University

Licensed under the Apache License, Version 2.0 (the "License");
you may not use files of this project except in compliance with the License.
You may obtain a copy of the License at

<http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

## Bindings

There are bindings for different scripting and programming languages available for VANet

* C# (project [VACS](https://git.rwth-aachen.de/ita/VACS) or via the gRPC client)
* Matlab (project [VAMatlab](https://git.rwth-aachen.de/ita/VAMatlab))
* LUA (project [VALUA](https://git.rwth-aachen.de/ita/VALUA))
* Python (project [VAPython](https://git.rwth-aachen.de/ita/VAPython) or via the gRPC client)

## Applications

* VAServer (project [VAServer](https://git.rwth-aachen.de/ita/VAServer))
* Inside (project [Inside](https://devhub.vr.rwth-aachen.de/VR-Group/Inside), collaboration with the IT Center, requires github account)
* Unity (project [VAUnity](https://git.rwth-aachen.de/ita/VAUnity), using VA C# binding)

## Quick build guide

It is recommended to clone and follow the build guide of the parent project [VA](https://git.rwth-aachen.de/ita/VA), which includes this project as a submodule.
