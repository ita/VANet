/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_CLIENT
#define IW_VANET_CLIENT

#include <VANetDefinitions.h>
#include <memory>
#include <string>

class IVAInterface;
class CVANetNetworkProtocol;

//! Networked transparend calls to VA interface view TCP/IP as a client
/**
 * This class defines an interface for network clients to IVAInterface.
 * Almost the entire interface is replicated via network.
 *
 * Create an instance using the factory method Create().
 *
 * Client creates a client-side implementation of the interface on success.
 * You can get access to the (transparently networked) interface using
 * GetCoreInstance(). Any method call will be forwarded to the corresponding
 * server.
 *
 */
class VANET_API IVANetClient
{
public:
	//! Exception handling modi
	enum class ExceptionHandlingMode
	{
		EXC_CLIENT_THROW, //!< for all functions, immediately transmit exception and throws it
		EXC_SERVER_PRINT, //!< functions without return value immediately return, and the exception is printed on the server
	};

	//! Factory method
	/**
	 * @return Pointer to new net client
	 */
	[[nodiscard]] static std::unique_ptr<IVANetClient> Create( );

	//! Destructor
	virtual ~IVANetClient( );

	//! Return client-side interface (networked calls)
	/**
	 * @return Interface pointer
	 */
	[[nodiscard]] virtual IVAInterface* GetCoreInstance( ) const = 0;

	//!< Server connection getter
	/**
	 * @return True, if connection is established
	 */
	[[nodiscard]] virtual bool IsConnected( ) const = 0;

	//! Server address getter
	/**
	 * @return Server address string
	 */
	[[nodiscard]] virtual std::string GetServerAddress( ) const = 0;

	//! Initialize client and connect server
	/**
	 * @param[in] sServerAddress Server address string
	 * @param[in] iServerPort Server port (defaults to VANet definition)
	 * @param[in] eExceptionHandlingMode Define exception mode handling
	 *
	 * @return Status code
	 *
	 */
	[[nodiscard]] virtual bool Initialize( const std::string& sServerAddress, const int iServerPort = VANET_DEFAULT_PORT,
	                                       const ExceptionHandlingMode eExceptionHandlingMode = ExceptionHandlingMode::EXC_CLIENT_THROW ) = 0;

	//! Exception mode setter
	/**
	 * @param[in] eExceptionHandlingMode Exception mode
	 */
	virtual void SetExceptionHandlingMode( const ExceptionHandlingMode eExceptionHandlingMode ) = 0;

	//! Exception mode getter
	/**
	 * @return Exception mode
	 */
	[[nodiscard]] virtual ExceptionHandlingMode GetExceptionHandlingMode( ) const = 0;

	//! Disconnect an established connection
	/**
	 * @return Status code
	 */
	[[nodiscard]] virtual bool Disconnect( ) = 0;

protected:
	//! No default construction
	IVANetClient( );
};

#endif // IW_VANET_CLIENT
