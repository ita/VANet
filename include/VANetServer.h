/*
 *  --------------------------------------------------------------------------------------------
 *
 *    VVV        VVV A           Virtual Acoustics (VA) | http://www.virtualacoustics.org
 *     VVV      VVV AAA          Licensed under the Apache License, Version 2.0
 *      VVV    VVV   AAA
 *       VVV  VVV     AAA        Copyright 2015-2024
 *        VVVVVV       AAA       Institute of Technical Acoustics (ITA)
 *         VVVV         AAA      RWTH Aachen University
 *
 *  --------------------------------------------------------------------------------------------
 */

#ifndef IW_VANET_SERVER
#define IW_VANET_SERVER

#include <VANetDefinitions.h>
#include <list>
#include <memory>
#include <string>

// Forward declarations
class IVAInterface;

//! VA server interface
/**
 * This class defines an interface for a TCP/IP network server with
 * a managed VA core.
 *
 * Use the factory method Create() to instantiate a server, then set a
 * VA core interface instance using SetCoreInstance().
 *
 * Calls from a network client will be transmitted over TCP/IP transparently,
 * and will be forwarded to the actual core.
 *
 * The server network functionality allows multiple client connections, however
 * updates are synchronized making them potentially less efficient.
 */
class VANET_API IVANetServer
{
public:
	using tPortRange = std::pair<int, int>;   //!< Port range typedef
	using tPortList  = std::list<tPortRange>; //!< List of ranges typedef for multi client support

	//! Factory method to create a server
	/**
	 * Use this method to create an instance of a server, because default constructor is protected.
	 */
	[[nodiscard]] static std::unique_ptr<IVANetServer> Create( );

	//! Destructor
	virtual ~IVANetServer( );

	//! Initialize server
	/**
	 *
	 * @param[in] sInterface		Network TCP/IP interface where server will listen
	 * @param[in]            iServerPort		Network TCP/IP socket port
	 * @param[in]            iFreePortMin		Port range beginning for communication
	 * @param[in]            iFreePortMax		Port range end for communication
	 * @param[in]            iMaxNumClients	Maximum number of allowed clients (-1 allows infinit connections)
	 *
	 * @return Status code
	 *
	 */
	[[nodiscard]] virtual bool Initialize( const std::string& sInterface, const int iServerPort = VANET_DEFAULT_PORT, const int iFreePortMin = 10000,
	                                       const int iFreePortMax = 11000, const int iMaxNumClients = -1 ) = 0;

	//! Initialize server
	/**
	 *
	 * @param[in] sInterface		Network TCP/IP interface where server will listen
	 * @param[in] iServerPort		Network TCP/IP socket port
	 * @param[in]           liFreePorts		Port range list for communication
	 * @param[in]            iMaxNumClients	Maximum number of allowed clients (-1 allows infinit connections)
	 *
	 * @return Status code
	 *
	 */
	[[nodiscard]] virtual bool Initialize( const std::string& sInterface, const int iServerPort, const tPortList& liFreePorts, const int iMaxNumClients = -1 ) = 0;

	//! Finalizes server and disconnects clients
	/**
	 * @return Status code
	 */
	[[nodiscard]] virtual bool Finalize( ) = 0;

	//! Reset all connections
	virtual void Reset( ) = 0;

	//! Server address getter
	/**
	 * @return Server address string
	 */
	[[nodiscard]] virtual std::string GetServerAddress( ) const = 0;

	//! Server port getter
	/**
	 * @return Server port
	 */
	[[nodiscard]] virtual int GetServerPort( ) const = 0;

	//! Core instance getter
	/**
	 * Get the attached core interface pointer.
	 *
	 * @return Interface pointer, or nullptr
	 */
	[[nodiscard]] virtual IVAInterface* GetCoreInstance( ) const = 0;

	//! Core instance setter
	/**
	 * If you have instantiated a VA core, use this method to attach a core
	 * to the server.
	 *
	 * @param[in] pCore Interface pointer
	 */
	virtual void SetCoreInstance( IVAInterface* pCore ) = 0;

	//! Returns true if at least one client is connected
	/**
	 * @return True, if at least one client is connected
	 */
	[[nodiscard]] virtual bool IsClientConnected( ) const = 0;

	//! Returns number of connected clients
	/**
	 * @return Number of connected clients
	 */
	[[nodiscard]] virtual int GetNumConnectedClients( ) const = 0;

	//! Returns hostname of given client
	/**
	 * @param[in] iClientIndex Client index integer
	 * @return Client host name, e.g. "127.0.0.1"
	 */
	[[nodiscard]] virtual std::string GetClientHostname( const int iClientIndex ) const = 0;

protected:
	//! Disabled default constructor, use Create() factory method instead.
	IVANetServer( );
};

#endif // IW_VANET_SERVER
