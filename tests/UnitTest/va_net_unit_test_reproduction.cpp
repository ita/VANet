#include "test_utils.h"


TEST_CASE( "VANet::VANet/Reproduction", "[VANet][Reproduction][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetReproductionModules" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::vector<CVAAudioReproductionInfo> result, actual;

		const auto numModules = GENERATE( take( 1, random( 1, 2 ) ) );
		bool bFilterCall;

		for( int i = 0; i < numModules; ++i )
		{
			CVAAudioReproductionInfo info;
			info.sID                      = GENERATE_RANDOM_STRING( 10 );
			info.sClass                   = GENERATE_RANDOM_STRING( 20 );
			info.sDescription             = GENERATE_RANDOM_STRING( 10 );
			info.bEnabled                 = GENERATE_RANDOM_BOOL( MAX_REPS );
			info.bInputDetectorEnabled    = GENERATE_RANDOM_BOOL( 1 );
			info.bInputRecordingEnabled   = GENERATE_RANDOM_BOOL( 1 );
			info.sInputRecordingFilePath  = GENERATE_RANDOM_STRING( 30 );
			info.bOutputDetectorEnabled   = GENERATE_RANDOM_BOOL( 1 );
			info.bOutputRecordingEnabled  = GENERATE_RANDOM_BOOL( 1 );
			info.sOutputRecordingFilePath = GENERATE_RANDOM_STRING( 30 );
			info.oParams                  = generateRandomStruct( );

			actual.push_back( info );
		}

		fakeit::When( Method( VACoreMock, GetReproductionModules ) )
		    .AlwaysDo(
		        [&]( std::vector<CVAAudioReproductionInfo>& voReproductions, const bool bFilterEnabled )
		        {
			        voReproductions = actual;
			        bFilterCall     = bFilterEnabled;
		        } );

		bool bFilter = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetReproductionModules( result, bFilter ) );

		fakeit::Verify( Method( VACoreMock, GetReproductionModules ) );

		REQUIRE( bFilterCall == bFilter );

		REQUIRE( result.size( ) == actual.size( ) );

		for( int i = 0; i < result.size( ); ++i )
		{
			REQUIRE( result.at( i ).sID == actual.at( i ).sID );
			REQUIRE( result.at( i ).sClass == actual.at( i ).sClass );
			REQUIRE( result.at( i ).sDescription == actual.at( i ).sDescription );
			REQUIRE( result.at( i ).bEnabled == actual.at( i ).bEnabled );
			REQUIRE( result.at( i ).bInputDetectorEnabled == actual.at( i ).bInputDetectorEnabled );
			REQUIRE( result.at( i ).bInputRecordingEnabled == actual.at( i ).bInputRecordingEnabled );
			REQUIRE( result.at( i ).sInputRecordingFilePath == actual.at( i ).sInputRecordingFilePath );
			REQUIRE( result.at( i ).bOutputDetectorEnabled == actual.at( i ).bOutputDetectorEnabled );
			REQUIRE( result.at( i ).bOutputRecordingEnabled == actual.at( i ).bOutputRecordingEnabled );
			REQUIRE( result.at( i ).sOutputRecordingFilePath == actual.at( i ).sOutputRecordingFilePath );
			REQUIRE( result.at( i ).oParams == actual.at( i ).oParams );
		}
	}

	SECTION( "SetReproductionModuleMuted" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bMutedCall;

		fakeit::When( Method( VACoreMock, SetReproductionModuleMuted ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID, const bool bMuted )
		        {
			        sIDCall    = sModuleID;
			        bMutedCall = bMuted;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bMuted     = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetReproductionModuleMuted( sID, bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetReproductionModuleMuted ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( bMuted == bMutedCall );
	}

	SECTION( "GetReproductionModuleMuted" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetReproductionModuleMuted ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID )
		        {
			        sIDCall = sModuleID;
			        return bMuted;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bMutedResult;

		REQUIRE_NOTHROW( bMutedResult = client->GetCoreInstance( )->GetReproductionModuleMuted( sID ) );

		fakeit::Verify( Method( VACoreMock, GetReproductionModuleMuted ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( bMutedResult == bMuted );
	}

	SECTION( "SetReproductionModuleGain" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		double dGainCall;

		fakeit::When( Method( VACoreMock, SetReproductionModuleGain ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID, const double dGain )
		        {
			        sIDCall   = sModuleID;
			        dGainCall = dGain;
		        } );

		const std::string sID = GENERATE_RANDOM_STRING( 10 );
		const double dGain    = GENERATE_RANDOM( double, MAX_REPS );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetReproductionModuleGain( sID, dGain ) );

		fakeit::Verify( Method( VACoreMock, SetReproductionModuleGain ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( dGain == dGainCall );
	}

	SECTION( "GetReproductionModuleGain" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		double dGain = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetReproductionModuleGain ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID )
		        {
			        sIDCall = sModuleID;
			        return dGain;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		double dGainResult;

		REQUIRE_NOTHROW( dGainResult = client->GetCoreInstance( )->GetReproductionModuleGain( sID ) );

		fakeit::Verify( Method( VACoreMock, GetReproductionModuleGain ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( dGainResult == dGain );
	}

	SECTION( "SetReproductionModuleParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );
		;

		fakeit::When( Method( VACoreMock, SetReproductionModuleParameters ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		const auto id            = GENERATE_RANDOM_STRING( 10 );
		const CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetReproductionModuleParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetReproductionModuleParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetReproductionModuleParameters" )
	{
		fakeit::When( Method( VACoreMock, GetReproductionModuleParameters ) )
		    .AlwaysDo(
		        []( const std::string& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE_RANDOM_STRING( 10 );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetReproductionModuleParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetReproductionModuleParameters ) );

		REQUIRE( oParamsResult == oParams );
	}
}