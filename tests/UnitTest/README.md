# VANet Unit Tests

## Concept

The process of testing VANet is a bit convoluted.
We cannot/do not test the server and client side separately since the split happens at the communication through gRPC.
Hence why we test the complete system at once.
This is done by faking a `VACore`/`VAInterface` with FakeIt which is then given to an instance of the `VANetServer`.

```cpp
fakeit::Mock<IVAInterface> VACoreMock;

fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );

auto& core = VACoreMock.get( );

auto server = IVANetServer::Create( );
server->SetCoreInstance( &core );
server->Initialize( "0.0.0.0" );
```

Afterwards, a new `VANetClient` is created which connects to the previously created server.

```cpp
auto client = IVANetClient::Create( );
client->Initialize( "localhost" );
```

This setup is the same for every test case.
With it, the communication can be tested like this:

```cpp
fakeit::When( Method( VACoreMock, GetState ) ).Return( IVAInterface::VA_CORESTATE_CREATED );
int result;
REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetState( ) );
REQUIRE( result == IVAInterface::VA_CORESTATE_CREATED );
fakeit::Verify( Method( VACoreMock, GetState ) );
```

Here, we define the mock behavior (server side) to return a specific value, here `VA_CORESTATE_CREATED`.
Then we call the corresponding client method and verify that the correct result was returned.
As a last step, we verify that the mocked method was called correctly.

This is concept is the same for almost every test.
The exact details differ depending on the method under test.

Using this method, client and server side, as well as any en- and decoding are implicitly tested.
Nonetheless, this method has its drawback.
For example if some method parameter is swapped both on the client side as well as the server side, the unit test would still pass.
