#include "test_utils.h"

#include <thread>

struct TestEventHandler : IVAEventHandler
{
	CVAEvent currentEvent;
	std::atomic_bool eventReceived = false;
	void HandleVAEvent( const CVAEvent* pEvent ) override
	{
		currentEvent  = *pEvent;
		eventReceived = true;
	}
};


TEST_CASE( "VANet::VANet/EventHandlingProofOfConcept", "[VANet][Eventing][PrioMid]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	CVAEvent currentEvent         = generateRandomEvent( );
	std::atomic_bool eventSendOut = false;

	// Mock method has to be set before initiation of the client since it will get called during it.
	fakeit::When( Method( VACoreMock, AttachEventHandler ) )
	    .AlwaysDo(
	        [currentEvent, &eventSendOut]( IVAEventHandler* pEventHandler )
	        {
		        pEventHandler->HandleVAEvent( &currentEvent );
		        eventSendOut = true;
	        } );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	REQUIRE( server->Initialize( "0.0.0.0" ) );

	auto client = IVANetClient::Create( );
	REQUIRE( client->Initialize( "localhost" ) );

	SECTION( "Event handling returns/exits, server before client" )
	{
		server.reset( );
		client.reset( );
		REQUIRE( true );
	}

	SECTION( "Event handling returns/exits, client before server" )
	{
		client.reset( );
		server.reset( );
		REQUIRE( true );
	}
}

TEST_CASE( "VANet::VANet/EventHandling", "[VANet][Eventing][PrioMid]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	CVAEvent currentEvent         = generateRandomEvent( );
	std::atomic_bool eventSendOut = false;

	// Mock method has to be set before initiation of the client since it will get called during it.
	fakeit::When( Method( VACoreMock, AttachEventHandler ) )
	    .AlwaysDo(
	        [currentEvent, &eventSendOut]( IVAEventHandler* pEventHandler )
	        {
		        pEventHandler->HandleVAEvent( &currentEvent );
		        eventSendOut = true;
	        } );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	REQUIRE( server->Initialize( "0.0.0.0" ) );

	auto client = IVANetClient::Create( );
	REQUIRE( client->Initialize( "localhost" ) );

	SECTION( "Event is forwarded, implicitly tests VA::Event Decode/Encode" )
	{
		TestEventHandler handler;

		REQUIRE_NOTHROW( client->GetCoreInstance( )->AttachEventHandler( &handler ) );

		// Wait for the event to be handled.
		// TODO consider using a conditional variable here to potentially reduce CPU load
		while( !eventSendOut && !handler.eventReceived )
		{
			std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
		}

		fakeit::Verify( Method( VACoreMock, AttachEventHandler ) );

		REQUIRE( currentEvent == handler.currentEvent );
	}
}