#include "test_utils.h"


TEST_CASE( "VANet::VANet/Update", "[VANet][Update][PrioMid]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetUpdateLocked" )
	{
		bool bLocked = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetUpdateLocked ) ).Return( bLocked );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetUpdateLocked( ) );

		fakeit::Verify( Method( VACoreMock, GetUpdateLocked ) );

		REQUIRE( bResult == bLocked );
	}

	SECTION( "LockUpdate" )
	{
		fakeit::Fake( Method( VACoreMock, LockUpdate ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->LockUpdate( ) );

		fakeit::Verify( Method( VACoreMock, LockUpdate ) );
	}

	SECTION( "UnlockUpdate" )
	{
		const int iStateID = GENERATE( take( MAX_REPS, random( -1, 255 ) ) );

		fakeit::When( Method( VACoreMock, UnlockUpdate ) ).Return( iStateID );

		int iResult;

		REQUIRE_NOTHROW( iResult = client->GetCoreInstance( )->UnlockUpdate( ) );

		fakeit::Verify( Method( VACoreMock, UnlockUpdate ) );

		REQUIRE( iResult == iStateID );
	}
}
