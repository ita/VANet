#ifndef IW_VANET_UNIT_TEST_UTILS
#define IW_VANET_UNIT_TEST_UTILS

#include <VA.h>
#include <VANet.h>
#include <catch2/catch_test_macros.hpp>
#include <catch2/catch_translate_exception.hpp>
#include <catch2/generators/catch_generators_all.hpp>
#include <catch2/matchers/catch_matchers_all.hpp>
#include <fakeit.hpp>
#include <limits>

CATCH_TRANSLATE_EXCEPTION( const CVAException& ex )
{
	return ex.ToString( );
}

static constexpr int MAX_REPS = 2;

#define GENERATE_RANDOM( type, reps ) GENERATE_COPY( take( reps, random( std::numeric_limits<type>::min( ), std::numeric_limits<type>::max( ) ) ) )

#define GENERATE_RANDOM_BOOL( reps ) GENERATE_COPY( take( reps, map( []( const int& i ) { return static_cast<bool>( i ); }, random( 0, 1 ) ) ) )

#define GENERATE_RANDOM_STRING( length ) \
	GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( length, take( length, random( 32, 122 ) ) ) ) )

// Note, that you cannot generate a vector of random length!
#define GENERATE_RANDOM_VECTOR( type, length ) GENERATE( chunk( length, take( length, random( std::numeric_limits<type>::min( ), std::numeric_limits<type>::max( ) ) ) ) )

// These helper functions are required so that one can use range-for-loops for VAStruct's
inline CVAStruct::iterator begin( CVAStruct& s )
{
	return s.Begin( );
}

inline CVAStruct::iterator end( CVAStruct& s )
{
	return s.End( );
}

inline CVAStruct::const_iterator begin( const CVAStruct& s )
{
	return s.Begin( );
}

inline CVAStruct::const_iterator end( const CVAStruct& s )
{
	return s.End( );
}

template<typename T>
inline bool operator==( const std::vector<T>& rhs, const std::vector<T>& lhs )
{
	if( rhs.size( ) != lhs.size( ) )
		return false;

	for( int i = 0; i < rhs.size( ); ++i )
	{
		if( !( rhs.at( i ) == lhs.at( i ) ) )
			return false;
	}

	return true;
}

bool operator==( const CVAStruct& rhs, const CVAStruct& lhs );

inline bool operator==( const CVAStructValue& rhs, const CVAStructValue& lhs )
{
	if( rhs.GetDatatype( ) != lhs.GetDatatype( ) )
		return false;

	if( rhs.IsBool( ) && static_cast<bool>( rhs ) != static_cast<bool>( lhs ) )
		return false;
	if( rhs.IsInt( ) && static_cast<int>( rhs ) != static_cast<int>( lhs ) )
		return false;
	if( rhs.IsDouble( ) && static_cast<double>( rhs ) != static_cast<double>( lhs ) )
		return false;
	if( rhs.IsString( ) && static_cast<std::string>( rhs ) != static_cast<std::string>( lhs ) )
		return false;
	if( rhs.IsStruct( ) && !( rhs.GetStruct( ) == lhs.GetStruct( ) ) )
		return false;
	if( rhs.IsData( ) &&
	    std::string( static_cast<const char*>( rhs.GetData( ) ), rhs.GetDataSize( ) ) != std::string( static_cast<const char*>( lhs.GetData( ) ), lhs.GetDataSize( ) ) )
		return false;
	if( rhs.IsSampleBuffer( ) )
	{
		const CVASampleBuffer& rhsBuf( rhs );
		const CVASampleBuffer& lhsBuf( lhs );

		if( !( rhsBuf.vfSamples == lhsBuf.vfSamples ) )
			return false;
	}

	return true;
}

inline bool operator==( const CVAStruct& rhs, const CVAStruct& lhs )
{
	if( rhs.Size( ) != lhs.Size( ) )
		return false;

	for( const auto& [key, value]: rhs )
	{
		if( !lhs.HasKey( key ) )
			return false;

		if( !( lhs[key] == value ) )
			return false;
	}

	return true;
}

inline CVAStruct generateRandomStruct( )
{
	auto structType = GENERATE( take( 1, random( 0, 6 ) ) );
	auto name       = GENERATE_RANDOM_STRING( 10 );
	CVAStruct out;

	if( structType == CVAStructValue::BOOL )
	{
		out[name] = GENERATE_RANDOM_BOOL( 1 );
	}
	else if( structType == CVAStructValue::INT )
	{
		out[name] = GENERATE_RANDOM( int, 1 );
	}
	else if( structType == CVAStructValue::DOUBLE )
	{
		out[name] = GENERATE_RANDOM( double, 1 );
	}
	else if( structType == CVAStructValue::STRING )
	{
		out[name] = GENERATE_RANDOM_STRING( 10 );
	}
	else if( structType == CVAStructValue::STRUCT )
	{
		out[name] = CVAStruct( );
	}
	else if( structType == CVAStructValue::DATA )
	{
		auto raw_data =
		    GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( MAX_REPS, take( MAX_REPS, random( 0, 255 ) ) ) ) );
		out[name] = CVAStructValue( &raw_data, raw_data.size( ) );
		;
	}
	else if( structType == CVAStructValue::SAMPLEBUFFER )
	{
		out[name] = GENERATE( map(
		    []( const std::vector<double>& values )
		    {
			    CVASampleBuffer buf( values.size( ) );
			    for( int i = 0; i < values.size( ); ++i )
				    buf.vfSamples[i] = values[i];
			    return buf;
		    },
		    chunk( MAX_REPS, take( MAX_REPS, random( -1., 1. ) ) ) ) );
	}

	return out;
}

inline bool operator==( const CVAVersionInfo& lhs, const CVAVersionInfo& rhs )
{
	return lhs.sVersion == rhs.sVersion && lhs.sComments == rhs.sComments && lhs.sDate == rhs.sDate && lhs.sFlags == rhs.sFlags;
}

inline bool operator==( const CVAModuleInfo& lhs, const CVAModuleInfo& rhs )
{
	return lhs.sName == rhs.sName && lhs.sDesc == rhs.sDesc && lhs.iID == rhs.iID;
}

inline bool operator==( const VAQuat& lhs, const VAQuat& rhs )
{
	return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z && lhs.w == rhs.w;
}

inline VAVec3 generateVAVec3( bool repetitions = true )
{
	VAVec3 vec;
	vec.x = GENERATE_RANDOM( double, repetitions ? MAX_REPS : 1 );
	vec.y = GENERATE_RANDOM( double, 1 );
	vec.z = GENERATE_RANDOM( double, 1 );

	return vec;
}

inline VAQuat generateVAQuaternion( bool repetitions = true )
{
	VAQuat quat;
	quat.x = GENERATE_RANDOM( double, repetitions ? MAX_REPS : 1 );
	quat.y = GENERATE_RANDOM( double, 1 );
	quat.z = GENERATE_RANDOM( double, 1 );
	quat.w = GENERATE_RANDOM( double, 1 );

	return quat;
}

inline CVAEvent generateRandomEvent( )
{
	CVAEvent out;

	out.pSender                = reinterpret_cast<IVAInterface*>( GENERATE_RANDOM( uint64_t, MAX_REPS ) );
	out.iEventID               = GENERATE_RANDOM( int, 1 );
	out.iEventType             = GENERATE_RANDOM( uint64_t, 1 );
	out.iObjectID              = GENERATE_RANDOM( int, 1 );
	out.sObjectID              = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	out.iParamID               = GENERATE_RANDOM( int, 1 );
	out.sParam                 = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	out.iIndex                 = GENERATE_RANDOM( int, 1 );
	out.iAuralizationMode      = GENERATE_RANDOM( int, 1 );
	out.dVolume                = GENERATE_RANDOM( double, 1 );
	out.dState                 = GENERATE_RANDOM( double, 1 );
	out.bMuted                 = static_cast<bool>( GENERATE( take( 1, random( 0, 1 ) ) ) );
	out.sName                  = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	out.sFilePath              = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	out.vPos                   = generateVAVec3( false );
	out.vView                  = generateVAVec3( false );
	out.vUp                    = generateVAVec3( false );
	out.qHATO                  = generateVAQuaternion( false );
	out.oOrientation           = generateVAQuaternion( false );
	out.vfInputPeaks           = GENERATE_RANDOM_VECTOR( float, 2 * MAX_REPS );
	out.vfInputRMSs            = GENERATE_RANDOM_VECTOR( float, 2 * MAX_REPS );
	out.vfOutputPeaks          = GENERATE_RANDOM_VECTOR( float, 2 * MAX_REPS );
	out.vfOutputRMSs           = GENERATE_RANDOM_VECTOR( float, 2 * MAX_REPS );
	out.fSysLoad               = GENERATE_RANDOM( float, 1 );
	out.fDSPLoad               = GENERATE_RANDOM( float, 1 );
	out.dCoreClock             = GENERATE_RANDOM( double, 1 );
	out.oProgress.iCurrentStep = GENERATE_RANDOM( int, 1 );
	out.oProgress.iMaxStep     = GENERATE_RANDOM( int, 1 );
	out.oProgress.sAction      = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	out.oProgress.sSubaction   = GENERATE_RANDOM_STRING( 2 * MAX_REPS );
	// out.oPrototypeParams       = generateRandomStruct( );

	return out;
}

inline bool operator==( const CVAEvent& lhs, const CVAEvent& rhs )
{
	return lhs.pSender == rhs.pSender && lhs.iEventID == rhs.iEventID && lhs.iEventType == rhs.iEventType && lhs.iObjectID == rhs.iObjectID &&
	       lhs.sObjectID == rhs.sObjectID && lhs.iParamID == rhs.iParamID && lhs.sParam == rhs.sParam && lhs.iIndex == rhs.iIndex &&
	       lhs.iAuralizationMode == rhs.iAuralizationMode && lhs.dVolume == rhs.dVolume && lhs.dState == rhs.dState && lhs.bMuted == rhs.bMuted &&
	       lhs.sName == rhs.sName && lhs.sFilePath == rhs.sFilePath && lhs.vPos == rhs.vPos && lhs.vView == rhs.vView && lhs.vUp == rhs.vUp && lhs.qHATO == rhs.qHATO &&
	       lhs.oOrientation == rhs.oOrientation && lhs.vfInputPeaks == rhs.vfInputPeaks && lhs.vfInputRMSs == rhs.vfInputRMSs && lhs.vfOutputPeaks == rhs.vfOutputPeaks &&
	       lhs.vfOutputRMSs == rhs.vfOutputRMSs && lhs.fSysLoad == rhs.fSysLoad && lhs.fDSPLoad == rhs.fDSPLoad && lhs.dCoreClock == rhs.dCoreClock &&
	       lhs.oProgress.iCurrentStep == rhs.oProgress.iCurrentStep && lhs.oProgress.iMaxStep == rhs.oProgress.iMaxStep &&
	       lhs.oProgress.sAction == rhs.oProgress.sAction && lhs.oProgress.sSubaction == rhs.oProgress.sSubaction && lhs.oPrototypeParams == rhs.oPrototypeParams;
}

inline CVADirectivityInfo generateRandomDirectivityInfo( )
{
	CVADirectivityInfo directivityInfo;

	directivityInfo.iID         = GENERATE( take( 1, random( 0, 255 ) ) );
	directivityInfo.iClass      = GENERATE( take( 1, random( -1, 3 ) ) );
	directivityInfo.sName       = GENERATE_RANDOM_STRING( 10 );
	directivityInfo.sDesc       = GENERATE_RANDOM_STRING( 10 );
	directivityInfo.iReferences = GENERATE( take( 1, random( 0, 255 ) ) );
	directivityInfo.oParams     = generateRandomStruct( );

	return directivityInfo;
}

inline CVAAcousticMaterial generateRandomAcousticMaterial( )
{
	CVAAcousticMaterial acousticMaterial;

	acousticMaterial.iID     = GENERATE( take( 1, random( 0, 255 ) ) );
	acousticMaterial.iType   = GENERATE( CVAAcousticMaterial::Type::UNSPECIFIED, CVAAcousticMaterial::Type::PROTOTYPE,
                                       CVAAcousticMaterial::Type::ENERGETIC_BAND_FILTER_WHOLE_OCTAVE, CVAAcousticMaterial::Type::ENERGETIC_BAND_FILTER_THIRD_OCTAVE );
	acousticMaterial.sName   = GENERATE_RANDOM_STRING( 10 );
	acousticMaterial.oParams = generateRandomStruct( );
	acousticMaterial.vfAbsorptionValues   = GENERATE_RANDOM_VECTOR( float, 10 );
	acousticMaterial.vfScatteringValues   = GENERATE_RANDOM_VECTOR( float, 10 );
	acousticMaterial.vfTransmissionValues = GENERATE_RANDOM_VECTOR( float, 10 );

	return acousticMaterial;
}

inline CVAGeometryMesh::CVAVertex generateRandomVertex( )
{
	CVAGeometryMesh::CVAVertex vertex;

	vertex.iID     = GENERATE( take( 1, random( 0, 255 ) ) );
	vertex.v3Point = generateVAVec3( false );

	return vertex;
}


inline CVAGeometryMesh::CVAFace generateRandomFace( )
{
	CVAGeometryMesh::CVAFace face;

	face.iID         = GENERATE( take( 1, random( 0, 255 ) ) );
	face.iMaterialID = GENERATE( take( 1, random( 0, 255 ) ) );

	for( int i = 0; i < GENERATE( take( 1, random( 0, 5 ) ) ); ++i )
	{
		face.viVertexList.push_back( GENERATE( take( 1, random( 0, 255 ) ) ) );
	}

	return face;
}

inline CVAGeometryMesh generateRandomGeometryMesh( )
{
	CVAGeometryMesh geometryMesh;

	geometryMesh.iID      = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
	geometryMesh.bEnabled = GENERATE( true, false );

	for( int i = 0; i < GENERATE( take( 1, random( 0, 5 ) ) ); ++i )
	{
		geometryMesh.voVertices.push_back( generateRandomVertex( ) );
	}

	for( int i = 0; i < GENERATE( take( 1, random( 0, 5 ) ) ); ++i )
	{
		geometryMesh.voFaces.push_back( generateRandomFace( ) );
	}

	geometryMesh.oParams = generateRandomStruct( );

	return geometryMesh;
}

inline bool operator==( const CVAGeometryMesh& rhs, const CVAGeometryMesh& lhs )
{
	return ( rhs.iID == lhs.iID ) && ( rhs.bEnabled == lhs.bEnabled ) && ( rhs.voVertices == lhs.voVertices ) && ( rhs.voFaces == lhs.voFaces ) &&
	       ( rhs.oParams == lhs.oParams );
}

inline bool operator==( const CVAGeometryMesh::CVAVertex& rhs, const CVAGeometryMesh::CVAVertex& lhs )
{
	return ( rhs.iID == lhs.iID ) && ( rhs.v3Point == lhs.v3Point );
}

inline bool operator==( const CVAGeometryMesh::CVAFace& rhs, const CVAGeometryMesh::CVAFace& lhs )
{
	return ( rhs.iID == lhs.iID ) && ( rhs.iMaterialID == lhs.iMaterialID ) && ( rhs.viVertexList == lhs.viVertexList );
}


#endif