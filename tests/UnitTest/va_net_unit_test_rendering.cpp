#include "test_utils.h"


TEST_CASE( "VANet::VANet/Rendering", "[VANet][Rendering][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetRenderingModules" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::vector<CVAAudioRendererInfo> result, actual;

		const auto numModules = GENERATE( take( 1, random( 0, 3 ) ) );
		bool bFilterCall;

		for( int i = 0; i < 2; ++i )
		{
			CVAAudioRendererInfo info;
			info.sID                      = GENERATE_RANDOM_STRING( 10 );
			info.sClass                   = GENERATE_RANDOM_STRING( 20 );
			info.sDescription             = GENERATE_RANDOM_STRING( 10 );
			info.bEnabled                 = GENERATE_RANDOM_BOOL( MAX_REPS );
			info.bOutputDetectorEnabled   = GENERATE_RANDOM_BOOL( 1 );
			info.bOutputRecordingEnabled  = GENERATE_RANDOM_BOOL( 1 );
			info.sOutputRecordingFilePath = GENERATE_RANDOM_STRING( 30 );
			info.oParams                  = generateRandomStruct( );

			actual.push_back( info );
		}

		fakeit::When( Method( VACoreMock, GetRenderingModules ) )
		    .AlwaysDo(
		        [&]( std::vector<CVAAudioRendererInfo>& voRenderer, const bool bFilterEnabled = true )
		        {
			        voRenderer  = actual;
			        bFilterCall = bFilterEnabled;
		        } );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetRenderingModules( result ) );

		fakeit::Verify( Method( VACoreMock, GetRenderingModules ) );


		REQUIRE( result.size( ) == actual.size( ) );

		for( int i = 0; i < result.size( ); ++i )
		{
			REQUIRE( result.at( i ).sID == actual.at( i ).sID );
			REQUIRE( result.at( i ).sClass == actual.at( i ).sClass );
			REQUIRE( result.at( i ).sDescription == actual.at( i ).sDescription );
			REQUIRE( result.at( i ).bEnabled == actual.at( i ).bEnabled );
			REQUIRE( result.at( i ).bOutputDetectorEnabled == actual.at( i ).bOutputDetectorEnabled );
			REQUIRE( result.at( i ).bOutputRecordingEnabled == actual.at( i ).bOutputRecordingEnabled );
			REQUIRE( result.at( i ).sOutputRecordingFilePath == actual.at( i ).sOutputRecordingFilePath );
			REQUIRE( result.at( i ).oParams == actual.at( i ).oParams );
		}
	}

	SECTION( "SetRenderingModuleMuted" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bMutedCall;

		fakeit::When( Method( VACoreMock, SetRenderingModuleMuted ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID, const bool bMuted )
		        {
			        sIDCall    = sModuleID;
			        bMutedCall = bMuted;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bMuted     = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetRenderingModuleMuted( sID, bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetRenderingModuleMuted ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( bMuted == bMutedCall );
	}

	SECTION( "GetRenderingModuleMuted" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetRenderingModuleMuted ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID )
		        {
			        sIDCall = sModuleID;
			        return bMuted;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bMutedResult;

		REQUIRE_NOTHROW( bMutedResult = client->GetCoreInstance( )->GetRenderingModuleMuted( sID ) );

		fakeit::Verify( Method( VACoreMock, GetRenderingModuleMuted ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( bMutedResult == bMuted );
	}

	SECTION( "SetRenderingModuleAuralizationMode" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		int iAuralizationModeCall;

		fakeit::When( Method( VACoreMock, SetRenderingModuleAuralizationMode ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID, const int iAuralizationMode )
		        {
			        sIDCall               = sModuleID;
			        iAuralizationModeCall = iAuralizationMode;
		        } );

		std::string sID       = GENERATE_RANDOM_STRING( 10 );
		int iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetRenderingModuleAuralizationMode( sID, iAuralizationMode ) );

		fakeit::Verify( Method( VACoreMock, SetRenderingModuleAuralizationMode ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( iAuralizationMode == iAuralizationModeCall );
	}

	SECTION( "GetRenderingModuleAuralizationMode" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		int iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetRenderingModuleAuralizationMode ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID )
		        {
			        sIDCall = sModuleID;
			        return iAuralizationMode;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		int iAuralizationModeResult;

		REQUIRE_NOTHROW( iAuralizationModeResult = client->GetCoreInstance( )->GetRenderingModuleAuralizationMode( sID ) );

		fakeit::Verify( Method( VACoreMock, GetRenderingModuleAuralizationMode ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( iAuralizationModeResult == iAuralizationMode );
	}

	SECTION( "SetRenderingModuleParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );
		;

		fakeit::When( Method( VACoreMock, SetRenderingModuleParameters ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		const auto id            = GENERATE_RANDOM_STRING( 10 );
		const CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetRenderingModuleParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetRenderingModuleParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetRenderingModuleParameters" )
	{
		fakeit::When( Method( VACoreMock, GetRenderingModuleParameters ) )
		    .AlwaysDo(
		        []( const std::string& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE_RANDOM_STRING( 10 );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetRenderingModuleParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetRenderingModuleParameters ) );

		REQUIRE( oParamsResult == oParams );
	}

	SECTION( "SetRenderingModuleGain" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		double dGainCall;

		fakeit::When( Method( VACoreMock, SetRenderingModuleGain ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID, const double dGain )
		        {
			        sIDCall   = sModuleID;
			        dGainCall = dGain;
		        } );

		const std::string sID = GENERATE_RANDOM_STRING( 10 );
		const double dGain    = GENERATE_RANDOM( double, MAX_REPS );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetRenderingModuleGain( sID, dGain ) );

		fakeit::Verify( Method( VACoreMock, SetRenderingModuleGain ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( dGain == dGainCall );
	}

	SECTION( "GetRenderingModuleGain" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		double dGain = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetRenderingModuleGain ) )
		    .AlwaysDo(
		        [&]( const std::string& sModuleID )
		        {
			        sIDCall = sModuleID;
			        return dGain;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		double dGainResult;

		REQUIRE_NOTHROW( dGainResult = client->GetCoreInstance( )->GetRenderingModuleGain( sID ) );

		fakeit::Verify( Method( VACoreMock, GetRenderingModuleGain ) );

		REQUIRE( sID == sIDCall );
		REQUIRE( dGainResult == dGain );
	}
}
