#include "test_utils.h"

TEST_CASE( "VANet::VANet/HomogeneusMedium", "[VANet][HomogeneousMedium][PrioMid]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "SetHomogeneousMediumSoundSpeed" )
	{
		const double dSoundSpeed = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::Fake( Method( VACoreMock, SetHomogeneousMediumSoundSpeed ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumSoundSpeed( dSoundSpeed ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumSoundSpeed ).Using( dSoundSpeed ) );
	}

	SECTION( "GetHomogeneousMediumSoundSpeed" )
	{
		double dSoundSpeed = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetHomogeneousMediumSoundSpeed ) ).Return( dSoundSpeed );

		double dSoundSpeedResult;

		REQUIRE_NOTHROW( dSoundSpeedResult = client->GetCoreInstance( )->GetHomogeneousMediumSoundSpeed( ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumSoundSpeed ) );

		REQUIRE( dSoundSpeedResult == dSoundSpeed );
	}

	SECTION( "SetHomogeneousMediumTemperature" )
	{
		const double dMediumTemperature = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::Fake( Method( VACoreMock, SetHomogeneousMediumTemperature ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumTemperature( dMediumTemperature ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumTemperature ).Using( dMediumTemperature ) );
	}

	SECTION( "GetHomogeneousMediumTemperature" )
	{
		double dMediumTemperature = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetHomogeneousMediumTemperature ) ).Return( dMediumTemperature );

		double dMediumTemperatureResult;

		REQUIRE_NOTHROW( dMediumTemperatureResult = client->GetCoreInstance( )->GetHomogeneousMediumTemperature( ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumTemperature ) );

		REQUIRE( dMediumTemperatureResult == dMediumTemperature );
	}

	SECTION( "SetHomogeneousMediumStaticPressure" )
	{
		const double dStaticPressure = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::Fake( Method( VACoreMock, SetHomogeneousMediumStaticPressure ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumStaticPressure( dStaticPressure ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumStaticPressure ).Using( dStaticPressure ) );
	}

	SECTION( "GetHomogeneousMediumStaticPressure" )
	{
		double dStaticPressure = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetHomogeneousMediumStaticPressure ) ).Return( dStaticPressure );

		double dStaticPressureResult;

		REQUIRE_NOTHROW( dStaticPressureResult = client->GetCoreInstance( )->GetHomogeneousMediumStaticPressure( ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumStaticPressure ) );

		REQUIRE( dStaticPressureResult == dStaticPressure );
	}

	SECTION( "SetHomogeneousMediumRelativeHumidity" )
	{
		const double dRelativeHumidityPercent = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::Fake( Method( VACoreMock, SetHomogeneousMediumRelativeHumidity ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumRelativeHumidity( dRelativeHumidityPercent ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumRelativeHumidity ).Using( dRelativeHumidityPercent ) );
	}

	SECTION( "GetHomogeneousMediumRelativeHumidity" )
	{
		double dRelativeHumidity = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetHomogeneousMediumRelativeHumidity ) ).Return( dRelativeHumidity );

		double dRelativeHumidityResult;

		REQUIRE_NOTHROW( dRelativeHumidityResult = client->GetCoreInstance( )->GetHomogeneousMediumRelativeHumidity( ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumRelativeHumidity ) );

		REQUIRE( dRelativeHumidityResult == dRelativeHumidity );
	}

	SECTION( "SetHomogeneousMediumShiftSpeed" )
	{
		// See SetSoundReceiverPosition unit test note.
		VAVec3 vecCall;
		fakeit::When( Method( VACoreMock, SetHomogeneousMediumShiftSpeed ) ).AlwaysDo( [&]( const VAVec3& vec ) { vecCall = vec; } );

		VAVec3 vec = generateVAVec3( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumShiftSpeed( vec ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumShiftSpeed ) );

		REQUIRE( vec == vecCall );
	}

	SECTION( "GetHomogeneousMediumShiftSpeed" )
	{
		VAVec3 v3TranslationSpeed = generateVAVec3( );

		fakeit::When( Method( VACoreMock, GetHomogeneousMediumShiftSpeed ) ).Return( v3TranslationSpeed );

		VAVec3 v3TranslationSpeedResult;

		REQUIRE_NOTHROW( v3TranslationSpeedResult = client->GetCoreInstance( )->GetHomogeneousMediumShiftSpeed( ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumShiftSpeed ) );

		REQUIRE( v3TranslationSpeedResult == v3TranslationSpeed );
	}

	SECTION( "SetHomogeneousMediumParameters" )
	{
		CVAStruct& oParamsCall = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, SetHomogeneousMediumParameters ) ).AlwaysDo( [&]( const CVAStruct& oParams ) { oParamsCall = oParams; } );

		CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetHomogeneousMediumParameters( oParams ) );

		fakeit::Verify( Method( VACoreMock, SetHomogeneousMediumParameters ) );

		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetHomogeneousMediumParameters" )
	{
		fakeit::When( Method( VACoreMock, GetHomogeneousMediumParameters ) ).AlwaysDo( []( CVAStruct params ) { return params; } );

		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetHomogeneousMediumParameters( oParams ) );

		fakeit::Verify( Method( VACoreMock, GetHomogeneousMediumParameters ) );

		REQUIRE( oParamsResult == oParams );
	}
}