#include "test_utils.h"


TEST_CASE( "VANet::VANet/SignalSource", "[VANet][SignalSource][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "CreateSignalSourceBufferFromParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;
		CVAStruct callArg_Parameter;
		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceBufferFromParameters ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& params, const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        callArg_Parameter        = params;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );
		CVAStruct params     = generateRandomStruct( );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceBufferFromParameters( params, id ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceBufferFromParameters ) );

		REQUIRE( id == callArg_SignalSourceName );
		REQUIRE( params == callArg_Parameter );
	}

	SECTION( "CreateSignalSourcePrototypeFromParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;
		CVAStruct callArg_Parameter;
		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourcePrototypeFromParameters ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& params, const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        callArg_Parameter        = params;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );
		CVAStruct params     = generateRandomStruct( );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourcePrototypeFromParameters( params, id ) );


		fakeit::Verify( Method( VACoreMock, CreateSignalSourcePrototypeFromParameters ) );

		REQUIRE( result == returnValue );
		REQUIRE( id == callArg_SignalSourceName );
		REQUIRE( params == callArg_Parameter );
	}

	SECTION( "CreateSignalSourceTextToSpeech" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;

		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceTextToSpeech ) )
		    .AlwaysDo(
		        [&]( const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceTextToSpeech( id ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceTextToSpeech ) );

		REQUIRE( id == callArg_SignalSourceName );
	}

	SECTION( "CreateSignalSourceSequencer" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceSequencerName;

		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceSequencer ) )
		    .AlwaysDo(
		        [&]( const std::string& name )
		        {
			        callArg_SignalSourceSequencerName = name;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceSequencer( id ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceSequencer ) );

		REQUIRE( id == callArg_SignalSourceSequencerName );
	}

	SECTION( "CreateSignalSourceNetworkStream" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sInterfaceCall;
		int iPortCall;
		std::string sNameCall;


		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceNetworkStream ) )
		    .AlwaysDo(
		        [&]( const std::string& sInterface, const int iPort, const std::string& sName )
		        {
			        sInterfaceCall = sInterface;
			        iPortCall      = iPort;
			        sNameCall      = sName;
			        return returnValue;
		        } );

		const std::string sInterface = GENERATE_RANDOM_STRING( 15 );
		const int iPort              = GENERATE( take( MAX_REPS, random( 12480, 12500 ) ) );
		const std::string sName      = GENERATE_RANDOM_STRING( 10 );


		std::string result;

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceNetworkStream( sInterface, iPort, sName ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceNetworkStream ) );

		REQUIRE( sInterface == sInterfaceCall );
		REQUIRE( iPort == iPortCall );
		REQUIRE( sName == sNameCall );
	}

	SECTION( "CreateSignalSourceEngine" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;
		CVAStruct callArg_Parameter;
		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceEngine ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& params, const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        callArg_Parameter        = params;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );
		CVAStruct params     = generateRandomStruct( );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceEngine( params, id ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceEngine ) );

		REQUIRE( id == callArg_SignalSourceName );
		REQUIRE( params == callArg_Parameter );
	}

	SECTION( "CreateSignalSourceMachine" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;
		CVAStruct callArg_Parameter;
		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceMachine ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& params, const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        callArg_Parameter        = params;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );
		CVAStruct params     = generateRandomStruct( );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceMachine( params, id ) );

		REQUIRE( result == returnValue );
		fakeit::Verify( Method( VACoreMock, CreateSignalSourceMachine ) );

		REQUIRE( id == callArg_SignalSourceName );
		REQUIRE( params == callArg_Parameter );
	}

	SECTION( "DeleteSignalSource" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bEnabled = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, DeleteSignalSource ) )
		    .AlwaysDo(
		        [&]( const std::string& sID )
		        {
			        sIDCall = sID;
			        return bEnabled;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bEnabledResult;

		REQUIRE_NOTHROW( bEnabledResult = client->GetCoreInstance( )->DeleteSignalSource( sID ) );

		fakeit::Verify( Method( VACoreMock, DeleteSignalSource ) );

		REQUIRE( sIDCall == sID );
		REQUIRE( bEnabledResult == bEnabled );
	}

	SECTION( "GetSignalSourceInfo" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;

		CVASignalSourceInfo signalSourceInfoResult, signalSourceInfoActual;

		signalSourceInfoActual.sID         = GENERATE_RANDOM_STRING( 10 );
		signalSourceInfoActual.iType       = GENERATE( CVASignalSourceInfo::Type::UNSPECIFIED, CVASignalSourceInfo::Type::FILE, CVASignalSourceInfo::Type::SAMPLER,
                                                 CVASignalSourceInfo::Type::DEVICE_INPUT, CVASignalSourceInfo::Type::NETSTREAM, CVASignalSourceInfo::Type::MACHINE,
                                                 CVASignalSourceInfo::Type::ENGINE );
		signalSourceInfoActual.sName       = GENERATE_RANDOM_STRING( 10 );
		signalSourceInfoActual.sDesc       = GENERATE_RANDOM_STRING( 10 );
		signalSourceInfoActual.sState      = GENERATE_RANDOM_STRING( 10 );
		signalSourceInfoActual.iReferences = GENERATE( take( 1, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetSignalSourceInfo ) )
		    .AlwaysDo(
		        [&]( const std::string& sSignalSourceID )
		        {
			        sIDCall = sSignalSourceID;

			        CVASignalSourceInfo signalSourceInfo;

			        signalSourceInfo.sID         = signalSourceInfoActual.sID;
			        signalSourceInfo.iType       = signalSourceInfoActual.iType;
			        signalSourceInfo.sName       = signalSourceInfoActual.sName;
			        signalSourceInfo.sDesc       = signalSourceInfoActual.sDesc;
			        signalSourceInfo.sState      = signalSourceInfoActual.sState;
			        signalSourceInfo.iReferences = signalSourceInfoActual.iReferences;

			        return signalSourceInfo;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );

		REQUIRE_NOTHROW( signalSourceInfoResult = client->GetCoreInstance( )->GetSignalSourceInfo( sID ) );

		fakeit::Verify( Method( VACoreMock, GetSignalSourceInfo ) );

		REQUIRE( signalSourceInfoResult.sID == signalSourceInfoActual.sID );
		REQUIRE( signalSourceInfoResult.iType == signalSourceInfoActual.iType );
		REQUIRE( signalSourceInfoResult.sName == signalSourceInfoActual.sName );
		REQUIRE( signalSourceInfoResult.sDesc == signalSourceInfoActual.sDesc );
		REQUIRE( signalSourceInfoResult.sState == signalSourceInfoActual.sState );
		REQUIRE( signalSourceInfoResult.iReferences == signalSourceInfoActual.iReferences );
	}

	SECTION( "GetSignalSourceInfos" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::vector<CVASignalSourceInfo> result, actual;

		const auto numSignalSources = GENERATE( take( 2, random( 0, 3 ) ) );

		for( int i = 0; i < numSignalSources; ++i )
		{
			CVASignalSourceInfo info;
			info.sID         = GENERATE_RANDOM_STRING( 10 );
			auto options     = std::vector<CVASignalSourceInfo::Type> { CVASignalSourceInfo::Type::UNSPECIFIED, CVASignalSourceInfo::Type::FILE,
                                                                    CVASignalSourceInfo::Type::SAMPLER,     CVASignalSourceInfo::Type::DEVICE_INPUT,
                                                                    CVASignalSourceInfo::Type::NETSTREAM,   CVASignalSourceInfo::Type::MACHINE,
                                                                    CVASignalSourceInfo::Type::ENGINE };
			info.iType       = GENERATE_COPY( map( [options]( const int& i ) { return options.at( i ); }, take( 1, random( 0, static_cast<int>( options.size( ) - 1 ) ) ) ) );
			info.sName       = GENERATE_RANDOM_STRING( 10 );
			info.sDesc       = GENERATE_RANDOM_STRING( 10 );
			info.sState      = GENERATE_RANDOM_STRING( 10 );
			info.iReferences = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
			actual.push_back( info );
		}

		fakeit::When( Method( VACoreMock, GetSignalSourceInfos ) ).AlwaysDo( [&]( std::vector<CVASignalSourceInfo>& signalSourceInfos ) { signalSourceInfos = actual; } );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSignalSourceInfos( result ) );

		fakeit::Verify( Method( VACoreMock, GetSignalSourceInfos ) );

		// REQUIRE( result == actual );

		REQUIRE( result.size( ) == actual.size( ) );

		for( int i = 0; i < result.size( ); ++i )
		{
			REQUIRE( result.at( i ).sID == actual.at( i ).sID );
			REQUIRE( result.at( i ).iType == actual.at( i ).iType );
			REQUIRE( result.at( i ).sName == actual.at( i ).sName );
			REQUIRE( result.at( i ).sDesc == actual.at( i ).sDesc );
			REQUIRE( result.at( i ).sState == actual.at( i ).sState );
			REQUIRE( result.at( i ).iReferences == actual.at( i ).iReferences );
		}
	}

	SECTION( "GetSignalSourceBufferPlaybackState" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		const int iReturnState = GENERATE( IVAInterface::VA_PLAYBACK_ACTION_NONE, IVAInterface::VA_PLAYBACK_ACTION_STOP, IVAInterface::VA_PLAYBACK_ACTION_PAUSE,
		                                   IVAInterface::VA_PLAYBACK_ACTION_PLAY );

		fakeit::When( Method( VACoreMock, GetSignalSourceBufferPlaybackState ) )
		    .AlwaysDo(
		        [&]( const std::string& sID )
		        {
			        sIDCall = sID;
			        return iReturnState;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		int iReturnStateResult;

		REQUIRE_NOTHROW( iReturnStateResult = client->GetCoreInstance( )->GetSignalSourceBufferPlaybackState( sID ) );

		fakeit::Verify( Method( VACoreMock, GetSignalSourceBufferPlaybackState ) );

		REQUIRE( sIDCall == sID );
		REQUIRE( iReturnState == iReturnStateResult );
	}

	SECTION( "SetSignalSourceBufferPlaybackAction" )
	{
		// See SetSoundReceiverPosition unit test note.
		int callArg_PlaybackAction;
		std::string callArg_Id;
		fakeit::When( Method( VACoreMock, SetSignalSourceBufferPlaybackAction ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const int& playbackAction )
		        {
			        callArg_PlaybackAction = playbackAction;
			        callArg_Id             = id;
		        } );

		const auto id            = GENERATE_RANDOM_STRING( 10 );
		const int playbackAction = GENERATE( IVAInterface::VA_PLAYBACK_ACTION_NONE, IVAInterface::VA_PLAYBACK_ACTION_STOP, IVAInterface::VA_PLAYBACK_ACTION_PAUSE,
		                                     IVAInterface::VA_PLAYBACK_ACTION_PLAY );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceBufferPlaybackAction( id, playbackAction ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceBufferPlaybackAction ) );

		REQUIRE( id == callArg_Id );
		REQUIRE( playbackAction == callArg_PlaybackAction );
	}

	SECTION( "SetSignalSourceBufferPlaybackPosition" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		double dPlaybackPositionCall;

		fakeit::When( Method( VACoreMock, SetSignalSourceBufferPlaybackPosition ) )
		    .AlwaysDo(
		        [&]( const std::string& sID, const double dPlaybackPosition )
		        {
			        sIDCall               = sID;
			        dPlaybackPositionCall = dPlaybackPosition;
		        } );

		const std::string sID          = GENERATE_RANDOM_STRING( 10 );
		const double dPlaybackPosition = GENERATE_RANDOM( double, 1 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceBufferPlaybackPosition( sID, dPlaybackPosition ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceBufferPlaybackPosition ) );

		REQUIRE( sIDCall == sID );
		REQUIRE( dPlaybackPositionCall == dPlaybackPosition );
	}

	SECTION( "GetSignalSourceBufferLooping" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		bool bLooping = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetSignalSourceBufferLooping ) )
		    .AlwaysDo(
		        [&]( const std::string& sID )
		        {
			        sIDCall = sID;
			        return bLooping;
		        } );

		std::string sID = GENERATE_RANDOM_STRING( 10 );
		bool bLoopingResult;

		REQUIRE_NOTHROW( bLoopingResult = client->GetCoreInstance( )->GetSignalSourceBufferLooping( sID ) );

		fakeit::Verify( Method( VACoreMock, GetSignalSourceBufferLooping ) );

		REQUIRE( sIDCall == sID );
		REQUIRE( bLoopingResult == bLooping );
	}

	SECTION( "SetSignalSourceBufferLooping" )
	{
		// See SetSoundReceiverPosition unit test note.
		bool loopingCall;
		std::string idCall;
		fakeit::When( Method( VACoreMock, SetSignalSourceBufferLooping ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const bool& looping )
		        {
			        loopingCall = looping;
			        idCall      = id;
		        } );

		const auto id      = GENERATE_RANDOM_STRING( 10 );
		const auto looping = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceBufferLooping( id, looping ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceBufferLooping ) );

		REQUIRE( id == idCall );
		REQUIRE( looping == loopingCall );
	}

	SECTION( "SetSignalSourceParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );
		;

		fakeit::When( Method( VACoreMock, SetSignalSourceParameters ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		const auto id            = GENERATE_RANDOM_STRING( 10 );
		const CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetSignalSourceParameters" )
	{
		fakeit::When( Method( VACoreMock, GetSignalSourceParameters ) )
		    .AlwaysDo(
		        []( const std::string& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE_RANDOM_STRING( 10 );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetSignalSourceParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetSignalSourceParameters ) );

		REQUIRE( oParamsResult == oParams );
	}

	/* SECTION( "AddSignalSourceSequencerSample" )
	{
	    const auto iSampleIDActual = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
	    std::string idCall;
	    CVAStruct oParamsCall;

	    fakeit::When( Method( VACoreMock, AddSignalSourceSequencerSample ) )
	        .AlwaysDo(
	            [&]( const std::string& sSignalSourceID, const CVAStruct& oArgs )
	            {
	                idCall      = sSignalSourceID;
	                oParamsCall = oArgs;
	                return iSampleIDActual;
	            } );

	    auto id            = GENERATE_RANDOM_STRING( 10 );
	    CVAStruct& oParams = generateRandomStruct( );
	    int iSampleID;

	    REQUIRE_NOTHROW( iSampleID = client->GetCoreInstance( )->AddSignalSourceSequencerSample( id, oParams ) );

	    fakeit::Verify( Method( VACoreMock, AddSignalSourceSequencerSample ) );

	    REQUIRE( idCall == id );
	    REQUIRE( oParamsCall == oParams );
	    REQUIRE( iSampleID == iSampleIDActual );
	}*/


	SECTION( "AddSignalSourceSequencerPlayback" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDResult;
		int iSoundIDResult;
		int iFlagsResult;
		double dTimeCodeResult;

		const int iPlaybackID = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, AddSignalSourceSequencerPlayback ) )
		    .AlwaysDo(
		        [&]( const std::string& sSignalSourceID, const int iSoundID, const int iFlags, const double dTimeCode )
		        {
			        sIDResult       = sSignalSourceID;
			        iSoundIDResult  = iSoundID;
			        iFlagsResult    = iFlags;
			        dTimeCodeResult = dTimeCode;

			        return iPlaybackID;
		        } );

		const std::string sIDActual  = GENERATE_RANDOM_STRING( 10 );
		const int iSoundIDActual     = GENERATE( take( 1, random( 0, 255 ) ) );
		const int iFlagsActual       = GENERATE( take( 1, random( 0, 255 ) ) );
		const double dTimeCodeActual = GENERATE_RANDOM( double, MAX_REPS );

		int iPlaybackIDResult;

		REQUIRE_NOTHROW( iPlaybackIDResult = client->GetCoreInstance( )->AddSignalSourceSequencerPlayback( sIDActual, iSoundIDActual, iFlagsActual, dTimeCodeActual ) );

		fakeit::Verify( Method( VACoreMock, AddSignalSourceSequencerPlayback ) );

		REQUIRE( sIDResult == sIDActual );
		REQUIRE( iSoundIDResult == iSoundIDActual );
		REQUIRE( iFlagsResult == iFlagsActual );
		REQUIRE( dTimeCodeResult == dTimeCodeActual );

		REQUIRE( iPlaybackIDResult == iPlaybackID );
	}

	SECTION( "RemoveSignalSourceSequencerSample" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string sIDCall;
		int iSoundIDCall;

		fakeit::When( Method( VACoreMock, RemoveSignalSourceSequencerSample ) )
		    .AlwaysDo(
		        [&]( const std::string& sID, const int iSoundID )
		        {
			        sIDCall      = sID;
			        iSoundIDCall = iSoundID;
		        } );

		const std::string sID = GENERATE_RANDOM_STRING( 10 );
		const int iSoundID    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->RemoveSignalSourceSequencerSample( sID, iSoundID ) );

		fakeit::Verify( Method( VACoreMock, RemoveSignalSourceSequencerSample ) );

		REQUIRE( sIDCall == sID );
		REQUIRE( iSoundIDCall == iSoundID );
	}
}
