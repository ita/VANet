#include "test_utils.h"


TEST_CASE( "VANet::VANet/SoundSource", "[VANet][SoundSource][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "CreateSoundSource" )
	{
		// See SetSoundReceiverPosition unit test note.
		const int returnValue = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		std::string nameCall;
		fakeit::When( Method( VACoreMock, CreateSoundSource ) )
		    .AlwaysDo(
		        [&]( const std::string& name )
		        {
			        nameCall = name;
			        return returnValue;
		        } );

		const auto name = GENERATE_RANDOM_STRING( 10 );

		int result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSoundSource( name ) );

		REQUIRE( result == returnValue );

		fakeit::Verify( Method( VACoreMock, CreateSoundSource ) );
		REQUIRE( name == nameCall );
	}

	SECTION( "GetSoundSourceIDs" )
	{
		std::vector<int> viSoundSourceIDsGenerated = GENERATE_RANDOM_VECTOR( int, 10 );

		// viSoundSourceIDsGenerated.assign( GENERATE( take( MAX_REPS, random( 0, 255 ) ) ), GENERATE( take( MAX_REPS, random( 0, 255 ) ) ) );

		fakeit::When( Method( VACoreMock, GetSoundSourceIDs ) ).AlwaysDo( [&]( std::vector<int>& viSoundSourceIDs ) { viSoundSourceIDs = viSoundSourceIDsGenerated; } );

		std::vector<int> viSoundSourceIDs;

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundSourceIDs( viSoundSourceIDs ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceIDs ) );

		REQUIRE( viSoundSourceIDs == viSoundSourceIDsGenerated );
	}

	SECTION( "GetSoundSourceInfo" )
	{
		int iIDCall;

		CVASoundSourceInfo oSoundSourceInfoResult, oSoundSourceInfoActual;

		oSoundSourceInfoActual.iID                 = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		oSoundSourceInfoActual.sName               = GENERATE_RANDOM_STRING( 5 );
		oSoundSourceInfoActual.dSpoundPower        = GENERATE_RANDOM( double, MAX_REPS );
		oSoundSourceInfoActual.bMuted              = GENERATE( true, false );
		oSoundSourceInfoActual.sSignalSourceID     = GENERATE_RANDOM_STRING( 5 );
		oSoundSourceInfoActual.sExplicitRendererID = GENERATE_RANDOM_STRING( 5 );

		fakeit::When( Method( VACoreMock, GetSoundSourceInfo ) )
		    .AlwaysDo(
		        [&]( const int& iID )
		        {
			        iIDCall = iID;

			        CVASoundSourceInfo oSoundSourceInfo;

			        oSoundSourceInfo.iID                 = oSoundSourceInfoActual.iID;
			        oSoundSourceInfo.sName               = oSoundSourceInfoActual.sName;
			        oSoundSourceInfo.dSpoundPower        = oSoundSourceInfoActual.dSpoundPower;
			        oSoundSourceInfo.bMuted              = oSoundSourceInfoActual.bMuted;
			        oSoundSourceInfo.sSignalSourceID     = oSoundSourceInfoActual.sSignalSourceID;
			        oSoundSourceInfo.sExplicitRendererID = oSoundSourceInfoActual.sExplicitRendererID;

			        return oSoundSourceInfo;
		        } );

		auto iID = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( oSoundSourceInfoResult = client->GetCoreInstance( )->GetSoundSourceInfo( iID ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceInfo ) );

		REQUIRE( oSoundSourceInfoResult.iID == oSoundSourceInfoActual.iID );
		REQUIRE( oSoundSourceInfoResult.sName == oSoundSourceInfoActual.sName );
		REQUIRE( oSoundSourceInfoResult.dSpoundPower == oSoundSourceInfoActual.dSpoundPower );
		REQUIRE( oSoundSourceInfoResult.bMuted == oSoundSourceInfoActual.bMuted );
		REQUIRE( oSoundSourceInfoResult.sSignalSourceID == oSoundSourceInfoActual.sSignalSourceID );
		REQUIRE( oSoundSourceInfoResult.sExplicitRendererID == oSoundSourceInfoActual.sExplicitRendererID );
	}

	SECTION( "CreateSoundSourceExplicitRenderer" )
	{
		auto iNewSoundSourceID = GENERATE( take( MAX_REPS, random( -1, 255 ) ) );
		std::string sRendererIDCall;
		std::string sNameCall;

		fakeit::When( Method( VACoreMock, CreateSoundSourceExplicitRenderer ) )
		    .AlwaysDo(
		        [&]( const std::string& sRendererID, const std::string& sName = "" )
		        {
			        sRendererIDCall = sRendererID;
			        sNameCall       = sName;
			        return iNewSoundSourceID;
		        } );

		const auto sRendererID = GENERATE_RANDOM_STRING( 10 );
		const auto sName       = GENERATE_RANDOM_STRING( 10 );

		int iResult;

		REQUIRE_NOTHROW( iResult = client->GetCoreInstance( )->CreateSoundSourceExplicitRenderer( sRendererID, sName ) );

		fakeit::Verify( Method( VACoreMock, CreateSoundSourceExplicitRenderer ) );

		REQUIRE( iResult == iNewSoundSourceID );
		REQUIRE( sRendererIDCall == sRendererID );
		REQUIRE( sNameCall == sName );
	}

	SECTION( "DeleteSoundSource" )
	{
		int iSuccess = GENERATE( 0, -1 );

		fakeit::When( Method( VACoreMock, DeleteSoundSource ) ).Return( iSuccess );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iResult;

		REQUIRE_NOTHROW( iResult = client->GetCoreInstance( )->DeleteSoundSource( id ) );

		fakeit::Verify( Method( VACoreMock, DeleteSoundSource ).Using( id ) );

		REQUIRE( iResult == iSuccess );
	}

	SECTION( "SetSoundSourceEnabled" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundSourceEnabled ) );

		auto id       = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		bool bEnabled = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceEnabled( id, bEnabled ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceEnabled ).Using( id, bEnabled ) );
	}

	SECTION( "GetSoundSourceEnabled" )
	{
		bool bEnabled = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetSoundSourceEnabled ) ).Return( bEnabled );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetSoundSourceEnabled( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceEnabled ).Using( id ) );

		REQUIRE( bResult == bEnabled );
	}

	SECTION( "GetSoundSourceName" )
	{
		const auto sName = GENERATE_RANDOM_STRING( 10 );

		fakeit::When( Method( VACoreMock, GetSoundSourceName ) ).Return( sName );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		std::string sResult;

		REQUIRE_NOTHROW( sResult = client->GetCoreInstance( )->GetSoundSourceName( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceName ).Using( id ) );

		REQUIRE( sResult == sName );
	}

	SECTION( "SetSoundSourceName" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		std::string sCall;

		fakeit::When( Method( VACoreMock, SetSoundSourceName ) )
		    .AlwaysDo(
		        [&]( const int& id, const std::string& sName )
		        {
			        idCall = id;
			        sCall  = sName;
		        } );

		auto id    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto sName = GENERATE_RANDOM_STRING( 10 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceName( id, sName ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceName ) );

		REQUIRE( id == idCall );
		REQUIRE( sName == sCall );
	}

	SECTION( "GetSoundSourceSignalSource" )
	{
		const auto signalSourceId = GENERATE_RANDOM_STRING( 10 );

		fakeit::When( Method( VACoreMock, GetSoundSourceSignalSource ) ).Return( signalSourceId );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		std::string sResult;

		REQUIRE_NOTHROW( sResult = client->GetCoreInstance( )->GetSoundSourceSignalSource( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceSignalSource ).Using( id ) );

		REQUIRE( sResult == signalSourceId );
	}

	SECTION( "SetSoundSourceSignalSource" )
	{
		int callArg_SoundSourceId;
		std::string callArg_SignalSourceId;
		fakeit::When( Method( VACoreMock, SetSoundSourceSignalSource ) )
		    .AlwaysDo(
		        [&]( const int& soundSourceId, const std::string& signalSourceId )
		        {
			        callArg_SignalSourceId = signalSourceId;
			        callArg_SoundSourceId  = soundSourceId;
		        } );

		auto soundSourceId  = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto signalSourceId = GENERATE_RANDOM_STRING( 10 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceSignalSource( soundSourceId, signalSourceId ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceSignalSource ) );

		REQUIRE( soundSourceId == callArg_SoundSourceId );
		REQUIRE( signalSourceId == callArg_SignalSourceId );
	}

	SECTION( "GetSoundSourceAuralizationMode" )
	{
		const auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetSoundSourceAuralizationMode ) ).Return( iAuralizationMode );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iAuralizationModeResult;

		REQUIRE_NOTHROW( iAuralizationModeResult = client->GetCoreInstance( )->GetSoundSourceAuralizationMode( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceAuralizationMode ).Using( id ) );

		REQUIRE( iAuralizationModeResult == iAuralizationMode );
	}

	SECTION( "SetSoundSourceAuralizationMode" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundSourceAuralizationMode ) );

		auto id                = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceAuralizationMode( id, iAuralizationMode ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceAuralizationMode ).Using( id, iAuralizationMode ) );
	}

	SECTION( "SetSoundSourceParameters" )
	{
		int idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, SetSoundSourceParameters ) )
		    .AlwaysDo(
		        [&]( const int& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetSoundSourceParameters" )
	{
		fakeit::When( Method( VACoreMock, GetSoundSourceParameters ) )
		    .AlwaysDo(
		        []( const int& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetSoundSourceParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetSoundSourceParameters ) );

		REQUIRE( oParamsResult == oParams );
	}

	SECTION( "GetSoundSourceDirectivity" )
	{
		const auto iDirectivityId = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetSoundSourceDirectivity ) ).Return( iDirectivityId );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iDirectivityIdResult;

		REQUIRE_NOTHROW( iDirectivityIdResult = client->GetCoreInstance( )->GetSoundSourceDirectivity( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceDirectivity ).Using( id ) );

		REQUIRE( iDirectivityIdResult == iDirectivityId );
	}

	SECTION( "SetSoundSourceDirectivity" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundSourceDirectivity ) );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto idDirectivity = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceDirectivity( id, idDirectivity ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceDirectivity ).Using( id, idDirectivity ) );
	}

	SECTION( "GetSoundSourceSoundPower" )
	{
		double dSoundPower = GENERATE_RANDOM( double, MAX_REPS );

		fakeit::When( Method( VACoreMock, GetSoundSourceSoundPower ) ).Return( dSoundPower );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		double dSoundPowerResult;

		REQUIRE_NOTHROW( dSoundPowerResult = client->GetCoreInstance( )->GetSoundSourceSoundPower( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceSoundPower ).Using( id ) );

		REQUIRE( dSoundPowerResult == dSoundPower );
	}

	SECTION( "SetSoundSourceSoundPower" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundSourceSoundPower ) );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		double dSoundPower = GENERATE_RANDOM( double, MAX_REPS );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceSoundPower( id, dSoundPower ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceSoundPower ).Using( id, dSoundPower ) );
	}

	SECTION( "GetSoundSourceMuted" )
	{
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetSoundSourceMuted ) ).Return( bMuted );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetSoundSourceMuted( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceMuted ).Using( id ) );

		REQUIRE( bResult == bMuted );
	}

	SECTION( "SetSoundSourceMuted" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundSourceMuted ) );

		auto id     = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		bool bMuted = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceMuted( id, bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceMuted ).Using( id, bMuted ) );
	}

	SECTION( "GetSoundSourcePose" )
	{
		VAVec3 v3Pos   = generateVAVec3( );
		VAQuat qOrient = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundSourcePose ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3PosCall, VAQuat& qOrientCall )
		        {
			        v3PosCall.x = v3Pos.x;
			        v3PosCall.y = v3Pos.y;
			        v3PosCall.z = v3Pos.z;

			        qOrientCall.x = qOrient.x;
			        qOrientCall.y = qOrient.y;
			        qOrientCall.z = qOrient.z;
			        qOrientCall.w = qOrient.w;
		        } );

		VAVec3 v3PosResult;
		VAQuat qOrientResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundSourcePose( id, v3PosResult, qOrientResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourcePose ) );

		REQUIRE( v3Pos == v3PosResult );
		REQUIRE( qOrient == qOrientResult );
	}

	SECTION( "SetSoundSourcePose" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAVec3 vecCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundSourcePose ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& vec, const VAQuat& qOrientation )
		        {
			        idCall  = id;
			        vecCall = vec;
			        qCall   = qOrientation;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 vec;
		vec.x = GENERATE_RANDOM( double, MAX_REPS );
		vec.y = GENERATE_RANDOM( double, 1 );
		vec.z = GENERATE_RANDOM( double, 1 );

		VAQuat qOrientation;
		qOrientation.x = GENERATE_RANDOM( double, MAX_REPS );
		qOrientation.y = GENERATE_RANDOM( double, 1 );
		qOrientation.z = GENERATE_RANDOM( double, 1 );
		qOrientation.w = GENERATE_RANDOM( double, 1 );


		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourcePose( id, vec, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourcePose ) );

		REQUIRE( id == idCall );
		REQUIRE( vec == vecCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundSourcePosition" )
	{
		VAVec3 v3Position = generateVAVec3( true );

		fakeit::When( Method( VACoreMock, GetSoundSourcePosition ) ).Return( v3Position );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAVec3 v3PositionResult;

		REQUIRE_NOTHROW( v3PositionResult = client->GetCoreInstance( )->GetSoundSourcePosition( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourcePosition ).Using( id ) );

		REQUIRE( v3PositionResult == v3Position );
	}

	SECTION( "SetSoundSourcePosition" )
	{
		// See SetSoundReceiverPosition unit test note.
		VAVec3 vecCall;
		int idCall;
		fakeit::When( Method( VACoreMock, SetSoundSourcePosition ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& vec )
		        {
			        vecCall = vec;
			        idCall  = id;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 vec;
		vec.x = GENERATE_RANDOM( double, MAX_REPS );
		vec.y = GENERATE_RANDOM( double, 1 );
		vec.z = GENERATE_RANDOM( double, 1 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourcePosition( id, vec ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourcePosition ) );

		REQUIRE( id == idCall );
		REQUIRE( vec == vecCall );
	}

	SECTION( "GetSoundSourceOrientation" )
	{
		VAQuat qOrientation;
		qOrientation.x = GENERATE_RANDOM( double, MAX_REPS );
		qOrientation.y = GENERATE_RANDOM( double, 1 );
		qOrientation.z = GENERATE_RANDOM( double, 1 );
		qOrientation.w = GENERATE_RANDOM( double, 1 );

		fakeit::When( Method( VACoreMock, GetSoundSourceOrientation ) ).Return( qOrientation );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientationResult;

		REQUIRE_NOTHROW( qOrientationResult = client->GetCoreInstance( )->GetSoundSourceOrientation( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceOrientation ).Using( id ) );

		REQUIRE( qOrientationResult == qOrientation );
	}

	SECTION( "SetSoundSourceOrientation" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundSourceOrientation ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAQuat qOrientation )
		        {
			        idCall = id;
			        qCall  = qOrientation;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientation;
		qOrientation.x = GENERATE_RANDOM( double, MAX_REPS );
		qOrientation.y = GENERATE_RANDOM( double, 1 );
		qOrientation.z = GENERATE_RANDOM( double, 1 );
		qOrientation.w = GENERATE_RANDOM( double, 1 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceOrientation( id, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceOrientation ) );

		REQUIRE( id == idCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundSourceOrientationVU" )
	{
		VAVec3 v3View;
		v3View.x = GENERATE_RANDOM( double, MAX_REPS );
		v3View.y = GENERATE_RANDOM( double, 1 );
		v3View.z = GENERATE_RANDOM( double, 1 );

		VAVec3 v3Up;
		v3Up.x = GENERATE_RANDOM( double, MAX_REPS );
		v3Up.y = GENERATE_RANDOM( double, 1 );
		v3Up.z = GENERATE_RANDOM( double, 1 );

		fakeit::When( Method( VACoreMock, GetSoundSourceOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3ViewCall, VAVec3& v3UpCall )
		        {
			        v3ViewCall.x = v3View.x;
			        v3ViewCall.y = v3View.y;
			        v3ViewCall.z = v3View.z;

			        v3UpCall.x = v3Up.x;
			        v3UpCall.y = v3Up.y;
			        v3UpCall.z = v3Up.z;
		        } );

		VAVec3 v3ViewResult;
		VAVec3 v3UpResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundSourceOrientationVU( id, v3ViewResult, v3UpResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceOrientationVU ) );

		REQUIRE( v3View == v3ViewResult );
		REQUIRE( v3Up == v3UpResult );
	}

	SECTION( "SetSoundSourceOrientationVU" )
	{
		int idCall;
		VAVec3 v3ViewCall;
		VAVec3 v3UpCall;

		fakeit::When( Method( VACoreMock, SetSoundSourceOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& v3View, const VAVec3& v3Up )
		        {
			        idCall     = id;
			        v3ViewCall = v3View;
			        v3UpCall   = v3Up;
		        } );

		auto id       = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 v3View = generateVAVec3( );

		VAVec3 v3Up = generateVAVec3( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundSourceOrientationVU( id, v3View, v3Up ) );

		fakeit::Verify( Method( VACoreMock, SetSoundSourceOrientationVU ) );

		REQUIRE( id == idCall );
		REQUIRE( v3View == v3ViewCall );
		REQUIRE( v3Up == v3UpCall );
	}
}