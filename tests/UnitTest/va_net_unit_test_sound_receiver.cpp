#include "test_utils.h"


TEST_CASE( "VANet::VANet/SoundReceiver", "[VANet][SoundReceiver][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetSoundReceiverIDs" )
	{
		std::vector<int> viSoundReceiverIDsGenerated = GENERATE_RANDOM_VECTOR( int, 10 );
		// viSoundReceiverIDsGenerated.assign( GENERATE( take( MAX_REPS, random( 0, 255 ) ) ), GENERATE( take( MAX_REPS, random( 0, 255 ) ) ) );

		fakeit::When( Method( VACoreMock, GetSoundReceiverIDs ) )
		    .AlwaysDo( [&]( std::vector<int>& viSoundReceiverIDs ) { viSoundReceiverIDs = viSoundReceiverIDsGenerated; } );

		std::vector<int> viSoundReceiverIDs;

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundReceiverIDs( viSoundReceiverIDs ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverIDs ) );

		REQUIRE( viSoundReceiverIDs == viSoundReceiverIDsGenerated );
	}

	SECTION( "CreateSoundReceiver" )
	{
		const int returnValue = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		std::string nameCall;
		fakeit::When( Method( VACoreMock, CreateSoundReceiver ) )
		    .AlwaysDo(
		        [&]( const std::string& name )
		        {
			        nameCall = name;
			        return returnValue;
		        } );

		const auto name = GENERATE_RANDOM_STRING( 10 );

		int result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSoundReceiver( name ) );

		REQUIRE( result == returnValue );

		fakeit::Verify( Method( VACoreMock, CreateSoundReceiver ) );
		REQUIRE( name == nameCall );
	}

	SECTION( "CreateSoundReceiverExplicitRenderer" )
	{
		auto iNewSoundReceiverID = GENERATE( take( MAX_REPS, random( -1, 255 ) ) );
		std::string sRendererIDCall;
		std::string sNameCall;

		fakeit::When( Method( VACoreMock, CreateSoundReceiverExplicitRenderer ) )
		    .AlwaysDo(
		        [&]( const std::string& sRendererID, const std::string& sName = "" )
		        {
			        sRendererIDCall = sRendererID;
			        sNameCall       = sName;
			        return iNewSoundReceiverID;
		        } );

		const auto sRendererID = GENERATE_RANDOM_STRING( 10 );
		const auto sName       = GENERATE_RANDOM_STRING( 10 );

		int iResult;

		REQUIRE_NOTHROW( iResult = client->GetCoreInstance( )->CreateSoundReceiverExplicitRenderer( sRendererID, sName ) );

		fakeit::Verify( Method( VACoreMock, CreateSoundReceiverExplicitRenderer ) );

		REQUIRE( iResult == iNewSoundReceiverID );
		REQUIRE( sRendererIDCall == sRendererID );
		REQUIRE( sNameCall == sName );
	}

	SECTION( "DeleteSoundReceiver" )
	{
		int iSuccess = GENERATE( 0, -1 );

		fakeit::When( Method( VACoreMock, DeleteSoundReceiver ) ).Return( iSuccess );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iResult;

		REQUIRE_NOTHROW( iResult = client->GetCoreInstance( )->DeleteSoundReceiver( id ) );

		fakeit::Verify( Method( VACoreMock, DeleteSoundReceiver ).Using( id ) );

		REQUIRE( iResult == iSuccess );
	}

	SECTION( "GetSoundReceiverInfo" )
	{
		int iIDCall;

		CVASoundReceiverInfo SoundReceiverInfoResult, SoundReceiverInfoActual;

		SoundReceiverInfoActual.iID            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		SoundReceiverInfoActual.sName          = GENERATE_RANDOM_STRING( 5 );
		SoundReceiverInfoActual.sExplicitRendererID = GENERATE_RANDOM_STRING( 5 );

		fakeit::When( Method( VACoreMock, GetSoundReceiverInfo ) )
		    .AlwaysDo(
		        [&]( const int& iID )
		        {
			        iIDCall = iID;

			        CVASoundReceiverInfo SoundReceiverInfo;

			        SoundReceiverInfo.iID            = SoundReceiverInfoActual.iID;
			        SoundReceiverInfo.sName          = SoundReceiverInfoActual.sName;
					SoundReceiverInfo.sExplicitRendererID = SoundReceiverInfoActual.sExplicitRendererID;

			        return SoundReceiverInfo;
		        } );

		auto iID = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( SoundReceiverInfoResult = client->GetCoreInstance( )->GetSoundReceiverInfo( iID ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverInfo ) );

		REQUIRE( iIDCall == iID );

		REQUIRE( SoundReceiverInfoResult.iID == SoundReceiverInfoActual.iID );
		REQUIRE( SoundReceiverInfoResult.sName == SoundReceiverInfoActual.sName );
		REQUIRE( SoundReceiverInfoResult.sExplicitRendererID == SoundReceiverInfoActual.sExplicitRendererID );
	}

	SECTION( "SetSoundReceiverEnabled" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundReceiverEnabled ) );

		auto id       = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		bool bEnabled = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverEnabled( id, bEnabled ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverEnabled ).Using( id, bEnabled ) );
	}

	SECTION( "GetSoundReceiverEnabled" )
	{
		bool bEnabled = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetSoundSourceEnabled ) ).Return( bEnabled );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetSoundSourceEnabled( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundSourceEnabled ).Using( id ) );

		REQUIRE( bResult == bEnabled );
	}

	SECTION( "GetSoundReceiverName" )
	{
		const auto sName = GENERATE_RANDOM_STRING( 10 );

		fakeit::When( Method( VACoreMock, GetSoundReceiverName ) ).Return( sName );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		std::string sResult;

		REQUIRE_NOTHROW( sResult = client->GetCoreInstance( )->GetSoundReceiverName( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverName ).Using( id ) );

		REQUIRE( sResult == sName );
	}

	SECTION( "SetSoundReceiverName" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		std::string sCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverName ) )
		    .AlwaysDo(
		        [&]( const int& id, const std::string& sName )
		        {
			        idCall = id;
			        sCall  = sName;
		        } );

		auto id    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto sName = GENERATE_RANDOM_STRING( 10 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverName( id, sName ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverName ) );

		REQUIRE( id == idCall );
		REQUIRE( sName == sCall );
	}

	SECTION( "GetSoundReceiverAuralizationMode" )
	{
		const auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetSoundReceiverAuralizationMode ) ).Return( iAuralizationMode );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iAuralizationModeResult;

		REQUIRE_NOTHROW( iAuralizationModeResult = client->GetCoreInstance( )->GetSoundReceiverAuralizationMode( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverAuralizationMode ).Using( id ) );

		REQUIRE( iAuralizationModeResult == iAuralizationMode );
	}

	SECTION( "SetSoundReceiverAuralizationMode" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundReceiverAuralizationMode ) );

		auto id                = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverAuralizationMode( id, iAuralizationMode ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverAuralizationMode ).Using( id, iAuralizationMode ) );
	}

	SECTION( "GetSoundReceiverParameters" )
	{
		fakeit::When( Method( VACoreMock, GetSoundReceiverParameters ) )
		    .AlwaysDo(
		        []( const int& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetSoundReceiverParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverParameters ) );

		REQUIRE( oParamsResult == oParams );
	}

	SECTION( "SetSoundReceiverParameters" )
	{
		int idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, SetSoundReceiverParameters ) )
		    .AlwaysDo(
		        [&]( const int& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetSoundReceiverDirectivity" )
	{
		const auto iDirectivityId = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetSoundReceiverDirectivity ) ).Return( iDirectivityId );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		int iDirectivityIdResult;

		REQUIRE_NOTHROW( iDirectivityIdResult = client->GetCoreInstance( )->GetSoundReceiverDirectivity( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverDirectivity ).Using( id ) );

		REQUIRE( iDirectivityIdResult == iDirectivityId );
	}

	SECTION( "SetSoundReceiverDirectivity" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundReceiverDirectivity ) );

		auto lhsId = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto rhsId = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverDirectivity( lhsId, rhsId ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverDirectivity ).Using( lhsId, rhsId ) );
	}

	SECTION( "GetSoundReceiverMuted" )
	{
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetSoundReceiverMuted ) ).Return( bMuted );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetSoundReceiverMuted( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverMuted ).Using( id ) );

		REQUIRE( bResult == bMuted );
	}

	SECTION( "SetSoundReceiverMuted" )
	{
		fakeit::Fake( Method( VACoreMock, SetSoundReceiverMuted ) );

		auto id     = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		bool bMuted = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverMuted( id, bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverMuted ).Using( id, bMuted ) );
	}

	SECTION( "GetSoundReceiverPose" )
	{
		VAVec3 v3Pos   = generateVAVec3( );
		VAQuat qOrient = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverPose ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3PosCall, VAQuat& qOrientCall )
		        {
			        v3PosCall = v3Pos;

			        qOrientCall = qOrient;
		        } );

		VAVec3 v3PosResult;
		VAQuat qOrientResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundReceiverPose( id, v3PosResult, qOrientResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverPose ) );

		REQUIRE( v3Pos == v3PosResult );
		REQUIRE( qOrient == qOrientResult );
	}

	SECTION( "SetSoundReceiverPose" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAVec3 vecCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverPose ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& vec, const VAQuat& qOrientation )
		        {
			        idCall  = id;
			        vecCall = vec;
			        qCall   = qOrientation;
		        } );

		auto id    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 vec = generateVAVec3( );

		VAQuat qOrientation = generateVAQuaternion( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverPose( id, vec, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverPose ) );

		REQUIRE( id == idCall );
		REQUIRE( vec == vecCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundReceiverPosition" )
	{
		VAVec3 v3Position = generateVAVec3( true );

		fakeit::When( Method( VACoreMock, GetSoundReceiverPosition ) ).Return( v3Position );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAVec3 v3PositionResult;

		REQUIRE_NOTHROW( v3PositionResult = client->GetCoreInstance( )->GetSoundReceiverPosition( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverPosition ).Using( id ) );

		REQUIRE( v3PositionResult == v3Position );
	}

	SECTION( "SetSoundReceiverPosition" )
	{
		// This is a work around.
		// As far as I can tell, there seems to be an error with storing the VAVec3 in the FakeIt Method invocation.
		// As a result, the verification fails.
		// This way we can verify the call and don't get an error.
		// It might have to to with this issue: https://github.com/eranpeer/FakeIt/issues/274
		// Or with the multi threaded nature of gRPC.
		VAVec3 vecCall;
		int idCall;
		fakeit::When( Method( VACoreMock, SetSoundReceiverPosition ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& vec )
		        {
			        vecCall = vec;
			        idCall  = id;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 vec;
		vec.x = GENERATE_RANDOM( double, MAX_REPS );
		vec.y = GENERATE_RANDOM( double, 1 );
		vec.z = GENERATE_RANDOM( double, 1 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverPosition( id, vec ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverPosition ) );

		REQUIRE( id == idCall );
		REQUIRE( vec == vecCall );
	}

	SECTION( " GetSoundReceiverOrientation" )
	{
		VAQuat qOrientation = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverOrientation ) ).Return( qOrientation );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientationResult;

		REQUIRE_NOTHROW( qOrientationResult = client->GetCoreInstance( )->GetSoundReceiverOrientation( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverOrientation ).Using( id ) );

		REQUIRE( qOrientationResult == qOrientation );
	}

	SECTION( "SetSoundReceiverOrientation" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverOrientation ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAQuat qOrientation )
		        {
			        idCall = id;
			        qCall  = qOrientation;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientation = generateVAQuaternion( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverOrientation( id, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverOrientation ) );

		REQUIRE( id == idCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundReceiverOrientationVU" )
	{
		VAVec3 v3View = generateVAVec3( );

		VAVec3 v3Up = generateVAVec3( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3ViewCall, VAVec3& v3UpCall )
		        {
			        v3ViewCall = v3View;
			        v3UpCall   = v3Up;
		        } );

		VAVec3 v3ViewResult;
		VAVec3 v3UpResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundReceiverOrientationVU( id, v3ViewResult, v3UpResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverOrientationVU ) );

		REQUIRE( v3View == v3ViewResult );
		REQUIRE( v3Up == v3UpResult );
	}

	SECTION( "SetSoundReceiverOrientationVU" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAVec3 v3ViewCall;
		VAVec3 v3UpCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& v3View, const VAVec3& v3Up )
		        {
			        idCall     = id;
			        v3ViewCall = v3View;
			        v3UpCall   = v3Up;
		        } );

		auto id       = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 v3View = generateVAVec3( );

		VAVec3 v3Up = generateVAVec3( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverOrientationVU( id, v3View, v3Up ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverOrientationVU ) );

		REQUIRE( id == idCall );
		REQUIRE( v3View == v3ViewCall );
		REQUIRE( v3Up == v3UpCall );
	}

	SECTION( "GetSoundReceiverHeadAboveTorsoOrientation" )
	{
		VAQuat qOrientation = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverHeadAboveTorsoOrientation ) ).Return( qOrientation );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientationResult;

		REQUIRE_NOTHROW( qOrientationResult = client->GetCoreInstance( )->GetSoundReceiverHeadAboveTorsoOrientation( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverHeadAboveTorsoOrientation ).Using( id ) );

		REQUIRE( qOrientationResult == qOrientation );
	}

	SECTION( "SetSoundReceiverHeadAboveTorsoOrientation" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverHeadAboveTorsoOrientation ) )
		    .AlwaysDo(
		        [&]( const int& iID, const VAQuat qOrientation )
		        {
			        idCall = iID;
			        qCall  = qOrientation;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientation = generateVAQuaternion( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverHeadAboveTorsoOrientation( id, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverHeadAboveTorsoOrientation ) );

		REQUIRE( id == idCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundReceiverRealWorldPose" )
	{
		VAVec3 v3Pos   = generateVAVec3( );
		VAQuat qOrient = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverRealWorldPose ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3PosCall, VAQuat& qOrientCall )
		        {
			        v3PosCall = v3Pos;

			        qOrientCall = qOrient;
		        } );

		VAVec3 v3PosResult;
		VAQuat qOrientResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundReceiverRealWorldPose( id, v3PosResult, qOrientResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverRealWorldPose ) );

		REQUIRE( v3Pos == v3PosResult );
		REQUIRE( qOrient == qOrientResult );
	}

	SECTION( "SetSoundReceiverRealWorldPose" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAVec3 vecCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverRealWorldPose ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& vec, const VAQuat& qOrientation )
		        {
			        idCall  = id;
			        vecCall = vec;
			        qCall   = qOrientation;
		        } );

		auto id    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 vec = generateVAVec3( );

		VAQuat qOrientation = generateVAQuaternion( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverRealWorldPose( id, vec, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverRealWorldPose ) );

		REQUIRE( id == idCall );
		REQUIRE( vec == vecCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundReceiverRealWorldHeadAboveTorsoOrientation" )
	{
		VAQuat qOrientation = generateVAQuaternion( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverRealWorldHeadAboveTorsoOrientation ) ).Return( qOrientation );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientationResult;

		REQUIRE_NOTHROW( qOrientationResult = client->GetCoreInstance( )->GetSoundReceiverRealWorldHeadAboveTorsoOrientation( id ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverRealWorldHeadAboveTorsoOrientation ).Using( id ) );

		REQUIRE( qOrientationResult == qOrientation );
	}

	SECTION( "SetSoundReceiverRealWorldHeadAboveTorsoOrientation" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAQuat qCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverRealWorldHeadAboveTorsoOrientation ) )
		    .AlwaysDo(
		        [&]( const int& iID, const VAQuat qOrientation )
		        {
			        idCall = iID;
			        qCall  = qOrientation;
		        } );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		VAQuat qOrientation = generateVAQuaternion( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverRealWorldHeadAboveTorsoOrientation( id, qOrientation ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverRealWorldHeadAboveTorsoOrientation ) );

		REQUIRE( id == idCall );
		REQUIRE( qOrientation == qCall );
	}

	SECTION( "GetSoundReceiverRealWorldPositionOrientationVU" )
	{
		VAVec3 v3Pos  = generateVAVec3( );
		VAVec3 v3View = generateVAVec3( );
		VAVec3 v3Up   = generateVAVec3( );

		fakeit::When( Method( VACoreMock, GetSoundReceiverRealWorldPositionOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, VAVec3& v3PosCall, VAVec3& v3ViewCall, VAVec3& v3UpCall )
		        {
			        v3PosCall  = v3Pos;
			        v3ViewCall = v3View;
			        v3UpCall   = v3Up;
		        } );

		VAVec3 v3PosResult;
		VAVec3 v3ViewResult;
		VAVec3 v3UpResult;

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetSoundReceiverRealWorldPositionOrientationVU( id, v3PosResult, v3ViewResult, v3UpResult ) );

		fakeit::Verify( Method( VACoreMock, GetSoundReceiverRealWorldPositionOrientationVU ) );

		REQUIRE( v3Pos == v3PosResult );
		REQUIRE( v3View == v3ViewResult );
		REQUIRE( v3Up == v3UpResult );
	}

	SECTION( "SetSoundReceiverRealWorldPositionOrientationVU" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		VAVec3 v3PosCall;
		VAVec3 v3ViewCall;
		VAVec3 v3UpCall;

		fakeit::When( Method( VACoreMock, SetSoundReceiverRealWorldPositionOrientationVU ) )
		    .AlwaysDo(
		        [&]( const int& id, const VAVec3& v3Pos, const VAVec3& v3View, const VAVec3& v3Up )
		        {
			        idCall     = id;
			        v3PosCall  = v3Pos;
			        v3ViewCall = v3View;
			        v3UpCall   = v3Up;
		        } );

		auto id       = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		VAVec3 v3Pos  = generateVAVec3( );
		VAVec3 v3View = generateVAVec3( );
		VAVec3 v3Up   = generateVAVec3( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSoundReceiverRealWorldPositionOrientationVU( id, v3Pos, v3View, v3Up ) );

		fakeit::Verify( Method( VACoreMock, SetSoundReceiverRealWorldPositionOrientationVU ) );

		REQUIRE( id == idCall );
		REQUIRE( v3Pos == v3PosCall );
		REQUIRE( v3View == v3ViewCall );
		REQUIRE( v3Up == v3UpCall );
	}
}