#include "test_utils.h"

#include <fstream>
#include <iostream>


TEST_CASE( "VANet::VANet/General", "[VANet][General][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetVersionInfo" )
	{
		// See SetSoundReceiverPosition unit test note.
		CVAVersionInfo result, actual;

		actual.sVersion  = GENERATE_RANDOM_STRING( 5 );
		actual.sDate     = GENERATE_RANDOM_STRING( 5 );
		actual.sFlags    = GENERATE_RANDOM_STRING( 5 );
		actual.sComments = GENERATE_RANDOM_STRING( 5 );
		fakeit::When( Method( VACoreMock, GetVersionInfo ) )
		    .AlwaysDo(
		        [&]( CVAVersionInfo* versionInfo )
		        {
			        versionInfo->sVersion  = actual.sVersion;
			        versionInfo->sDate     = actual.sDate;
			        versionInfo->sFlags    = actual.sFlags;
			        versionInfo->sComments = actual.sComments;
			        ;
		        } );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetVersionInfo( &result ) );

		fakeit::Verify( Method( VACoreMock, GetVersionInfo ) );

		REQUIRE( result == actual );
	}

	// Deprecated
	/* SECTION( "SetOutputStream" )
	{
	    // See SetSoundReceiverPosition unit test note.

	    std::ostream posDebug_call;

	    fakeit::When( Method( VACoreMock, SetOutputStream ) ).AlwaysDo( [&]( std::ostream* posDebug ) { posDebug_call = posDebug; } );

	    std::filebuf fb;
	    fb.open( GENERATE_RANDOM_STRING( 10 ), std::ios::out );
	    std::ostream posDebugActual( &fb );

	    REQUIRE_NOTHROW( client->GetCoreInstance( )->SetOutputStream( &posDebugActual ) );

	    fakeit::Verify( Method( VACoreMock, SetOutputStream ) );

	    REQUIRE( posDebugActual == posDebug_call );
	}
	*/


	SECTION( "GetState" )
	{
		SECTION( "Core created" )
		{
			fakeit::When( Method( VACoreMock, GetState ) ).Return( IVAInterface::VA_CORESTATE_CREATED );
			int result;
			REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetState( ) );
			REQUIRE( result == IVAInterface::VA_CORESTATE_CREATED );
			fakeit::Verify( Method( VACoreMock, GetState ) );
		}

		SECTION( "Core ready" )
		{
			fakeit::When( Method( VACoreMock, GetState ) ).Return( IVAInterface::VA_CORESTATE_READY );
			int result;
			REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetState( ) );
			REQUIRE( result == IVAInterface::VA_CORESTATE_READY );
			fakeit::Verify( Method( VACoreMock, GetState ) );
		}

		SECTION( "Core fail" )
		{
			fakeit::When( Method( VACoreMock, GetState ) ).Return( IVAInterface::VA_CORESTATE_FAIL );
			int result;
			REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetState( ) );
			REQUIRE( result == IVAInterface::VA_CORESTATE_FAIL );
			fakeit::Verify( Method( VACoreMock, GetState ) );
		}
	}

	SECTION( "Reset" )
	{
		fakeit::Fake( Method( VACoreMock, Reset ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->Reset( ) );

		fakeit::Verify( Method( VACoreMock, Reset ) );
	}

	SECTION( "GetSearchPaths" )
	{
		CVAStruct actual = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, GetSearchPaths ) ).AlwaysReturn( actual );

		CVAStruct result;

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetSearchPaths( ) );

		fakeit::Verify( Method( VACoreMock, GetSearchPaths ) );

		REQUIRE( actual == result );
	}

	// Problem in client, FindFilePath function not present in m_pStub TODO
	/*SECTION( "FindFilePath" )
	{
	    // See SetSoundReceiverPosition unit test note.
	    std::string sFilepathCall, sResult;
	    std::string sResultActual = GENERATE_RANDOM_STRING( 30 );

	    fakeit::When( Method( VACoreMock, FindFilePath ) )
	        .AlwaysDo(
	            [&]( const std::string& sFilePath )
	            {
	                sFilepathCall = sFilePath;
	                return sResultActual;
	            } );

	    auto sFilePathActual = GENERATE_RANDOM_STRING( 30 );

	    REQUIRE_NOTHROW( sResult = client->GetCoreInstance( )->FindFilePath( sFilePathActual ) );

	    fakeit::Verify( Method( VACoreMock, FindFilePath ) );

	    REQUIRE( sFilePathActual == sFilepathCall );
	    REQUIRE( sResultActual == sResult );
	}*/

	SECTION( "GetCoreConfiguration" )
	{
		bool bFilterEnabled = GENERATE( true, false );
		CVAStruct returnValue, result;
		returnValue = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, GetCoreConfiguration ) ).AlwaysReturn( returnValue );

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetCoreConfiguration( bFilterEnabled ) );

		fakeit::Verify( Method( VACoreMock, GetCoreConfiguration ).Using( bFilterEnabled ) );

		REQUIRE( result == returnValue );
	}

	SECTION( "GetHardwareConfiguration" )
	{
		CVAStruct returnValue, result;
		returnValue = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, GetHardwareConfiguration ) ).AlwaysReturn( returnValue );

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetHardwareConfiguration( ) );

		fakeit::Verify( Method( VACoreMock, GetHardwareConfiguration ) );

		REQUIRE( result == returnValue );
	}

	SECTION( "GetFileList" )
	{
		// See SetSoundReceiverPosition unit test note.

		bool bRecursiveResult;
		std::string sFileSuffixFilterResult;

		CVAStruct returnValue, result;
		returnValue = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, GetFileList ) )
		    .AlwaysDo(
		        [&]( const bool bRecursive = true, const std::string& sFileSuffixFilter = "*" )
		        {
			        bRecursiveResult        = bRecursive;
			        sFileSuffixFilterResult = sFileSuffixFilter;
			        return returnValue;
		        } );

		std::string sFileSuffixFilterActual = GENERATE_RANDOM_STRING( 5 );
		bool bRecursiveActual               = GENERATE( true, false );

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->GetFileList( bRecursiveActual, sFileSuffixFilterActual ) );

		fakeit::Verify( Method( VACoreMock, GetFileList ) );

		REQUIRE( bRecursiveResult == bRecursiveActual );
		REQUIRE( sFileSuffixFilterResult == sFileSuffixFilterActual );
		REQUIRE( result == returnValue );
	}

	SECTION( "GetModules" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::vector<CVAModuleInfo> result, actual;

		const auto numModules = GENERATE( take( 1, random( 0, MAX_REPS ) ) );

		for( int i = 0; i < numModules; ++i )
		{
			CVAModuleInfo info;
			info.sName   = GENERATE_RANDOM_STRING( 10 );
			info.sDesc   = GENERATE_RANDOM_STRING( 20 );
			info.iID     = GENERATE( take( MAX_REPS, random( 0, MAX_REPS ) ) );
			actual.push_back( info );
		}

		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, GetModules ) ).AlwaysDo( [&]( std::vector<CVAModuleInfo>& moduleInfos ) { moduleInfos = actual; } );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetModules( result ) );

		fakeit::Verify( Method( VACoreMock, GetModules ) );

		REQUIRE( result == actual );
	}

	SECTION( "CallModule, implicitly tests VA::Struct encode/decode" )
	{
		fakeit::When( Method( VACoreMock, CallModule ) )
		    .Do(
		        []( const std::string& name, CVAStruct args )
		        {
			        args["name"] = name;
			        return args;
		        } );
		;


		CVAStruct input, nested, result;

		std::string moduleName =
		    GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( MAX_REPS, take( MAX_REPS, random( 32, 122 ) ) ) ) );

		std::string fieldName =
		    GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( MAX_REPS, take( MAX_REPS, random( 32, 122 ) ) ) ) );

		input["bool"]    = GENERATE( true, false );
		input["double"]  = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );
		input["integer"] = GENERATE( take( MAX_REPS, random( std::numeric_limits<int>::min( ), std::numeric_limits<int>::max( ) ) ) );
		input["string"] =
		    GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( MAX_REPS, take( MAX_REPS, random( 32, 122 ) ) ) ) );

		auto raw_data =
		    GENERATE( map( []( const std::vector<int>& i ) { return std::string( i.begin( ), i.end( ) ); }, chunk( MAX_REPS, take( MAX_REPS, random( 0, 255 ) ) ) ) );
		nested["data"] = CVAStructValue( &raw_data, raw_data.size( ) );

		nested["buffer"] = GENERATE( map(
		    []( const std::vector<double>& values )
		    {
			    CVASampleBuffer buf( values.size( ) );
			    for( int i = 0; i < values.size( ); ++i )
				    buf.vfSamples[i] = values[i];
			    return buf;
		    },
		    chunk( MAX_REPS, take( MAX_REPS, random( -1., 1. ) ) ) ) );

		input[fieldName] = nested;

		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CallModule( moduleName, input ) );

		// Add the modele name, which will be added in the mock
		input["name"] = moduleName;
		REQUIRE( result == input );
		fakeit::Verify( Method( VACoreMock, CallModule ) );
	}


	// Old from other modules
	SECTION( "SetSignalSourceBufferLooping" )
	{
		// See SetSoundReceiverPosition unit test note.
		bool loopingCall;
		std::string idCall;
		fakeit::When( Method( VACoreMock, SetSignalSourceBufferLooping ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const bool& looping )
		        {
			        loopingCall = looping;
			        idCall      = id;
		        } );

		const auto id      = GENERATE_RANDOM_STRING( 10 );
		const auto looping = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceBufferLooping( id, looping ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceBufferLooping ) );

		REQUIRE( id == idCall );
		REQUIRE( looping == loopingCall );
	}

	SECTION( "SetSignalSourceBufferPlaybackAction" )
	{
		// See SetSoundReceiverPosition unit test note.
		int callArg_PlaybackAction;
		std::string callArg_Id;
		fakeit::When( Method( VACoreMock, SetSignalSourceBufferPlaybackAction ) )
		    .AlwaysDo(
		        [&]( const std::string& id, const int& playbackAction )
		        {
			        callArg_PlaybackAction = playbackAction;
			        callArg_Id             = id;
		        } );

		const auto id            = GENERATE_RANDOM_STRING( 10 );
		const int playbackAction = GENERATE( IVAInterface::VA_PLAYBACK_ACTION_NONE, IVAInterface::VA_PLAYBACK_ACTION_STOP, IVAInterface::VA_PLAYBACK_ACTION_PAUSE,
		                                     IVAInterface::VA_PLAYBACK_ACTION_PLAY );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetSignalSourceBufferPlaybackAction( id, playbackAction ) );

		fakeit::Verify( Method( VACoreMock, SetSignalSourceBufferPlaybackAction ) );

		REQUIRE( id == callArg_Id );
		REQUIRE( playbackAction == callArg_PlaybackAction );
	}

	SECTION( "CreateSignalSourceBufferFromParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		std::string callArg_SignalSourceName;
		CVAStruct callArg_Parameter;
		auto returnValue = GENERATE_RANDOM_STRING( 10 );
		fakeit::When( Method( VACoreMock, CreateSignalSourceBufferFromParameters ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& params, const std::string& name )
		        {
			        callArg_SignalSourceName = name;
			        callArg_Parameter        = params;
			        return returnValue;
		        } );

		const std::string id = GENERATE_RANDOM_STRING( 10 );
		CVAStruct params     = generateRandomStruct( );

		std::string result;
		REQUIRE_NOTHROW( result = client->GetCoreInstance( )->CreateSignalSourceBufferFromParameters( params, id ) );

		fakeit::Verify( Method( VACoreMock, CreateSignalSourceBufferFromParameters ) );

		REQUIRE( result == returnValue );
		REQUIRE( id == callArg_SignalSourceName );
		REQUIRE( params == callArg_Parameter );
	}

	SECTION( "SetOutputGain" )
	{
		fakeit::Fake( Method( VACoreMock, SetOutputGain ) );

		auto gain = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetOutputGain( gain ) );

		fakeit::Verify( Method( VACoreMock, SetOutputGain ).Using( gain ) );
	}
}
