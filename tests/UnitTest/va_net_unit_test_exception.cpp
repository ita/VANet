#include "test_utils.h"

TEST_CASE( "VANet::VANet/ExceptionHandling", "[VANet][Exception][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = std::move( IVANetClient::Create( ) );
	client->Initialize( "localhost" );


	SECTION( "GetState" )
	{
		const std::string exceptionMessage = GENERATE_RANDOM_STRING( 10 );

		SECTION( "std::exception" )
		{
			fakeit::When( Method( VACoreMock, GetState ) )
			    .AlwaysDo(
			        [&]( )
			        {
				        throw std::runtime_error( exceptionMessage );
				        return 1;
			        } );

			REQUIRE_THROWS_MATCHES( client->GetCoreInstance( )->GetState( ), CVAException,
			                        Catch::Matchers::Predicate<CVAException>(
			                            [&]( const CVAException& exception )
			                            { return exception.GetErrorCode( ) == CVAException::UNSPECIFIED && exception.GetErrorMessage( ) == exceptionMessage; },
			                            "Exception did not match the expected details." ) );
		}

		SECTION( "VA Exception" )
		{
			const CVAException::ErrorCode errorCode =
			    GENERATE( CVAException::UNSPECIFIED, CVAException::INVALID_PARAMETER, CVAException::INVALID_ID, CVAException::MODAL_ERROR, CVAException::RESOURCE_IN_USE,
			              CVAException::FILE_NOT_FOUND, CVAException::NETWORK_ERROR, CVAException::PROTOCOL_ERROR, CVAException::NOT_IMPLEMENTED );

			fakeit::When( Method( VACoreMock, GetState ) )
			    .AlwaysDo(
			        [&]( )
			        {
				        throw CVAException( errorCode, exceptionMessage );
				        return 1;
			        } );

			REQUIRE_THROWS_MATCHES( client->GetCoreInstance( )->GetState( ), CVAException,
			                        Catch::Matchers::Predicate<CVAException>(
			                            [&]( const CVAException& exception )
			                            {
				                            int a;
				                            return exception.GetErrorCode( ) == errorCode && exception.GetErrorMessage( ) == exceptionMessage;
			                            },
			                            "Exception did not match the expected details." ) );
		}
	}
}

TEST_CASE( "VANet::VANet/ExceptionHandling,print", "[VANet][Exception][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = std::move( IVANetClient::Create( ) );
	client->Initialize( "localhost", 12340, IVANetClient::ExceptionHandlingMode::EXC_SERVER_PRINT );


	SECTION( "GetState" )
	{
		const std::string exceptionMessage = GENERATE_RANDOM_STRING( 10 );

		SECTION( "std::exception" )
		{
			fakeit::When( Method( VACoreMock, GetState ) )
			    .AlwaysDo(
			        [&]( )
			        {
				        throw std::runtime_error( exceptionMessage );
				        return 1;
			        } );

			REQUIRE_NOTHROW( client->GetCoreInstance( )->GetState( ) );
		}

		SECTION( "VA Exception" )
		{
			const CVAException::ErrorCode errorCode =
			    GENERATE( CVAException::UNSPECIFIED, CVAException::INVALID_PARAMETER, CVAException::INVALID_ID, CVAException::MODAL_ERROR, CVAException::RESOURCE_IN_USE,
			              CVAException::FILE_NOT_FOUND, CVAException::NETWORK_ERROR, CVAException::PROTOCOL_ERROR, CVAException::NOT_IMPLEMENTED );

			fakeit::When( Method( VACoreMock, GetState ) )
			    .AlwaysDo(
			        [&]( )
			        {
				        throw CVAException( errorCode, exceptionMessage );
				        return 1;
			        } );

			REQUIRE_NOTHROW( client->GetCoreInstance( )->GetState( ) );
		}
	}
}