#include "test_utils.h"

#include <thread>

TEST_CASE( "VABase::VAStruct/ValueVector", "[VABase]" )
{
	SECTION( "VAStructValue Data, vector input" )
	{
		const std::vector<uint8_t> data { 0x1, 0x5, 0x55, 0xFF, 0xAA };

		CVAStructValue value( const_cast<void*>( reinterpret_cast<const void*>( data.data( ) ) ), data.size( ) );

		const std::vector<uint8_t> result { static_cast<uint8_t*>( value.GetData( ) ), static_cast<uint8_t*>( value.GetData( ) ) + value.GetDataSize( ) };

		REQUIRE( data == result );
	}

	SECTION( "VAStructValue Data, string input" )
	{
		const std::string data { "this is a test" };

		CVAStructValue value( const_cast<void*>( reinterpret_cast<const void*>( data.data( ) ) ), data.size( ) );

		const std::string result { static_cast<uint8_t*>( value.GetData( ) ), static_cast<uint8_t*>( value.GetData( ) ) + value.GetDataSize( ) };

		REQUIRE( data == result );
	}
}

TEST_CASE( "VANet::VANetServer/ClientInfoCorrect", "[VANet][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	std::atomic_bool eventSendOut = false;

	// Mock method has to be set before initiation of the client since it will get called during it.
	fakeit::When( Method( VACoreMock, AttachEventHandler ) ).AlwaysDo( [&eventSendOut]( IVAEventHandler* pEventHandler ) { eventSendOut = true; } );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	while( !eventSendOut )
	{
		std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
	}

	eventSendOut = false;

	REQUIRE( server->IsClientConnected( ) );

	REQUIRE( server->GetNumConnectedClients( ) == 1 );

	REQUIRE( !server->GetClientHostname( 0 ).empty( ) );

	REQUIRE_NOTHROW( [&]( ) { REQUIRE( server->GetClientHostname( 1 ).empty( ) ); } );

	auto secondClient = IVANetClient::Create( );
	secondClient->Initialize( "localhost" );

	while( !eventSendOut )
	{
		std::this_thread::sleep_for( std::chrono::milliseconds( 5 ) );
	}

	REQUIRE( server->IsClientConnected( ) );

	REQUIRE( server->GetNumConnectedClients( ) == 2 );

	REQUIRE( !server->GetClientHostname( 0 ).empty( ) );

	REQUIRE( !server->GetClientHostname( 1 ).empty( ) );

	REQUIRE_NOTHROW( [&]( ) { REQUIRE( server->GetClientHostname( 2 ).empty( ) ); } );

	secondClient.reset( );

	// wait for the client shutdown to propagate to the server
	std::this_thread::sleep_for( std::chrono::milliseconds( 200 ) );

	REQUIRE( server->IsClientConnected( ) );

	REQUIRE( server->GetNumConnectedClients( ) == 1 );

	REQUIRE( !server->GetClientHostname( 0 ).empty( ) );

	REQUIRE_NOTHROW( [&]( ) { REQUIRE( server->GetClientHostname( 1 ).empty( ) ); } );

	client.reset( );

	// wait for the client shutdown to propagate to the server
	std::this_thread::sleep_for( std::chrono::milliseconds( 200 ) );

	REQUIRE( !server->IsClientConnected( ) );

	REQUIRE( server->GetNumConnectedClients( ) == 0 );

	REQUIRE( server->GetClientHostname( 0 ).empty( ) );
}