#include "test_utils.h"


TEST_CASE( "VANet::VANet/InputOutput", "[VANet][InputOutput][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "GetInputMuted" )
	{
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetInputMuted ) ).Return( bMuted );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetInputMuted( ) );

		fakeit::Verify( Method( VACoreMock, GetInputMuted ) );

		REQUIRE( bResult == bMuted );
	}

	SECTION( "SetInputMuted" )
	{
		fakeit::Fake( Method( VACoreMock, SetInputMuted ) );

		bool bMuted = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetInputMuted( bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetInputMuted ).Using( bMuted ) );
	}

	SECTION( "GetInputGain" )
	{
		auto gain = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		fakeit::When( Method( VACoreMock, GetInputGain ) ).Return( gain );

		double dResult;

		REQUIRE_NOTHROW( dResult = client->GetCoreInstance( )->GetInputGain( ) );

		fakeit::Verify( Method( VACoreMock, GetInputGain ) );

		REQUIRE( dResult == gain );
	}

	SECTION( "SetInputGain" )
	{
		fakeit::Fake( Method( VACoreMock, SetInputGain ) );

		auto gain = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetInputGain( gain ) );

		fakeit::Verify( Method( VACoreMock, SetInputGain ).Using( gain ) );
	}

	SECTION( "GetOutputGain" )
	{
		auto gain = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		fakeit::When( Method( VACoreMock, GetOutputGain ) ).Return( gain );

		double dResult;

		REQUIRE_NOTHROW( dResult = client->GetCoreInstance( )->GetOutputGain( ) );

		fakeit::Verify( Method( VACoreMock, GetOutputGain ) );

		REQUIRE( dResult == gain );
	}

	SECTION( "SetOutputGain" )
	{
		fakeit::Fake( Method( VACoreMock, SetOutputGain ) );

		auto gain = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetOutputGain( gain ) );

		fakeit::Verify( Method( VACoreMock, SetOutputGain ).Using( gain ) );
	}

	SECTION( "GetOutputMuted" )
	{
		bool bMuted = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, GetOutputMuted ) ).Return( bMuted );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->GetOutputMuted( ) );

		fakeit::Verify( Method( VACoreMock, GetOutputMuted ) );

		REQUIRE( bResult == bMuted );
	}

	SECTION( "SetOutputMuted" )
	{
		fakeit::Fake( Method( VACoreMock, SetOutputMuted ) );

		bool bMuted = GENERATE( true, false );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetOutputMuted( bMuted ) );

		fakeit::Verify( Method( VACoreMock, SetOutputMuted ).Using( bMuted ) );
	}

	SECTION( "GetGlobalAuralizationMode" )
	{
		const auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, GetGlobalAuralizationMode ) ).Return( iAuralizationMode );

		int iAuralizationModeResult;

		REQUIRE_NOTHROW( iAuralizationModeResult = client->GetCoreInstance( )->GetGlobalAuralizationMode( ) );

		fakeit::Verify( Method( VACoreMock, GetGlobalAuralizationMode ) );

		REQUIRE( iAuralizationModeResult == iAuralizationMode );
	}

	SECTION( "SetGlobalAuralizationMode" )
	{
		fakeit::Fake( Method( VACoreMock, SetGlobalAuralizationMode ) );

		auto iAuralizationMode = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetGlobalAuralizationMode( iAuralizationMode ) );

		fakeit::Verify( Method( VACoreMock, SetGlobalAuralizationMode ).Using( iAuralizationMode ) );
	}

	SECTION( "GetCoreClock" )
	{
		auto clock = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		fakeit::When( Method( VACoreMock, GetCoreClock ) ).Return( clock );

		double dResult;

		REQUIRE_NOTHROW( dResult = client->GetCoreInstance( )->GetCoreClock( ) );

		fakeit::Verify( Method( VACoreMock, GetCoreClock ) );

		REQUIRE( dResult == clock );
	}

	SECTION( "SetCoreClock" )
	{
		fakeit::Fake( Method( VACoreMock, SetCoreClock ) );

		auto clock = GENERATE( take( MAX_REPS, random( std::numeric_limits<double>::min( ), std::numeric_limits<double>::max( ) ) ) );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetCoreClock( clock ) );

		fakeit::Verify( Method( VACoreMock, SetCoreClock ).Using( clock ) );
	}
}