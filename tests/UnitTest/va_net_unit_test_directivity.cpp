#include "test_utils.h"


TEST_CASE( "VANet::VANet/Directivity", "[VANet][Directivity][PrioHigh]" )
{
	fakeit::Mock<IVAInterface> VACoreMock;

	fakeit::Fake( Method( VACoreMock, AttachEventHandler ) );
	fakeit::Fake( Method( VACoreMock, DetachEventHandler ) );

	auto& core = VACoreMock.get( );

	auto server = IVANetServer::Create( );
	server->SetCoreInstance( &core );
	server->Initialize( "0.0.0.0" );

	auto client = IVANetClient::Create( );
	client->Initialize( "localhost" );

	SECTION( "CreateDirectivityFromParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		CVAStruct oParamsCall;
		std::string sNameCall;

		int iID = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		fakeit::When( Method( VACoreMock, CreateDirectivityFromParameters ) )
		    .AlwaysDo(
		        [&]( const CVAStruct& oParams, const std::string& sName = "" )
		        {
			        oParamsCall = oParams;
			        sNameCall   = sName;

			        return iID;
		        } );

		const CVAStruct oParams = generateRandomStruct( );
		const auto sName        = GENERATE_RANDOM_STRING( 10 );

		int iIDResult;

		REQUIRE_NOTHROW( iIDResult = client->GetCoreInstance( )->CreateDirectivityFromParameters( oParams, sName ) );

		fakeit::Verify( Method( VACoreMock, CreateDirectivityFromParameters ) );
		REQUIRE( iIDResult == iID );
		REQUIRE( oParamsCall == oParams );
		REQUIRE( sNameCall == sName );
	}

	SECTION( "DeleteDirectivity" )
	{
		bool bSuccess = GENERATE( true, false );

		fakeit::When( Method( VACoreMock, DeleteDirectivity ) ).Return( bSuccess );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		bool bResult;

		REQUIRE_NOTHROW( bResult = client->GetCoreInstance( )->DeleteDirectivity( id ) );

		fakeit::Verify( Method( VACoreMock, DeleteDirectivity ).Using( id ) );

		REQUIRE( bResult == bSuccess );
	}

	SECTION( "GetDirectivityInfo" )
	{
		int iIDCall;

		CVADirectivityInfo directivityInfoResult, directivityInfoActual;

		directivityInfoActual.iID         = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		directivityInfoActual.iClass      = GENERATE( take( 1, random( -1, 3 ) ) );
		directivityInfoActual.sName       = GENERATE_RANDOM_STRING( 10 );
		directivityInfoActual.sDesc       = GENERATE_RANDOM_STRING( 10 );
		directivityInfoActual.iReferences = GENERATE( take( 1, random( 0, 255 ) ) );
		directivityInfoActual.oParams     = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, GetDirectivityInfo ) )
		    .AlwaysDo(
		        [&]( const int& iID )
		        {
			        iIDCall = iID;

			        CVADirectivityInfo directivityInfo;

			        directivityInfo.iID         = directivityInfoActual.iID;
			        directivityInfo.iClass      = directivityInfoActual.iClass;
			        directivityInfo.sName       = directivityInfoActual.sName;
			        directivityInfo.sDesc       = directivityInfoActual.sDesc;
			        directivityInfo.iReferences = directivityInfoActual.iReferences;
			        directivityInfo.oParams     = directivityInfoActual.oParams;

			        return directivityInfo;
		        } );

		auto iID = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		REQUIRE_NOTHROW( directivityInfoResult = client->GetCoreInstance( )->GetDirectivityInfo( iID ) );

		fakeit::Verify( Method( VACoreMock, GetDirectivityInfo ) );

		REQUIRE( directivityInfoResult.iID == directivityInfoActual.iID );
		REQUIRE( directivityInfoResult.iClass == directivityInfoActual.iClass );
		REQUIRE( directivityInfoResult.sName == directivityInfoActual.sName );
		REQUIRE( directivityInfoResult.sDesc == directivityInfoActual.sDesc );
		REQUIRE( directivityInfoResult.iReferences == directivityInfoActual.iReferences );
		REQUIRE( directivityInfoResult.oParams == directivityInfoActual.oParams );
	}

	SECTION( "GetDirectivityInfos" )
	{
		// See SetSoundReceiverPosition unit test note.

		std::vector<CVADirectivityInfo> vDirInfos, vDirInfosResult;

		for( int i = 0; i < GENERATE( take( 1, random( 1, 3 ) ) ); i++ )
		{
			vDirInfos.push_back( generateRandomDirectivityInfo( ) );
		}

		fakeit::When( Method( VACoreMock, GetDirectivityInfos ) ).AlwaysDo( [&]( std::vector<CVADirectivityInfo>& vDirInfosCall ) { vDirInfosCall = vDirInfos; } );

		vDirInfosResult.clear( );
		REQUIRE_NOTHROW( client->GetCoreInstance( )->GetDirectivityInfos( vDirInfosResult ) );

		fakeit::Verify( Method( VACoreMock, GetDirectivityInfos ) );

		REQUIRE( vDirInfosResult.size( ) == vDirInfos.size( ) );

		for( int i = 0; i < vDirInfos.size( ); ++i )
		{
			REQUIRE( vDirInfosResult.at( i ).iID == vDirInfos.at( i ).iID );
			REQUIRE( vDirInfosResult.at( i ).iClass == vDirInfos.at( i ).iClass );
			REQUIRE( vDirInfosResult.at( i ).sName == vDirInfos.at( i ).sName );
			REQUIRE( vDirInfosResult.at( i ).sDesc == vDirInfos.at( i ).sDesc );
			REQUIRE( vDirInfosResult.at( i ).iReferences == vDirInfos.at( i ).iReferences );
			REQUIRE( vDirInfosResult.at( i ).oParams == vDirInfos.at( i ).oParams );
		}
	}

	SECTION( "SetDirectivityName" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		std::string sCall;

		fakeit::When( Method( VACoreMock, SetDirectivityName ) )
		    .AlwaysDo(
		        [&]( const int& id, const std::string& sName )
		        {
			        idCall = id;
			        sCall  = sName;
		        } );

		auto id    = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		auto sName = GENERATE_RANDOM_STRING( 10 );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetDirectivityName( id, sName ) );

		fakeit::Verify( Method( VACoreMock, SetDirectivityName ) );

		REQUIRE( id == idCall );
		REQUIRE( sName == sCall );
	}

	SECTION( "GetDirectivityName" )
	{
		const auto sName = GENERATE_RANDOM_STRING( 10 );

		fakeit::When( Method( VACoreMock, GetDirectivityName ) ).Return( sName );

		auto id = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );

		std::string sResult;

		REQUIRE_NOTHROW( sResult = client->GetCoreInstance( )->GetDirectivityName( id ) );

		fakeit::Verify( Method( VACoreMock, GetDirectivityName ).Using( id ) );

		REQUIRE( sResult == sName );
	}

	SECTION( "SetDirectivityParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		int idCall;
		CVAStruct& oParamsCall = generateRandomStruct( );

		fakeit::When( Method( VACoreMock, SetDirectivityParameters ) )
		    .AlwaysDo(
		        [&]( const int& id, const CVAStruct& oParams )
		        {
			        idCall      = id;
			        oParamsCall = oParams;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );

		REQUIRE_NOTHROW( client->GetCoreInstance( )->SetDirectivityParameters( id, oParams ) );

		fakeit::Verify( Method( VACoreMock, SetDirectivityParameters ) );

		REQUIRE( idCall == id );
		REQUIRE( oParamsCall == oParams );
	}

	SECTION( "GetDirectivityParameters" )
	{
		// See SetSoundReceiverPosition unit test note.
		fakeit::When( Method( VACoreMock, GetDirectivityParameters ) )
		    .AlwaysDo(
		        []( const int& id, CVAStruct params )
		        {
			        params["id"] = id;
			        return params;
		        } );

		auto id            = GENERATE( take( MAX_REPS, random( 0, 255 ) ) );
		CVAStruct& oParams = generateRandomStruct( );
		CVAStruct oParamsResult;

		REQUIRE_NOTHROW( oParamsResult = client->GetCoreInstance( )->GetDirectivityParameters( id, oParams ) );

		oParams["id"] = id;

		fakeit::Verify( Method( VACoreMock, GetDirectivityParameters ) );

		REQUIRE( oParamsResult == oParams );
	}
}
