syntax = "proto3";

import "va_struct.proto";

package VA;

message Vector3 {
	double x = 1;
	double y = 2;
	double z = 3;
}

message Quaternion {
	double x = 1;
	double y = 2;
	double z = 3;
	double w = 4;
}

message IntIdVector {
	repeated int32 ids = 1;
}

message CreateSignalSourceMachineRequest {
	string name       = 1;
	Struct parameters = 2;
}

message CreateSignalSourceEngineRequest {
	string name       = 1;
	Struct parameters = 2;
}

message CreateSignalSourcePrototypeFromParametersRequest {
	string name       = 1;
	Struct parameters = 2;
}

message CreateSignalSourceBufferFromParametersRequest {
	string name       = 1;
	Struct parameters = 2;
}

message CreateDirectivityFromParametersRequest {
	string name       = 1;
	Struct parameters = 2;
}

message CallModuleRequest {
	string module_name       = 1;
	Struct module_parameters = 2;
}

message SetSoundSourceDirectivityRequest {
	int32 sound_source_id = 1;
	int32 directivity_id  = 2;
}

message SetSoundReceiverDirectivityRequest {
	int32 sound_receiver_id = 1;
	int32 directivity_id    = 2;
}

message SetSoundSourceSignalSourceRequest {
	int32 sound_source_id   = 1;
	string signal_source_id = 2;
}

message CoreState {
	enum State {
		CREATED = 0;
		READY   = 1;
		FAIL    = 2;
	}
	State state = 1;
}

message VersionInfo {
	string version        = 1;
	string release_date   = 2;
	string property_flags = 3;
	string comments       = 4;
}

message VAModuleInfos {
	message ModuleInfo {
		string name        = 1;
		string description = 2;
		int32 id           = 3;
	}

	repeated ModuleInfo module_infos = 1;
}

message GetFileListRequest {
	bool recursive            = 1;
	string file_suffix_filter = 2;
}

message DirectivityInfo {
	enum Class {
		UNSPECIFIED             = 0;
		PROTOTYPE               = 1;
		IMPULSE_RESPONSE        = 2;
		TRANSFER_FUNCTION       = 3;
		ENERGETIC_BAND_SPECTRUM = 4;
	}

	int32 id             = 1;
	Class class          = 2;
	string name          = 3;
	string description   = 4;
	int32 num_references = 5;
	Struct parameters    = 6;
}

message DirectivityInfosReply {
	repeated DirectivityInfo directivity_infos = 1;
}

message CreateSignalSourceNetworkStreamRequest {
	string interface = 1;
	int32 port       = 2;
	string name      = 3;
}

message SignalSourceInfo {
	enum Type {
		UNSPECIFIED  = 0;
		FILE         = 1;
		SAMPLER      = 2;
		DEVICE_INPUT = 3;
		NETSTREAM    = 4;
		MACHINE      = 5;
		ENGINE       = 6;
	}

	string id                = 1;
	Type type                = 2;
	string name              = 3;
	string description       = 4;
	string state             = 5;
	int32 num_references     = 6;
}

message SignalSourceInfos {
	repeated SignalSourceInfo signal_source_infos = 1;
}

message PlaybackState {
	enum State {
		INVALID = 0;
		STOPPED = 1;
		PAUSED  = 2;
		PLAYING = 3;
	}

	State state = 1;
}

message PlaybackAction {
	enum Action {
		NONE  = 0;
		STOP  = 1;
		PAUSE = 2;
		PLAY  = 3;
	}

	Action action = 1;
}

message SetSignalSourceBufferPlaybackActionRequest {
	string signal_source_id        = 1;
	PlaybackAction playback_action = 2;
}

message AddSignalSourceSequencerPlaybackRequest {
	string signal_source_id = 1;
	int32 sound_id          = 2;
	int32 flags             = 3;
	double time_code        = 4;
}

message SoundSourceInfo {
	int32 id                = 1;
	string name             = 2;
	double sound_power      = 3;
	bool muted              = 4;
	string signal_source_id  = 5;
	string explicit_renderer_id = 6;
}

message GetSoundSourcePoseReply {
	Vector3 position       = 1;
	Quaternion orientation = 2;
}

message GetSoundSourceOrientationVUReply {
	Vector3 view = 1;
	Vector3 up   = 2;
}

message SoundReceiverInfo {
	int32 id                = 1;
	string name             = 2;
	string explicit_renderer_id = 3;
}

message GetSoundReceiverPoseReply {
	Vector3 position       = 1;
	Quaternion orientation = 2;
}

message GetSoundReceiverOrientationVUReply {
	Vector3 view = 1;
	Vector3 up   = 2;
}

message GetSoundReceiverRealWorldPositionOrientationVUReply {
	Vector3 position = 1;
	Vector3 view     = 2;
	Vector3 up       = 3;
}

message GetSoundReceiverRealWorldPoseReply {
	Vector3 position       = 1;
	Quaternion orientation = 2;
}

message AudioRendererInfo {
	string name                       = 1;
	string class                      = 2;
	string description                = 3;
	bool enabled                      = 4;
	bool output_detector_enabled      = 5;
	bool output_recording_enabled     = 6;
	string output_recording_file_path = 7;
	Struct parameters                 = 8;
}

message AudioRendererInfos {
	repeated AudioRendererInfo audio_renderer_infos = 1;
}

message AudioReproductionInfo {
	string id                        = 1;
	string class                     = 2;
	string description               = 3;
	bool enabled                     = 4;
	bool input_detector_enabled      = 5;
	bool input_recording_enabled     = 6;
	string input_recording_file_path = 7;
	bool bool_detector_enabled       = 8;
	bool bool_recording_enabled      = 9;
	string bool_recording_file_path  = 10;
	Struct parameters                = 11;
}

message AudioReproductionInfos {
	repeated AudioReproductionInfo audio_reproduction_infos = 1;
}

message SetSoundReceiverRealWorldPositionOrientationVURequest {
	int32 sound_receiver_id = 1;
	Vector3 position        = 2;
	Vector3 view            = 3;
	Vector3 up              = 4;
}

message SetDirectivityNameRequest {
	int32 directivity_id = 1;
	string name          = 2;
}

message SetDirectivityParametersRequest {
	int32 directivity_id = 1;
	Struct parameters    = 2;
}

message GetDirectivityParametersRequest {
	int32 directivity_id = 1;
	Struct parameters    = 2;
}

message SetSignalSourceBufferPlaybackPositionRequest {
	string signal_source_id = 1;
	double position         = 2;
}

message SetSignalSourceBufferLoopingRequest {
	string signal_source_id = 1;
	bool looping            = 2;
}

message SetSignalSourceParametersRequest {
	string signal_source_id = 1;
	Struct parameters       = 2;
}

message GetSignalSourceParametersRequest {
	string signal_source_id = 1;
	Struct parameters       = 2;
}

message AddSignalSourceSequencerSampleRequest {
	string signal_source_id = 1;
	string file_path        = 2;
}

message RemoveSignalSourceSequencerSampleRequest {
	string signal_source_id = 1;
	int32 sample_id         = 2;
}

message CreateSoundSourceExplicitRendererRequest {
	string renderer_id = 1;
	string name        = 2;
}

message SetSoundSourceEnabledRequest {
	int32 sound_source_id = 1;
	bool enabled          = 2;
}

message SetSoundSourceNameRequest {
	int32 sound_source_id = 1;
	string name           = 2;
}

message SetSoundSourceAuralizationModeRequest {
	int32 sound_source_id = 1;
	int32 mode            = 2;
}

message SetSoundSourceParametersRequest {
	int32 sound_source_id = 1;
	Struct parameters     = 2;
}

message GetSoundSourceParametersRequest {
	int32 sound_source_id = 1;
	Struct parameters     = 2;
}

message SetSoundSourceSoundPowerRequest {
	int32 sound_source_id = 1;
	double sound_power    = 2;
}

message SetSoundSourceMutedRequest {
	int32 sound_source_id = 1;
	bool muted            = 2;
}

message SetSoundSourcePoseRequest {
	int32 sound_source_id  = 1;
	Vector3 position       = 2;
	Quaternion orientation = 3;
}

message SetSoundSourcePositionRequest {
	int32 sound_source_id = 1;
	Vector3 position      = 2;
}

message SetSoundSourceOrientationRequest {
	int32 sound_source_id  = 1;
	Quaternion orientation = 2;
}

message SetSoundSourceOrientationVURequest {
	int32 sound_source_id = 1;
	Vector3 view          = 2;
	Vector3 up            = 3;
}

message CreateSoundReceiverExplicitRendererRequest {
	string renderer_id = 1;
	string name        = 2;
}

message SetSoundReceiverEnabledRequest {
	int32 sound_receiver_id = 1;
	bool enabled            = 2;
}

message SetSoundReceiverNameRequest {
	int32 sound_receiver_id = 1;
	string name             = 2;
}

message SetSoundReceiverAuralizationModeRequest {
	int32 sound_receiver_id = 1;
	int32 mode              = 2;
}

message SetSoundReceiverParametersRequest {
	int32 sound_receiver_id = 1;
	Struct parameters       = 2;
}

message GetSoundReceiverParametersRequest {
	int32 sound_receiver_id = 1;
	Struct parameters       = 2;
}

message SetSoundReceiverMutedRequest {
	int32 sound_receiver_id = 1;
	bool muted              = 2;
}

message SetSoundReceiverPoseRequest {
	int32 sound_receiver_id = 1;
	Vector3 position        = 2;
	Quaternion orientation  = 3;
}

message SetSoundReceiverPositionRequest {
	int32 sound_receiver_id = 1;
	Vector3 position        = 2;
}

message SetSoundReceiverOrientationVURequest {
	int32 sound_receiver_id = 1;
	Vector3 view            = 2;
	Vector3 up              = 3;
}

message SetSoundReceiverOrientationRequest {
	int32 sound_receiver_id = 1;
	Quaternion orientation  = 2;
}

message SetSoundReceiverHeadAboveTorsoOrientationRequest {
	int32 sound_receiver_id = 1;
	Quaternion orientation  = 2;
}

message SetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest {
	int32 sound_receiver_id = 1;
	Quaternion orientation  = 2;
}

message SetSoundReceiverRealWorldPoseRequest {
	int32 sound_receiver_id = 1;
	Vector3 position        = 2;
	Quaternion orientation  = 3;
}

message SetRenderingModuleMutedRequest {
	string rendering_module_id = 1;
	bool muted                 = 2;
}

message SetRenderingModuleGainRequest {
	string rendering_module_id = 1;
	double gain                = 2;
}

message SetRenderingModuleAuralizationModeRequest {
	string rendering_module_id = 1;
	int32 mode                 = 2;
}

message SetRenderingModuleParametersRequest {
	string rendering_module_id = 1;
	Struct parameters          = 2;
}

message GetRenderingModuleParametersRequest {
	string rendering_module_id = 1;
	Struct parameters          = 2;
}

message SetReproductionModuleMutedRequest {
	string reproduction_module_id = 1;
	bool muted                    = 2;
}

message SetReproductionModuleGainRequest {
	string reproduction_module_id = 1;
	double gain                   = 2;
}
message SetReproductionModuleParametersRequest {
	string reproduction_module_id = 1;
	Struct parameters             = 2;
}

message GetReproductionModuleParametersRequest {
	string reproduction_module_id = 1;
	Struct parameters             = 2;
}

message DeleteDirectivityRequest {
	int32 directivity_id = 1;
}

message GetDirectivityInfoRequest {
	int32 directivity_id = 1;
}

message GetDirectivityNameRequest {
	int32 directivity_id = 1;
}

message DeleteSignalSourceRequest {
	string signal_source_id = 1;
}

message GetSignalSourceInfoRequest {
	string signal_source_id = 1;
}

message GetSignalSourceBufferPlaybackStateRequest {
	string signal_source_id = 1;
}

message GetSignalSourceBufferLoopingRequest {
	string signal_source_id = 1;
}

message GetSoundSourceInfoRequest {
	int32 sound_source_id = 1;
}

message DeleteSoundSourceRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceEnabledRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceNameRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceSignalSourceRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceAuralizationModeRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceDirectivityRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceSoundPowerRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceMutedRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourcePoseRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourcePositionRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceOrientationRequest {
	int32 sound_source_id = 1;
}

message GetSoundSourceOrientationVURequest {
	int32 sound_source_id = 1;
}

message DeleteSoundReceiverRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverInfoRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverEnabledRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverNameRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverAuralizationModeRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverDirectivityRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverMutedRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverPoseRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverPositionRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverOrientationVURequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverOrientationRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverHeadAboveTorsoOrientationRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverRealWorldPositionOrientationVURequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverRealWorldPoseRequest {
	int32 sound_receiver_id = 1;
}

message GetSoundReceiverRealWorldHeadAboveTorsoOrientationRequest {
	int32 sound_receiver_id = 1;
}

message GetRenderingModuleMutedRequest {
	string rendering_module_id = 1;
}

message GetRenderingModuleGainRequest {
	string rendering_module_id = 1;
}

message GetRenderingModuleAuralizationModeRequest {
	string rendering_module_id = 1;
}

message GetReproductionModuleMutedRequest {
	string reproduction_module_id = 1;
}

message GetReproductionModuleGainRequest {
	string reproduction_module_id = 1;
}

message HomogeneousMediumParametersRequest {
	Struct parameters = 1;
}

message GetCoreConfigurationRequest {
	bool only_enabled = 1;
}

message GetRenderingModulesRequest {
	bool only_enabled = 1;
}

message GetReproductionModulesRequest {
	bool only_enabled = 1;
}

message CreateSignalSourceTextToSpeechRequest {
	string name = 1;
}

message CreateSignalSourceSequencerRequest {
	string name = 1;
}

message CreateSoundSourceRequest {
	string name = 1;
}

message CreateSoundReceiverRequest {
	string name = 1;
}

message SetHomogeneousMediumSoundSpeedRequest {
	double sound_speed = 1;
}

message SetHomogeneousMediumTemperatureRequest {
	double temperature = 1;
}

message SetHomogeneousMediumStaticPressureRequest {
	double static_pressure = 1;
}

message SetHomogeneousMediumRelativeHumidityRequest {
	double humidity = 1;
}

message SetInputGainRequest {
	double gain = 1;
}

message SetOutputGainRequest {
	double gain = 1;
}

message SetInputMutedRequest {
	bool muted = 1;
}

message SetOutputMutedRequest {
	bool muted = 1;
}

message SetGlobalAuralizationModeRequest {
	int32 mode = 1;
}

message SetCoreClockRequest {
	double time = 1;
}